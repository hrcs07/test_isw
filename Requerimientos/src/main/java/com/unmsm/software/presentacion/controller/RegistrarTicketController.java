package com.unmsm.software.presentacion.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.CategoriaService;
import com.unmsm.software.negocio.service.ServicioService;
import com.unmsm.software.negocio.service.TicketService;
import com.unmsm.software.presentacion.model.AbrirTicketModel;
import com.unmsm.software.presentacion.model.Categoria;
import com.unmsm.software.util.clases.PaginaUtil;

@Controller("registrarTicketController")
@ViewScoped
public class RegistrarTicketController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	private TicketService ticketService;

	@Autowired
	private CategoriaService categoriaService;

	@Autowired
	private ServicioService servicioService;
	
	private AbrirTicketModel abrirTicketModel;

	@PostConstruct
	public void init() {
		inicializarModels();
		listarCategoria1();
	}

	public void inicializarModels() {
		abrirTicketModel = new AbrirTicketModel();
	}
	
	//Listar Categorias y servicios para la navegacion del nuevo ticket
	public void listarCategoria1(){
		abrirTicketModel.setCategoria1(categoriaService.listarCategoriaPrimerNivel());
	}

	public void listarCategoria2(Categoria categoria1){
		abrirTicketModel.setCategoria1Seleccionada(categoria1);
		abrirTicketModel.setCategoria2(categoriaService.listarCategoriaSegundoNivel(abrirTicketModel.getCategoria1Seleccionada().getId_categoria()));
		PaginaUtil.ejecutar("PF('wgvMenuCategorias').next()");
	}

	public void listarCategoria3(Categoria categoria2){
		abrirTicketModel.setCategoria2Seleccionada(categoria2);
		abrirTicketModel.setCategoria3(categoriaService.listarCategoriaTercerNivel(abrirTicketModel.getCategoria2Seleccionada().getId_categoria()));
		PaginaUtil.ejecutar("PF('wgvMenuCategorias').next()");
	}

	public void listarServicios(Categoria categoria3){
		abrirTicketModel.setCategoria3Seleccionada(categoria3);
		abrirTicketModel.setServicio(servicioService.obtenerServiciosPorCategoria(abrirTicketModel.getCategoria3Seleccionada().getId_categoria()));
		PaginaUtil.ejecutar("PF('wgvMenuCategorias').next()");
	}
	
	//  ***************************************************************************************
	
	//  *********************************  Metodos set y get  *********************************
	
	//  ***************************************************************************************


	public TicketService getTicketService() {
		return ticketService;
	}
	public void setTicketService(TicketService ticketService) {
		this.ticketService = ticketService;
	}
	public CategoriaService getCategoriaService() {
		return categoriaService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public ServicioService getServicioService() {
		return servicioService;
	}
	public void setServicioService(ServicioService servicioService) {
		this.servicioService = servicioService;
	}
	public AbrirTicketModel getAbrirTicketModel() {
		return abrirTicketModel;
	}
	public void setAbrirTicketModel(AbrirTicketModel abrirTicketModel) {
		this.abrirTicketModel = abrirTicketModel;
	}
}
