package com.unmsm.software.presentacion.model;

import com.unmsm.software.persistencia.dto.UsuarioDTO;

public class UserModel {
	
	private UsuarioDTO usuarioDTO;
	
	public UserModel() {
		usuarioDTO = new UsuarioDTO();
	}

	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}

	public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
		this.usuarioDTO = usuarioDTO;
	}	
}
