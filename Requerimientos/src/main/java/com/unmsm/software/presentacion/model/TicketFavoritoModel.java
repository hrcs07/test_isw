package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.FavoritoDTO;

public class TicketFavoritoModel {
	
	private List<FavoritoDTO> listFavoritos;
	
	public List<FavoritoDTO> getListFavoritos() {
		return listFavoritos;
	}

	public void setListFavoritos(List<FavoritoDTO> listFavoritos) {
		this.listFavoritos = listFavoritos;
	}
}
