package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.SlaDTO;

public class SlaListarModel {

	private List<SlaDTO> listaSla;
	private List<SlaDTO> listaSlaFiltrados;

	public SlaListarModel() {
	}

	public List<SlaDTO> getListaSla() {
		return listaSla;
	}

	public void setListaSla(List<SlaDTO> listaSla) {
		this.listaSla = listaSla;
	}

	public List<SlaDTO> getListaSlaFiltrados() {
		return listaSlaFiltrados;
	}

	public void setListaSlaFiltrados(List<SlaDTO> listaSlaFiltrados) {
		this.listaSlaFiltrados = listaSlaFiltrados;
	}
}
