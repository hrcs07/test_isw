package com.unmsm.software.presentacion.controller;
import javax.servlet.http.HttpServletRequest; 

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;


import com.unmsm.software.negocio.service.TicketService;
import com.unmsm.software.negocio.service.TrabajadorService;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.persistencia.mapper.TicketMapper;
import com.unmsm.software.presentacion.model.SlaFormularioModel;
import com.unmsm.software.presentacion.model.SlaListarModel;
import com.unmsm.software.presentacion.model.Trabajador;

@Controller("bandejaController")
@ViewScoped
public class BandejaController implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private TrabajadorService trabajadorService;
	
	private TicketDTO ticket;
	
	private TicketDTO ticketSeleccionado;
	
	private TicketDTO selectedTicket;
	
	private Trabajador trabajador;
	
	private Trabajador trabajadorSeleccionado;
	
	


	public TicketDTO getSelectedTicket() {
		obtenerTrabajadores();
		return selectedTicket;
	}


	public void setSelectedTicket(TicketDTO selectedTicket) {
		this.selectedTicket = selectedTicket;
	}


	public TicketDTO getTicketSeleccionado() {
		return ticketSeleccionado;
	}


	public void setTicketSeleccionado(TicketDTO ticketSeleccionado) {
		this.ticketSeleccionado = ticketSeleccionado;
	}


	@PostConstruct
	public void init() {
		inicializarTickets();
		obtenerTicketsInicial();
		inicializarTrabajadores();
		obtenerTrabajadores();
		
	}

	
	public TicketService getTicketService() {
		return ticketService;
	}


	public void setTicketService(TicketService ticketService) {
		this.ticketService = ticketService;
	}

	public TicketDTO getTicket() {
		return ticket;
	}
	
	public void setTicket(TicketDTO ticket) {
		this.ticket = ticket;
	}


	public void obtenerTicketsInicial() {
		this.ticket.setListaTickets(ticketService.obtenerTicketsInicial());
	}
	
	public void obtenerTicketsTecnico() {
		this.ticket.setListaTickets(ticketService.obtenerTickets(getIdUsuario()));
	}
	
	public void obtenerTrabajadores() {
		this.trabajador.setListaTrabajador(trabajadorService.obtenerListaTrabajadores());
	}
	
	public void inicializarTickets() {
		this.ticket = new TicketDTO();
	}
	
	public void inicializarTrabajadores() {
		this.trabajador = new Trabajador();
	}
	
	public Integer ticketsNuevos(){
		return ticketService.numeroTicketsNuevos();
	}
	
	public Integer ticketsEnAtencion(){
		return ticketService.numeroTicketsEnAtencion();
	}
	
	public Integer ticketsEnEspera(){
		return ticketService.numeroTicketsEnEspera();
	}
	
	public Integer ticketsFinalizado(){
		return ticketService.numeroTicketsFinalizado();
	}
	
	public Integer ticketsTotal(){
		return ticketService.numeroTicketsTotal();
	}
	
	public Integer semaforoVerde(){
		return ticketService.numeroSemaforoVerde();
	}
	
	public Integer semaforoAmarillo(){
		return ticketService.numeroSemaforoAmarillo();
	}
	
	public Integer semaforoRojo(){
		return ticketService.numeroSemaforoRojo();
	}
	
	public void aceptarTicket(){
		System.out.println("ID DE TECNICO: " + getIdUsuario());
		try {	
			if(this.selectedTicket.getEstadoTicket().equals("REGISTRADO")){
				ticketService.aceptarTicket(this.selectedTicket.getIdTicket(), getIdUsuario());
				actualizarTabla();
				addMessage("Ticket asignado correctamente");
			}else if(this.selectedTicket.getEstadoTicket().equals("EN CURSO")){
				addMessage("El Ticket ya ha sido tomado");
			}else if(this.selectedTicket.getEstadoTicket().equals("EN ESPERA")){
				addMessage("El Ticket se encuentra en reasignacion");
			}else{
				addMessage("El Ticket ya ha sido finalizado");
				System.out.println(this.selectedTicket.getEstadoTicket());
			}
			
		} catch (Exception e) {
			System.out.println("Error:" + e);
		}
		
	}
	
	public Integer getIdUsuario(){
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().
				   getAuthentication().getPrincipal();
		String username=userDetails.getUsername();
		Integer id=0;
		id =trabajadorService.getIdUsuario(username);
		return id;
	}
	
	public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }


	public Trabajador getTrabajador() {
		return trabajador;
	}


	public void setTrabajador(Trabajador trabajador) {
		this.trabajador = trabajador;
	}


	public Trabajador getTrabajadorSeleccionado() {
		return trabajadorSeleccionado;
	}


	public void setTrabajadorSeleccionado(Trabajador trabajadorSeleccionado) {
		this.trabajadorSeleccionado = trabajadorSeleccionado;
	}
	
	public String obtenerNombreTrabajador(Integer idTrabajador){
		return ticketService.obtenerNombreTrabajador(idTrabajador);
	}
	
	public void reasignarTicket(){
		System.out.println("Reasignacion del ticket: " + selectedTicket.getIdTicket());
		System.out.println("Al tecnico: " + trabajadorSeleccionado.getId_trabajador());
		String correo=trabajadorService.obtenerNombreTrabajador(trabajadorSeleccionado.getId_usuario());
		System.out.println("Correo del tecnico: " +  correo);
		trabajador.enviarCorreosReasig(correo);
		ticketService.reasignacionTicket(selectedTicket.getIdTicket(), trabajadorSeleccionado.getId_usuario());
		addMessage("Ticket Reasignado Correctamente");
		actualizarTabla();
	}
	
	public void actualizarTabla(){
		inicializarTickets();
		obtenerTicketsTecnico();
		System.out.println("Se actualizo la tabla");
	}
	
}
