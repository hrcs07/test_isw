package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.ComponenteDTO;
import com.unmsm.software.persistencia.dto.SlaDTO;
import com.unmsm.software.persistencia.dto.TablaMaestraDTO;
import com.unmsm.software.util.clases.AplicacionUtil;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.TipoOperacion;

public class PlantillaFormularioModel {
	private ComponenteDTO componenteFormulario;
	private TipoOperacion tipoOperacion;

	private List<TablaMaestraDTO> listaTiposComponentes;

	private TablaMaestraDTO tipoComponenteSeleccionado;

	public PlantillaFormularioModel() {
		this.inicializarTipoComponentes();
	}

	public TablaMaestraDTO getTipoComponenteSeleccionado() {
		return tipoComponenteSeleccionado;
	}

	public void setTipoComponenteSeleccionado(TablaMaestraDTO tipoComponenteSeleccionado) {
		this.tipoComponenteSeleccionado = tipoComponenteSeleccionado;
	}

	public ComponenteDTO getComponenteFormulario() {
		return componenteFormulario;
	}

	public void setComponenteFormulario(ComponenteDTO componenteFormulario) {
		this.componenteFormulario = componenteFormulario;
	}

	public TipoOperacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(TipoOperacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public List<TablaMaestraDTO> getListaTiposComponentes() {
		return listaTiposComponentes;
	}

	public void setListaTiposComponentes(List<TablaMaestraDTO> listaTiposComponentes) {
		this.listaTiposComponentes = listaTiposComponentes;
	}

	public void inicializarComponenteFormulario() {
		this.componenteFormulario = new ComponenteDTO();
	}

	public boolean esOPeracionAgregar() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR)
			return true;
		else
			return false;
	}

	public boolean esOperacionModificar() {
		if (this.tipoOperacion == TipoOperacion.MODIFICAR)
			return true;
		else
			return false;
	}

	public String obtenerTituloOperacion() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR) {
			return TipoOperacion.AGREGAR.getMensaje();
		} else if (this.tipoOperacion == TipoOperacion.MODIFICAR) {
			return TipoOperacion.MODIFICAR.getMensaje();
		}
		return "";
	}

	public void inicializarTipoComponentes() {
		this.tipoComponenteSeleccionado = new TablaMaestraDTO();
	}
	// public void cargarDatosIniciales(int codigoComponente) {
	// this.slaFormulario.setCodigo(AplicacionUtil.formatoCodigoGenerado(Constantes.PREFIJO_COD_SLA,
	// codigoComponente));
	// }
}
