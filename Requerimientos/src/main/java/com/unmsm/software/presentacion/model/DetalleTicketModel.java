package com.unmsm.software.presentacion.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;
import com.unmsm.software.persistencia.dto.MensajeDTO;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.util.ArchivoUtil;
import com.unmsm.software.util.clases.PaginaUtil;

public class DetalleTicketModel {
	//Ticket que se selecciona para ver sus detalles
	private TicketDTO ticketSeleccionado;
	
	//Lista de mensajes del ticket
	private List<MensajeDTO> listaMensajesTicket;
	
	//Nuevo mensaje que se va a registrar
	private MensajeDTO mensajeDTO;
	
	private UploadedFile archivoMensaje;
	private List<ArchivoTicketDTO> listaArchivosMensaje;
	private StreamedContent archivoDescargar;
	
	public DetalleTicketModel() {
		ticketSeleccionado = new TicketDTO();
		listaMensajesTicket = new ArrayList<MensajeDTO>();
		
		mensajeDTO = new MensajeDTO();
	}
	
	public void inicializarMensaje(Integer idUsuario){
		mensajeDTO.setIdTicket(ticketSeleccionado.getIdTicket());
		mensajeDTO.setIdUsuario(idUsuario);
		mensajeDTO.setFechaMensaje(new Date());
		
		listaArchivosMensaje = new ArrayList<ArchivoTicketDTO>();
	}
	
	public void cargarArchivo(FileUploadEvent event) {
		archivoMensaje = event.getFile();
		
		ArchivoTicketDTO archivoTicketDTO = new ArchivoTicketDTO();
		archivoTicketDTO.setfSubida(new Date());
		archivoTicketDTO.setNombreArchivo(archivoMensaje.getFileName());
		archivoTicketDTO.setArchivoBytes(archivoMensaje.getContents());
		System.out.println(archivoTicketDTO.getNombreArchivo()+" - "+archivoTicketDTO.getArchivoBytes());
		
		listaArchivosMensaje.add(archivoTicketDTO);
		
		PaginaUtil.mostrarMensajeJSF(1, "Archivos cargados con exito.", "Archivos cargados con exito.");
	}
	
	public void descargarArchivo(ArchivoTicketDTO archivoTicketDTO){
		archivoDescargar = ArchivoUtil.convertirAStreamed(archivoTicketDTO.getArchivoBytes(), archivoTicketDTO.getNombreArchivo());
	}
	
	public String formatoFecha(Date fecha){
		String fechaFormat = "";
		
		if(fecha != null)
			fechaFormat = fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+(fecha.getYear()+1900);
			
		return fechaFormat;
	}
	
	//  ***************************************************************************************
	
	//  *********************************  Metodos set y get  *********************************
	
	//  ***************************************************************************************

	public TicketDTO getTicketSeleccionado() {
		return ticketSeleccionado;
	}

	public void setTicketSeleccionado(TicketDTO ticketSeleccionado) {
		this.ticketSeleccionado = ticketSeleccionado;
	}
	
	public List<MensajeDTO> getListaMensajesTicket() {
		return listaMensajesTicket;
	}
	
	public void setListaMensajesTicket(List<MensajeDTO> listaMensajesTicket) {
		this.listaMensajesTicket = listaMensajesTicket;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}

	public UploadedFile getArchivo() {
		return archivoMensaje;
	}

	public void setArchivo(UploadedFile archivo) {
		this.archivoMensaje = archivo;
	}

	public List<ArchivoTicketDTO> getListaArchivos() {
		return listaArchivosMensaje;
	}

	public void setListaArchivos(List<ArchivoTicketDTO> listaArchivos) {
		this.listaArchivosMensaje = listaArchivos;
	}

	public UploadedFile getArchivoMensaje() {
		return archivoMensaje;
	}

	public void setArchivoMensaje(UploadedFile archivoMensaje) {
		this.archivoMensaje = archivoMensaje;
	}

	public List<ArchivoTicketDTO> getListaArchivosMensaje() {
		return listaArchivosMensaje;
	}

	public void setListaArchivosMensaje(List<ArchivoTicketDTO> listaArchivosMensaje) {
		this.listaArchivosMensaje = listaArchivosMensaje;
	}

	public StreamedContent getArchivoDescargar() {
		return archivoDescargar;
	}

	public void setArchivoDescargar(StreamedContent archivoDescargar) {
		this.archivoDescargar = archivoDescargar;
	}
}
