package com.unmsm.software.presentacion.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

@Controller
@ManagedBean(name="principalController")
@ViewScoped
public class PrincipalController implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	
    @PostConstruct
    public void init() {

    }

	public String getUsername() {
		   UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().
				   getAuthentication().getPrincipal();
		username=userDetails.getUsername();
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
    
}
