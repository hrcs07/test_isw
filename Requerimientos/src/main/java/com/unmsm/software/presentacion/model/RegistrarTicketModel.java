package com.unmsm.software.presentacion.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.dto.TablaMaestraDTO;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.util.clases.AplicacionUtil;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.EstadoTicket;
import com.unmsm.software.util.clases.PaginaUtil;

public class RegistrarTicketModel {
	private TicketDTO ticketDTO;
	
	private UploadedFile archivoTicket;
	private List<ArchivoTicketDTO> listaArchivosTicket;

	private Integer categoria1Seleccionada;
	private List<SelectItem> listItemCategoria1;

	private Integer categoria2Seleccionada;
	private List<SelectItem> listItemCategoria2;
	
	private Integer categoria3Seleccionada;
	private List<SelectItem> listItemCategoria3;

	private List<SelectItem> listItemServicio;
	private List<SelectItem> listItemPrioridad;

	public void inicializarTicket(Integer idCliente) {
		ticketDTO = new TicketDTO();
		ticketDTO.setIdCliente(idCliente);
		ticketDTO.setEstadoTicket(EstadoTicket.REGISTRADO.getMensaje());
		ticketDTO.setFechaTicket(new Date());
		
		listaArchivosTicket = new ArrayList<ArchivoTicketDTO>();
	}
	
	public void reiniciarTicketFormulario(){
		ticketDTO = new TicketDTO();
		listItemCategoria1 = new ArrayList<SelectItem>();
		listItemCategoria2 = new ArrayList<SelectItem>();
		listItemCategoria3 = new ArrayList<SelectItem>();
		listItemServicio = new ArrayList<SelectItem>();
	}

	public void asignarCodigoTicket(int codigoTicket) {
		ticketDTO.setCodigoTicket(AplicacionUtil.formatoCodigoGenerado(Constantes.PREFIJO_COD_TICKET, codigoTicket));
	}
	
	public void listarCategoria1Combo(List<Categoria> lista){
		listItemCategoria1 = convertirListToItemCategoria(lista);
	}
	
	public void listarCategoria2Combo(List<Categoria> lista){		
		listItemCategoria2 = convertirListToItemCategoria(lista);
		inicializarDatosCategoria2();
	}
	
	public void listarCategoria3Combo(List<Categoria> lista){
		listItemCategoria3 = convertirListToItemCategoria(lista);
		inicializarDatosCategoria3();
	}
	
	public void listarServicios(List<ServicioDTO> lista){
		listItemServicio = convertirListToItemServicio(lista);
		inicializarDatosServicios();
	}
	
	public void listarPrioridades(List<TablaMaestraDTO> lista){
		listItemPrioridad = convertirListToItemPrioridad(lista);
	}

	public void cargarArchivo(FileUploadEvent event) {
		archivoTicket = event.getFile();
		
		ArchivoTicketDTO archivoTicketDTO = new ArchivoTicketDTO();
		archivoTicketDTO.setfSubida(new Date());
		archivoTicketDTO.setNombreArchivo(archivoTicket.getFileName());
		archivoTicketDTO.setArchivoBytes(archivoTicket.getContents());
		System.out.println(archivoTicketDTO.getNombreArchivo()+" - "+archivoTicketDTO.getArchivoBytes());
		
		listaArchivosTicket.add(archivoTicketDTO);
		
		PaginaUtil.mostrarMensajeJSF(1, "Archivos cargados con exito.", "Archivos cargados con exito.");
	}

	// ********************* Metodo para iniciar los componentes al crear un ticket rapido
	
	public void inicializarDatosCategoria2() {
		categoria2Seleccionada = null;

		listItemCategoria3 = new ArrayList<>();
		categoria3Seleccionada = null;
		
		listItemServicio = new ArrayList<>();
		ticketDTO.setIdServicio(null);
	}
	
	public void inicializarDatosCategoria3() {
		categoria3Seleccionada = null;
		
		listItemServicio = new ArrayList<>();
		ticketDTO.setIdServicio(null);
	}
	
	public void inicializarDatosServicios() {
		ticketDTO.setIdServicio(null);
	}
	
	public List<SelectItem> convertirListToItemCategoria(List<Categoria> lista) {
		List<SelectItem> listItemCategoria = new ArrayList<SelectItem>();
		
		if (lista != null && lista.size() > 0) {
			for (Categoria categoriaDTO : lista) {
				SelectItem item = new SelectItem();
				item.setLabel(categoriaDTO.getDescripcion());
				item.setValue(categoriaDTO.getId_categoria());
				listItemCategoria.add(item);
			}
		}
		return listItemCategoria;
	}

	public List<SelectItem> convertirListToItemServicio(List<ServicioDTO> lista) {
		List<SelectItem> listItemServicio = new ArrayList<SelectItem>();
		if (lista != null && lista.size() > 0) {
			for (ServicioDTO servicioDTO : lista) {
				SelectItem item = new SelectItem();
				item.setLabel(servicioDTO.getNombreServicio());
				item.setValue(servicioDTO.getIdServicio());
				listItemServicio.add(item);
			}
		}
		return listItemServicio;
	}
	
	public List<SelectItem> convertirListToItemPrioridad(List<TablaMaestraDTO> lista){
		List<SelectItem> listItemPrioridad = new ArrayList<SelectItem>();
		if (lista != null && lista.size() > 0) {
			for (TablaMaestraDTO tablaMaestraDTO : lista) {
				SelectItem item = new SelectItem();
				item.setLabel(tablaMaestraDTO.getDescripcion());
				item.setValue(tablaMaestraDTO.getDescripcion());
				listItemPrioridad.add(item);
			}
		}
		return listItemPrioridad;
	}
	
//  ***************************************************************************************

//  *********************************  Metodos set y get  *********************************

//  ***************************************************************************************

	public TicketDTO getTicketDTO() {
		return ticketDTO;
	}

	public void setTicketDTO(TicketDTO ticketDTO) {
		this.ticketDTO = ticketDTO;
	}

	public List<ArchivoTicketDTO> getListaArchivos() {
		return listaArchivosTicket;
	}

	public UploadedFile getArchivo() {
		return archivoTicket;
	}

	public void setArchivo(UploadedFile archivo) {
		this.archivoTicket = archivo;
	}

	public void setListaArchivos(List<ArchivoTicketDTO> listaArchivos) {
		this.listaArchivosTicket = listaArchivos;
	}

	public Integer getCategoria1Seleccionada() {
		return categoria1Seleccionada;
	}

	public void setCategoria1Seleccionada(Integer categoria1Seleccionada) {
		this.categoria1Seleccionada = categoria1Seleccionada;
	}

	public List<SelectItem> getListItemCategoria1() {
		return listItemCategoria1;
	}

	public void setListItemCategoria1(List<SelectItem> listItemCategoria1) {
		this.listItemCategoria1 = listItemCategoria1;
	}

	public Integer getCategoria2Seleccionada() {
		return categoria2Seleccionada;
	}

	public void setCategoria2Seleccionada(Integer categoria2Seleccionada) {
		this.categoria2Seleccionada = categoria2Seleccionada;
	}

	public List<SelectItem> getListItemCategoria2() {
		return listItemCategoria2;
	}

	public void setListItemCategoria2(List<SelectItem> listItemCategoria2) {
		this.listItemCategoria2 = listItemCategoria2;
	}

	public Integer getCategoria3Seleccionada() {
		return categoria3Seleccionada;
	}

	public void setCategoria3Seleccionada(Integer categoria3Seleccionada) {
		this.categoria3Seleccionada = categoria3Seleccionada;
	}

	public List<SelectItem> getListItemCategoria3() {
		return listItemCategoria3;
	}

	public void setListItemCategoria3(List<SelectItem> listItemCategoria3) {
		this.listItemCategoria3 = listItemCategoria3;
	}

	public List<SelectItem> getListItemServicio() {
		return listItemServicio;
	}

	public void setListItemServicio(List<SelectItem> listItemServicio) {
		this.listItemServicio = listItemServicio;
	}

	public List<SelectItem> getListItemPrioridad() {
		return listItemPrioridad;
	}

	public void setListItemPrioridad(List<SelectItem> listItemPrioridad) {
		this.listItemPrioridad = listItemPrioridad;
	}
}
