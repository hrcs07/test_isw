package com.unmsm.software.presentacion.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.SlaService;
import com.unmsm.software.persistencia.dto.SlaDTO;
import com.unmsm.software.presentacion.model.SlaFormularioModel;
import com.unmsm.software.presentacion.model.SlaListarModel;
import com.unmsm.software.util.clases.PaginaUtil;
import com.unmsm.software.util.clases.TipoMensajeJSF;
import com.unmsm.software.util.clases.TipoOperacion;
import com.unmsm.software.util.excepciones.NegocioExcepcion;

/**
 * @author renzo broncano
 *
 */
@Controller("mantenerSlaController")
@ViewScoped
public class MantenerSlaController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	private SlaService slaService;

	private SlaListarModel slaModel;

	private SlaFormularioModel slaFormularioModel;

	@PostConstruct
	public void init() {
		inicializarModels();
		obtenerSlas();
	}

	public void iniciarFormularioAgregar() {
		this.slaFormularioModel.inicializarSlaFormulario();
		this.slaFormularioModel.cargarDatosIniciales(slaService.generarNumeracionCodSLA());
		this.slaFormularioModel.setTipoOperacion(TipoOperacion.AGREGAR);
	}

	public void agregarSLA() {
		try {
			slaService.guardarSLA(slaFormularioModel.getSlaFormulario());
			PaginaUtil.ejecutar("PF('wgvFormularioSLA').hide()");
			this.slaFormularioModel.inicializarSlaFormulario();
			obtenerSlas();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje", "El SLA se agrego con exito.");
			PaginaUtil.actualizarComponente("frmMantenerSLA:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}

	}

	public void iniciarFormularioModificar(SlaDTO slaSeleccionado) {
		this.slaFormularioModel.setSlaFormulario(slaSeleccionado);
		this.slaFormularioModel.setTipoOperacion(TipoOperacion.MODIFICAR);
	}

	public void modificarSLA() {
		try {
			slaService.actualizarSLA(slaFormularioModel.getSlaFormulario());
			PaginaUtil.ejecutar("PF('wgvFormularioSLA').hide()");
			this.slaFormularioModel.inicializarSlaFormulario();
			obtenerSlas();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El SLA se modifico con exito.");
			PaginaUtil.actualizarComponente("frmMantenerSLA:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}
	}

	public void iniciarFormularioEliminar(SlaDTO slaSeleccionado) {
		this.slaFormularioModel.setSlaFormulario(slaSeleccionado);
		this.slaFormularioModel.setTipoOperacion(TipoOperacion.ELIMINAR);
	}

	public void eliminarSLA() {
		try {
			slaService.eliminarSLA(slaFormularioModel.getSlaFormulario());
			obtenerSlas();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje", "El SLA se elimino con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}finally{
			PaginaUtil.actualizarComponente("frmMantenerSLA:messages");
			PaginaUtil.ejecutar("PF('dgEliminarSLA').hide()");
		}
	}

	public void ejecutarTipoOperacion() {
		if (this.slaFormularioModel.esOPeracionAgregar()) {
			agregarSLA();
		} else if (this.slaFormularioModel.esOperacionModificar()) {
			modificarSLA();
		}
	}

	public SlaService getSlaService() {
		return slaService;
	}

	public void setSlaService(SlaService slaService) {
		this.slaService = slaService;
	}

	public SlaListarModel getSlaModel() {
		return slaModel;
	}

	public void setSlaModel(SlaListarModel slaModel) {
		this.slaModel = slaModel;
	}

	public void obtenerSlas() {
		this.slaModel.setListaSla(slaService.obtenerSLAs());
	}

	public SlaFormularioModel getSlaFormularioModel() {
		return slaFormularioModel;
	}

	public void setSlaFormularioModel(SlaFormularioModel slaFormularioModel) {
		this.slaFormularioModel = slaFormularioModel;
	}

	public void inicializarModels() {
		this.slaModel = new SlaListarModel();
		this.slaFormularioModel = new SlaFormularioModel();
	}

}
