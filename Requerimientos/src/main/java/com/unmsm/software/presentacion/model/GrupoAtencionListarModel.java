package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;

public class GrupoAtencionListarModel {

	private List<GrupoAtencionDTO> listaGrupoAtencion;
	private List<GrupoAtencionDTO> listaGrupoAtencionFiltrados;

	public List<GrupoAtencionDTO> getListaGrupoAtencion() {
		return listaGrupoAtencion;
	}

	public void setListaGrupoAtencion(List<GrupoAtencionDTO> listaGrupoAtencion) {
		this.listaGrupoAtencion = listaGrupoAtencion;
	}

	public List<GrupoAtencionDTO> getListaGrupoAtencionFiltrados() {
		return listaGrupoAtencionFiltrados;
	}

	public void setListaGrupoAtencionFiltrados(List<GrupoAtencionDTO> listaGrupoAtencionFiltrados) {
		this.listaGrupoAtencionFiltrados = listaGrupoAtencionFiltrados;
	}

}
