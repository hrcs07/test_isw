package com.unmsm.software.presentacion.model;

import java.util.List;

public class Categoria implements Comparable<Categoria>{
    /**
	 * 
	 */
	
	/**
	 * 
	 */
	private Integer id_categoria;
	private String descripcion;
	private Integer estado;
	private Integer nivel;
	private Integer cat_padre;
	private List<Categoria> categorias;
	private byte[] imagen;
	private Integer niv1;
	private Integer niv2;
	private Integer niv3;
	
	private String name;
    
    private String size;
     
    private String type;

    	
	public Categoria(String name, String size, String type) {
		super();
		this.name = name;
		this.size = size;
		this.type = type;
	}
	

	public Categoria() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
		
    public Integer getId_categoria() {
		return id_categoria;
	}


	public void setId_categoria(Integer id_categoria) {
		this.id_categoria = id_categoria;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getEstado() {
		return estado;
	}


	public void setEstado(Integer estado) {
		this.estado = estado;
	}


	public Integer getNivel() {
		return nivel;
	}


	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}


	public Integer getCat_padre() {
		return cat_padre;
	}


	public void setCat_padre(Integer cat_padre) {
		this.cat_padre = cat_padre;
	}


	public List<Categoria> getCategorias() {
		return categorias;
	}


	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}


	public byte[] getImagen() {
		return imagen;
	}


	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}


	public Integer getNiv1() {
		return niv1;
	}


	public void setNiv1(Integer niv1) {
		this.niv1 = niv1;
	}


	public Integer getNiv2() {
		return niv2;
	}


	public void setNiv2(Integer niv2) {
		this.niv2 = niv2;
	}


	public Integer getNiv3() {
		return niv3;
	}


	public void setNiv3(Integer niv3) {
		this.niv3 = niv3;
	}


	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Categoria other = (Categoria) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (size == null) {
            if (other.size != null)
                return false;
        } else if (!size.equals(other.size))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return name;
    }
	public int compareTo(Categoria o) {
		// TODO Auto-generated method stub
		 return this.getName().compareTo(o.getName());
	}
    
    
}
