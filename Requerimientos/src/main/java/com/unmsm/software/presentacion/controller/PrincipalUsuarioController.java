package com.unmsm.software.presentacion.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.ArchivoTicketService;
import com.unmsm.software.negocio.service.CategoriaService;
import com.unmsm.software.negocio.service.FavoritoService;
import com.unmsm.software.negocio.service.ServicioService;
import com.unmsm.software.negocio.service.TicketService;
import com.unmsm.software.negocio.service.TrabajadorService;
import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.presentacion.model.PrincipalListarModel;
import com.unmsm.software.presentacion.model.RegistrarTicketModel;
import com.unmsm.software.presentacion.model.TicketFavoritoModel;
import com.unmsm.software.presentacion.model.TicketFormularioModel;
import com.unmsm.software.util.clases.EstadoTicket;
import com.unmsm.software.util.clases.PaginaUtil;

@Controller("principalUsuarioController")
@ViewScoped
public class PrincipalUsuarioController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	private TicketService ticketService;

	@Autowired
	private ArchivoTicketService archivoTicketService;

	@Autowired
	private CategoriaService categoriaService;

	@Autowired
	private ServicioService servicioService;

	@Autowired
	private FavoritoService favoritoService;
	
	@Autowired
	private TrabajadorService trabajadorService;
	
	private TicketFormularioModel ticketFormularioModel;

	// Model que controla la lista de ticket del cliente
	private PrincipalListarModel principalListarModel;
	// Model que controla la lista de favoritos del cliente
	private TicketFavoritoModel ticketFavoritoModel;
	// Model que guarda los datos para registrar un ticket
	private RegistrarTicketModel registrarTicketModel;

	// Usuario el cual se logea como cliente
	// public UsuarioDTO usuarioDTO;
	private Integer idCliente = 1;

	@PostConstruct
	public void init() {
		inicializarModels();
		obtenerTicketsYFavoritos(idCliente);
	}

	public void inicializarModels() {
		principalListarModel = new PrincipalListarModel();
		registrarTicketModel = new RegistrarTicketModel();
		ticketFavoritoModel = new TicketFavoritoModel();
	}

	public void misTickets() {
		principalListarModel.setListaTickets(ticketService.obtenerTicketsCliente(idCliente));
		principalListarModel.setListaTicketsFiltrados(principalListarModel.getListaTickets());
		
		for(TicketDTO ticketDTO : principalListarModel.getListaTickets()){
			ticketDTO.setListaArchivosTicket(archivoTicketService.buscarArchivosTicket(ticketDTO.getIdTicket()));
		}
		
		filtrarLista(0);
		PaginaUtil.actualizarComponente("frmPrincipal:frmListaTickets:tablaTicket");
	}

	public void abrirTicket() {
		PaginaUtil.actualizarComponente("frmPrincipal");
		PaginaUtil.actualizarComponente("frmNavegacionTicket");
		PaginaUtil.ejecutar("PF('wgvMenuCategorias').loadStep('categoria1', false)");
	}

	public void obtenerTicketsYFavoritos(Integer idCliente) {
		principalListarModel.setListaTickets(ticketService.obtenerTicketsCliente(idCliente));
		principalListarModel.setListaTicketsFiltrados(principalListarModel.getListaTickets());
		
		for(TicketDTO ticketDTO : principalListarModel.getListaTickets()){
			ticketDTO.setListaArchivosTicket(archivoTicketService.buscarArchivosTicket(ticketDTO.getIdTicket()));
		}
		
		ticketFavoritoModel.setListFavoritos(favoritoService.obtenerServiciosFavoritos(idCliente));
	}

	// Metodos para listar el menu de combos para la creacion de un ticket
	// rapido
	public void listarCategoria1Combo() {
		registrarTicketModel.listarCategoria1Combo(categoriaService.listarCategoriaPrimerNivel());
	}

	public void listarCategoria2Combo() {
		registrarTicketModel.listarCategoria2Combo(
				categoriaService.listarCategoriaSegundoNivel(registrarTicketModel.getCategoria1Seleccionada()));
	}

	public void listarCategoria3Combo() {
		registrarTicketModel.listarCategoria3Combo(
				categoriaService.listarCategoriaTercerNivel(registrarTicketModel.getCategoria2Seleccionada()));

	}

	public void listarServiciosCombo() {
		registrarTicketModel.listarServicios(
				servicioService.obtenerServiciosPorCategoria(registrarTicketModel.getCategoria3Seleccionada()));
	}

	public void listarPrioricadaCombo() {
		registrarTicketModel.listarPrioridades(ticketService.obtenerPrioridades());
	}

	public void iniciarAgregarTicketRapido() {
		registrarTicketModel.reiniciarTicketFormulario();
		listarCategoria1Combo();
		listarPrioricadaCombo();
		registrarTicketModel.inicializarTicket(idCliente);
		registrarTicketModel.asignarCodigoTicket(ticketService.generarNumeracionCodTicket());
		System.out.println("Codigo de nuevo ticket : " + registrarTicketModel.getTicketDTO().getCodigoTicket());
	}

	public void iniciarAgregarTicket(Integer idServicio) {
		registrarTicketModel.reiniciarTicketFormulario();
		listarPrioricadaCombo();
		ServicioDTO serv = servicioService.obtenerServicio(idServicio);
		registrarTicketModel.inicializarTicket(idCliente);
		registrarTicketModel.asignarCodigoTicket(ticketService.generarNumeracionCodTicket());
		System.out.println("Codigo de nuevo ticket : " + registrarTicketModel.getTicketDTO().getCodigoTicket());
		registrarTicketModel.getTicketDTO().setIdServicio(serv.getIdServicio());
		registrarTicketModel.getTicketDTO().setDescNomServico(serv.getNombreServicio());
	}

	public void agregarTicket() {
		try {
			if (validarCampos()) {
				List<ArchivoTicketDTO> listaArchivos = registrarTicketModel.getListaArchivos();
				
				//Modulo de Atencion
				String idServicio = registrarTicketModel.getTicketDTO().getIdServicio().toString();
				System.out.println("Id de Servicio: " + idServicio);
				//
				
				for (ArchivoTicketDTO archivo : listaArchivos) {
					archivo.setIdTicket(ticketService.generarNumeracionCodTicket());
					archivo.setIdMensaje(null);
				}
				ticketService.guardarTicket(registrarTicketModel.getTicketDTO());
				
				//Modulo de Atencion
				trabajadorService.enviarEmail(Integer.parseInt(idServicio));
				//
				
				archivoTicketService.guardarArchivos(listaArchivos);
				PaginaUtil.ejecutar("PF('wgvFormularioTicketRapido').hide()");
				PaginaUtil.ejecutar("PF('wgvFormularioTicket').hide()");
				PaginaUtil.ejecutar("PF('wgvNavegacionTicket').hide()");
				PaginaUtil.mostrarMensajeJSF(1,
						"El Ticket se registro con exito. \n Numero de Ticket: "
								+ registrarTicketModel.getTicketDTO().getCodigoTicket() + ".",
						"El Ticket se registro con exito. \n Numero de Ticket: "
								+ registrarTicketModel.getTicketDTO().getCodigoTicket() + ".");
				System.out.println("********* Ticket registrado *********");
				obtenerTicketsYFavoritos(idCliente);
				PaginaUtil.actualizarComponente("frmFiltro:accordionFiltro");
				PaginaUtil.actualizarComponente("frmPrincipal:frmListaTickets");
			}
		} catch (Exception e) {
			System.out.println("Erro al registrar ticket: " + e);
		}
	}

	public boolean validarCampos() {
		TicketDTO ticketDTO = registrarTicketModel.getTicketDTO();
		boolean camposCorrectos = true;
		if (ticketDTO.getAsuntoTicket().length() == 0) {
			PaginaUtil.mostrarMensajeJSF(2, "Ingrese un Asunto para el ticket.", "Ingrese un Asunto para el ticket.");
			camposCorrectos = false;
		} else {
			if (ticketDTO.getIdServicio() == 0) {
				PaginaUtil.mostrarMensajeJSF(2, "Seleccione un servicio para el ticket.",
						"Seleccione un servicio para el ticket.");
				camposCorrectos = false;
			} else {
				if (ticketDTO.getPrioridadTicket().length() == 0) {
					PaginaUtil.mostrarMensajeJSF(2, "Seleccione la prioridad del ticket.",
							"Seleccione la prioridad del ticket.");
					camposCorrectos = false;
				} else {
					if (ticketDTO.getDetalleTicket().length() == 0) {
						PaginaUtil.mostrarMensajeJSF(2, "Ingrese un detalle para el ticket.",
								"Ingrese un detalle para el ticket.");
						camposCorrectos = false;
					}
				}
			}
		}
		return camposCorrectos;
	}

	public int cantidadTicketEstado(int estadoTicket) {
		int cantidadTicket = 0;

		switch (estadoTicket) {
		case 0:
			cantidadTicket = principalListarModel.getListaTickets().size();
			break;
		case 1:
			cantidadTicket = ticketService.cantidadTicketsEstado(principalListarModel.getListaTickets(),
					EstadoTicket.REGISTRADO.getMensaje());
			break;
		case 2:
			cantidadTicket = ticketService.cantidadTicketsEstado(principalListarModel.getListaTickets(),
					EstadoTicket.CURSO.getMensaje());
			break;
		case 3:
			cantidadTicket = ticketService.cantidadTicketsEstado(principalListarModel.getListaTickets(),
					EstadoTicket.ESPERA.getMensaje());
			break;
		case 4:
			cantidadTicket = ticketService.cantidadTicketsEstado(principalListarModel.getListaTickets(),
					EstadoTicket.FINALIZADO.getMensaje());
			break;
		}

		return cantidadTicket;
	}

	public void filtrarLista(int tipoFiltro) {
		switch (tipoFiltro) {
		case 0:
			principalListarModel.setListaTicketsFiltrados(principalListarModel.getListaTickets());
			break;
		case 1:
			principalListarModel.setListaTicketsFiltrados(ticketService
					.filtrarListaTickets(principalListarModel.getListaTickets(), EstadoTicket.REGISTRADO.getMensaje()));
			break;
		case 2:
			principalListarModel.setListaTicketsFiltrados(ticketService
					.filtrarListaTickets(principalListarModel.getListaTickets(), EstadoTicket.CURSO.getMensaje()));
			break;
		case 3:
			principalListarModel.setListaTicketsFiltrados(ticketService
					.filtrarListaTickets(principalListarModel.getListaTickets(), EstadoTicket.ESPERA.getMensaje()));
			break;
		case 4:
			principalListarModel.setListaTicketsFiltrados(ticketService
					.filtrarListaTickets(principalListarModel.getListaTickets(), EstadoTicket.FINALIZADO.getMensaje()));
			break;
		}
		PaginaUtil.actualizarComponente("frmPrincipal:frmListaTickets:tablaTicket");
	}

	// ***************************************************************************************

	// ********************************* Metodos set y get
	// *********************************

	// ***************************************************************************************

	public TicketService getTicketService() {
		return ticketService;
	}

	public void setTicketService(TicketService ticketService) {
		this.ticketService = ticketService;
	}

	public PrincipalListarModel getPrincipalListarModel() {
		return principalListarModel;
	}

	public void setPrincipalListarModel(PrincipalListarModel principalListarModel) {
		this.principalListarModel = principalListarModel;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public RegistrarTicketModel getTicketFormularioModel() {
		return registrarTicketModel;
	}

	// public UsuarioDTO getUsuarioDTO() {
	// return usuarioDTO;
	// }
	//
	// public void setUsuarioDTO(UsuarioDTO usuario) {
	// usuarioDTO = usuario;
	// }

	public void setTicketFormularioModel(RegistrarTicketModel ticketFormularioModel) {
		this.registrarTicketModel = ticketFormularioModel;
	}

	public CategoriaService getCategoriaService() {
		return categoriaService;
	}

	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}

	public ServicioService getServicioService() {
		return servicioService;
	}

	public void setServicioService(ServicioService servicioService) {
		this.servicioService = servicioService;
	}

	public ArchivoTicketService getArchivoTicketService() {
		return archivoTicketService;
	}

	public void setArchivoTicketService(ArchivoTicketService archivoTicketService) {
		this.archivoTicketService = archivoTicketService;
	}

	public FavoritoService getFavoritoService() {
		return favoritoService;
	}

	public void setFavoritoService(FavoritoService favoritoService) {
		this.favoritoService = favoritoService;
	}

	public TicketFavoritoModel getTicketFavoritoModel() {
		return ticketFavoritoModel;
	}

	public void setTicketFavoritoModel(TicketFavoritoModel ticketFavoritoModel) {
		this.ticketFavoritoModel = ticketFavoritoModel;
	}

	public RegistrarTicketModel getRegistrarTicketModel() {
		return registrarTicketModel;
	}

	public void setRegistrarTicketModel(RegistrarTicketModel registrarTicketModel) {
		this.registrarTicketModel = registrarTicketModel;
	}
}
