package com.unmsm.software.presentacion.model;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.DualListModel;

import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.util.clases.AplicacionUtil;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.TipoOperacion;

public class GrupoAtencionFormularioModel {

	private GrupoAtencionDTO grupoFormulario;
	private TipoOperacion tipoOperacion;

	private GrupoAtencionDTO grupoAtencionSeleccionado;
	private DualListModel<TrabajadorDTO> dualModelListaTrabajadores;
	/**
	 * Lista que me almacena la lista de trabajadores actualmente estan en un
	 * grupo de atencion
	 */
	private List<TrabajadorDTO> listaTrabajadoresTemporal;

	public GrupoAtencionFormularioModel() {
		inicializarDualListModelTrabajadores();
	}

	private void inicializarDualListModelTrabajadores() {
		ArrayList<TrabajadorDTO> listaTrabajadores = new ArrayList<>();
		ArrayList<TrabajadorDTO> listaTrabajadoresSeleccionados = new ArrayList<>();
		dualModelListaTrabajadores = new DualListModel<>(listaTrabajadores, listaTrabajadoresSeleccionados);
	}

	public GrupoAtencionDTO getGrupoAtencionSeleccionado() {
		return grupoAtencionSeleccionado;
	}

	public void setGrupoAtencionSeleccionado(GrupoAtencionDTO grupoAtencionSeleccionado) {
		this.grupoAtencionSeleccionado = grupoAtencionSeleccionado;
	}

	public DualListModel<TrabajadorDTO> getDualModelListaTrabajadores() {
		return dualModelListaTrabajadores;
	}

	public void setDualModelListaTrabajadores(DualListModel<TrabajadorDTO> dualModelListaTrabajadores) {
		this.dualModelListaTrabajadores = dualModelListaTrabajadores;
	}

	public GrupoAtencionDTO getGrupoFormulario() {
		return grupoFormulario;
	}

	public void setGrupoFormulario(GrupoAtencionDTO grupoFormulario) {
		this.grupoFormulario = grupoFormulario;
	}

	public TipoOperacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(TipoOperacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public List<TrabajadorDTO> getListaTrabajadoresTemporal() {
		return listaTrabajadoresTemporal;
	}

	public void setListaTrabajadoresTemporal(List<TrabajadorDTO> listaTrabajadoresTemporal) {
		this.listaTrabajadoresTemporal = listaTrabajadoresTemporal;
	}

	public boolean esOPeracionAgregar() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR)
			return true;
		else
			return false;
	}

	public boolean esOperacionModificar() {
		if (this.tipoOperacion == TipoOperacion.MODIFICAR)
			return true;
		else
			return false;
	}

	public void inicializarGrupoAtencionFormulario() {
		this.grupoFormulario = new GrupoAtencionDTO();
	}

	public void inicializarAgregarUsuarioGrupAtencion() {
		this.grupoAtencionSeleccionado = new GrupoAtencionDTO();
		inicializarDualListModelTrabajadores();
	}

	public String obtenerTituloOperacion() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR) {
			return TipoOperacion.AGREGAR.getMensaje();
		} else if (this.tipoOperacion == TipoOperacion.MODIFICAR) {
			return TipoOperacion.MODIFICAR.getMensaje();
		}
		return "";
	}

	public void cargarDatosIniciales(int codigoGrupoAtencion) {
		this.grupoFormulario.setCodigo(
				AplicacionUtil.formatoCodigoGenerado(Constantes.PREFIJO_COD_GRUP_ATENCION, codigoGrupoAtencion));
	}

	public String idTrabajadorFormateado(Integer idTrabajador) {
		return String.valueOf(idTrabajador.intValue());
	}

}
