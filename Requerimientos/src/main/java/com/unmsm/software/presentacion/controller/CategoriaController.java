package com.unmsm.software.presentacion.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.unmsm.software.negocio.service.CategoriaService;
import com.unmsm.software.presentacion.model.Categoria;
import com.unmsm.software.util.clases.PaginaUtil;


@Controller
@ManagedBean(name="categoriaController")
@ViewScoped
public class CategoriaController implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Autowired 
	CategoriaService categoriaService;
	
    private TreeNode root;
    private TreeNode selectedNode;
    private StreamedContent image;
    private UploadedFile file;
    private Categoria categoria;
    private String modo;
    
    @PostConstruct
    public void init() {
    	categoria= new Categoria();
    	this.cargar(categoriaService.getCategorias());   	
    }
    
	 public void iniciarCrearCategoria(){
	    	try{
	    		modo="Registrar";
	    		categoria= new Categoria();
	    		FacesContext context = FacesContext.getCurrentInstance (); 
	    		ServletContext sc = (ServletContext) context.getExternalContext ().getContext();
	    		String ruta=sc.getRealPath ("/resources/img/default.png");    		
		    	System.out.println(ruta);
		    	Path path = Paths.get(ruta);
		    	byte[] data = Files.readAllBytes(path);
		    	categoria.setImagen(data);
		        image = new DefaultStreamedContent(new ByteArrayInputStream(data), "image/png");
	    	}catch(Exception e){
	    	}		 
	 } 
	 
	 public void iniciarEditarCategoria(){
	    	try{
	    		modo="Editar";
	    		categoria= ((Categoria) selectedNode.getData());
	    		if(categoria.getImagen().length<=0){
		    		FacesContext context = FacesContext.getCurrentInstance (); 
		    		ServletContext sc = (ServletContext) context.getExternalContext ().getContext();
		    		String ruta=sc.getRealPath ("/resources/img/default.png");    		
			    	Path path = Paths.get(ruta);
			    	byte[] data = Files.readAllBytes(path);
			    	categoria.setImagen(data);
			        image = new DefaultStreamedContent(new ByteArrayInputStream(data), "image/png");	    			
	    		}else
	    			image = new DefaultStreamedContent(new ByteArrayInputStream(categoria.getImagen()), "image/png");
	    		
	    	}catch(Exception e){
	    	}		 
	 } 
	 
    public void deleteNode() { 
        selectedNode.getChildren().clear();
        selectedNode.getParent().getChildren().remove(selectedNode);
        selectedNode.setParent(null);
         
        selectedNode = null;
    }
    
    public void deshabilitarCategoria(){
        FacesMessage message = null;  
    	if(selectedNode!=null){
    		categoria= ((Categoria) selectedNode.getData());
    		categoriaService.deshabilitarCategoria(categoria);
    		selectedNode.setType("deshabilitado");
    		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Servicio", "El servicio fue deshabilitado");
    	}
    	else
    		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Servicio", "Hubo un error al deshabilitar ");
    	
        if(message!=null){
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    
    public void habilitarCategoria(){
    	FacesMessage message = null;  
    	if(selectedNode!=null){
    		categoria= ((Categoria) selectedNode.getData());
    		categoriaService.habilitarCategoria(categoria);
    		selectedNode.setType("servicio");
    		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Servicio", "El servicio fue habilitado");
    	}else
    		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Servicio", "Hubo un error al habilitar");
    	

        if(message!=null){
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    
    public void eliminarCategoria(){
    	FacesMessage message = null;  
    	if(selectedNode!=null){
    		categoria= ((Categoria) selectedNode.getData());   		
    		categoriaService.eliminarCategoria(categoria);
            selectedNode.setType("eliminado");     		
    		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Servicio", categoria.getDescripcion() +"El servicio fue eliminado");
    	}else
    		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Servicio", "Hubo un error al eliminar");
    	
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void cargar (List<Categoria> lista) {     
        root = new DefaultTreeNode("Root", null);
    	List<Categoria> respaldo= lista;
    	int j=0;
    	for(int i=0; i< lista.size();i++){
    		Categoria categoria = lista.get(i);
    		if(categoria.getNivel()==1 || i==(lista.size()-1)){
    			if(i!=0){
    				List<Categoria> categoriasHijo=null;
    				if(i==(lista.size()-1))
    					 categoriasHijo = respaldo.subList(j, i+1);    				
    				else
    					categoriasHijo = respaldo.subList(j, i);
    				
	    			llenar_hijos(root, categoriasHijo , 0 );
    			}
    			j=i;    			    			
    		}
    	}    	
    }
   
    private void llenar_hijos(TreeNode nodo_raiz , List<Categoria> lista_hijos, int indice){
        
    	Categoria uni_pad = (Categoria) lista_hijos.get( indice );
        int cant = lista_hijos.size();
        int i = indice;
        TreeNode nodo_padre;
        if(uni_pad.getNivel()==3){
        	if(uni_pad.getEstado()==2)
        	 nodo_padre = new DefaultTreeNode("servicio", uni_pad, nodo_raiz);
        	else
        		nodo_padre = new DefaultTreeNode("deshabilitado", uni_pad, nodo_raiz);
        }else{
             nodo_padre = new DefaultTreeNode("categoria", uni_pad, nodo_raiz);
        }        
        while( i < cant-1 ){
        	
        	Categoria hijo = (Categoria) lista_hijos.get(i+1) ;
            
            if(uni_pad.getNivel() >= hijo.getNivel() ){
                break;
            }
                if(uni_pad.getId_categoria() == hijo.getCat_padre() ){
                    llenar_hijos(nodo_padre, lista_hijos,  i+1 );
                }
            i++;
        }        
    }
    
    public void fileUploadListener(FileUploadEvent event) {
		this.file = event.getFile();
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        byte[] file = event.getFile().getContents();
        categoria.setImagen(file);
        String contentType = event.getFile().getContentType();
        image = new DefaultStreamedContent(new ByteArrayInputStream(file), contentType);

     }
    
    public void procesarCategoria(ActionEvent event){
    	if(this.modo.equals("Registrar")){
    		registrarCategoria(event);
    	}else{
        	if(this.modo.equals("Editar")){
        		editarCategoria(event);
        	}
    	}
    }
    
    public void registrarCategoria(ActionEvent event) {
        FacesMessage message = null;      
        if(categoria.getDescripcion() == null || categoria.getDescripcion().equals("")) {
            //message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Categoria", "Debe registrar la descripción ");
        } else {
	        	Categoria padre=(Categoria) selectedNode.getData();
	        	categoria.setCat_padre(padre.getId_categoria());
	        	categoria.setEstado(2);
	        	categoria.setNivel(padre.getNivel()+1);
	        	if(padre.getNivel()==1){
	        		categoria.setNiv1(padre.getId_categoria());
	        		categoria.setNiv2(categoria.getId_categoria());
		        	//TreeNode nodeCreado= new DefaultTreeNode("categoria",categoria,selectedNode);		        	
	        	}
	        	else{
	        		if(padre.getNivel()==2){
	        			Categoria padre_superior=(Categoria)selectedNode.getParent().getData();
	            		categoria.setNiv1(padre_superior.getId_categoria());
	            		categoria.setNiv2(padre.getId_categoria());
	            		categoria.setNiv3(categoria.getId_categoria());
	    	        	//TreeNode nodeCreado= new DefaultTreeNode("servicio",categoria,selectedNode);
	        		}
	        	}   
	        	TreeNode nodeCreado=null;	        	
	        	try{
	        		categoriaService.insertarCategoria(categoria);
//	        		System.out.println("id: "+categoria.getId_categoria());
//	        		categoria.setId_categoria(categoriaService.obtenerMaxIdCategoria());
	        		if(padre.getNivel()==1)
	        			 nodeCreado= new DefaultTreeNode("categoria",categoria,selectedNode);
	        		else
	        			if(padre.getNivel()==2)
	        			  nodeCreado= new DefaultTreeNode("servicio",categoria,selectedNode);
	        		
	        		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria", "La categoría fue registrada");
	        	}catch(Exception e){
	        		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Categoria", "Ha ocurrido un error al registrar");
	        	}
	        	PaginaUtil.ejecutar("PF('dlg').hide()");
	        	//this.cargar(categoriaService.getCategorias());
	            
        }  
        if(message!=null){
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }
    
    public void editarCategoria(ActionEvent event) {
        FacesMessage message = null;      
        if(categoria.getDescripcion() == null || categoria.getDescripcion().equals("")) {
            //message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Categoria", "Debe registrar la descripción ");

        } else {
        		String descripcion=((Categoria)selectedNode.getData()).getDescripcion();
        		byte[] imagen=((Categoria)selectedNode.getData()).getImagen();
        		
        		try{
    	        	categoriaService.editarCategoria(categoria);
    	        	((Categoria)selectedNode.getData()).setDescripcion(categoria.getDescripcion());
    	        	((Categoria)selectedNode.getData()).setImagen(categoria.getImagen());
    	        	message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Categoria", "La categoría fue editada");
        		}catch(Exception e){
    	        	((Categoria)selectedNode.getData()).setDescripcion(descripcion);
    	        	((Categoria)selectedNode.getData()).setImagen(imagen);
        			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Categoria", "Ha ocurrido un error al editar");
        		}

	        	PaginaUtil.ejecutar("PF('dlg').hide()");

	        	//this.cargar(categoriaService.getCategorias());
	            
        }         
        if(message!=null){
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }   
    
	public StreamedContent getImage() {
		return image;
	}

	public void setImage(StreamedContent image) {
		this.image = image;
	}    
	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	} 
        
}
