package com.unmsm.software.presentacion.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
 
public class KeyAuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {
 
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
    	String message=exception.getMessage();
    	if(message.equals("Maximum sessions of 1 for this principal exceeded")) message="Ya se ha iniciado sesion con esta cuenta";
        setDefaultFailureUrl("/faces/index.xhtml?status=" + message);
        super.onAuthenticationFailure(request, response, exception);
    }
 
}