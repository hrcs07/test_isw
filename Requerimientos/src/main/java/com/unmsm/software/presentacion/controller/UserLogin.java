package com.unmsm.software.presentacion.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.UsuariosService;
import com.unmsm.software.persistencia.dto.UsuarioDTO;
import com.unmsm.software.presentacion.model.UserModel;
import com.unmsm.software.util.clases.PaginaUtil;
 
@Controller("userLogin")
@ViewScoped
public class UserLogin implements Serializable{
	private static final long serialVersionUID = 1L;

	@Autowired
	private UsuariosService usuariosService;
    
    private UserModel userModel;
    
    @PostConstruct
    public void init(){
    	userModel = new UserModel();
    }

    public void login() throws IOException {
//        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
//        boolean loggedIn = false;
        String redirect="";
        UsuarioDTO usuario = usuariosService.buscarUsuario(userModel.getUsuarioDTO().getUsername(), userModel.getUsuarioDTO().getPassword());
        
        if (usuario != null) {
        	System.out.println(usuario.getDesRolUsuario()+" "+usuario.getCtRol()+" "+usuario.getNombre());
        	
        	switch(usuario.getDesRolUsuario()){
	        	case "Admin": redirect="faces/principal.xhtml";			break;
	        	case "Cliente" : redirect="faces/principalUsuario.xhtml";	break;
	        	case "Tecnico" : redirect="faces/plantillaTecnico/bandejaMain.xhtml";	break;
        	}
        	
//            loggedIn = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido ", usuario.getNombre()+" "+usuario.getApellidos());
//            PaginaUtil.mostrarMensajeJSF(1, "Bienvenido", usuario.getNombre()+" "+usuario.getApellidos());
            
            
            FacesContext contex = FacesContext.getCurrentInstance();
            contex.getExternalContext (). getFlash (). setKeepMessages ( true );
            contex.addMessage(null, message);
            PaginaUtil.redireccionar(redirect);
//            contex.getExternalContext().redirect(redirect);
        } else {
//            loggedIn = false;
//            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
            PaginaUtil.mostrarMensajeJSF(2, "Loggin Error", "Invalid credentials");
        }
       
//        FacesContext.getCurrentInstance().addMessage(null, message);
//        context.addCallbackParam("loggedIn", loggedIn);
    }

	public UsuariosService getUsuariosService() {
		return usuariosService;
	}

	public void setUsuariosService(UsuariosService usuariosService) {
		this.usuariosService = usuariosService;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}
}
