package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.dto.TablaMaestraDTO;
import com.unmsm.software.persistencia.dto.UsuarioDTO;
import com.unmsm.software.util.clases.TablaMaestra;
import com.unmsm.software.util.clases.TipoOperacion;

/**
 * @author renzo
 *
 */
public class UsuariosFormularioModel {

	private UsuarioDTO usuarioFormulario;

	private TipoOperacion tipoOperacion;

	private List<TablaMaestraDTO> listaRolesUsuarios;

	public UsuariosFormularioModel() {
		// TODO Auto-generated constructor stub
	}

	public UsuarioDTO getUsuarioFormulario() {
		return usuarioFormulario;
	}

	public void setUsuarioFormulario(UsuarioDTO usuarioFormulario) {
		this.usuarioFormulario = usuarioFormulario;
	}

	public TipoOperacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(TipoOperacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public List<TablaMaestraDTO> getListaRolesUsuarios() {
		return listaRolesUsuarios;
	}

	public void setListaRolesUsuarios(List<TablaMaestraDTO> listaRolesUsuarios) {
		this.listaRolesUsuarios = listaRolesUsuarios;
	}

	public boolean esOPeracionAgregar() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR)
			return true;
		else
			return false;
	}

	public boolean esOperacionModificar() {
		if (this.tipoOperacion == TipoOperacion.MODIFICAR)
			return true;
		else
			return false;
	}

	public String obtenerTituloOperacion() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR) {
			return TipoOperacion.AGREGAR.getMensaje();
		} else if (this.tipoOperacion == TipoOperacion.MODIFICAR) {
			return TipoOperacion.MODIFICAR.getMensaje();
		}
		return "";
	}

	public void inicializarUsuarioFormulario() {
		this.usuarioFormulario = new UsuarioDTO();
	}
	
	public String mensajeCambioEstadoServicio(UsuarioDTO usuarioDTO){
		return (usuarioDTO.isEstadoUsuario()?"El usuario se habilito con exito.":"El usuario se deshabilito con exito.");
	}

}
