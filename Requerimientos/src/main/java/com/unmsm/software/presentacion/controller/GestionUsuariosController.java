package com.unmsm.software.presentacion.controller;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.GrupoAtencionService;
import com.unmsm.software.negocio.service.RolService;
import com.unmsm.software.negocio.service.TrabajadorGrupoAtencionService;
import com.unmsm.software.negocio.service.TrabajadorService;
import com.unmsm.software.negocio.service.UsuariosService;
import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.persistencia.dto.UsuarioDTO;
import com.unmsm.software.presentacion.model.GrupoAtencionFormularioModel;
import com.unmsm.software.presentacion.model.GrupoAtencionListarModel;
import com.unmsm.software.presentacion.model.UsuariosFormularioModel;
import com.unmsm.software.presentacion.model.UsuariosListarModel;
import com.unmsm.software.util.clases.PaginaUtil;
import com.unmsm.software.util.clases.TipoMensajeJSF;
import com.unmsm.software.util.clases.TipoOperacion;
import com.unmsm.software.util.excepciones.NegocioExcepcion;

/**
 * @author renzo
 *
 */
@Controller("gestionUsuariosController")
@ViewScoped
public class GestionUsuariosController implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Servicios que utiliza el controlador para gestionar los usuarios y grupos
	 * de atencion
	 */
	@Autowired
	private UsuariosService usuariosService;

	@Autowired
	private RolService rolService;

	@Autowired
	private GrupoAtencionService grupoAtencionService;

	@Autowired
	private TrabajadorService trabajadorService;

	@Autowired
	private TrabajadorGrupoAtencionService trabajadorGrupoAtencionService;

	/**
	 * Models para la gestion de usuarios
	 */
	private UsuariosListarModel usuarioModel;

	private UsuariosFormularioModel usuarioFormularioModel;

	/**
	 * Models para la gestion de grupos de usuarios
	 */
	private GrupoAtencionListarModel grupoAtencionModel;

	private GrupoAtencionFormularioModel grupoAtencionFormularioModel;

	@PostConstruct
	public void init() {
		this.inicializarUsuarioModels();
		this.inicializarGrupoAtencionModels();
		this.obtenerUsuarios();
		this.obtenerGruposAtencion();
	}

	public void iniciarFormularioAgregarUsuario() {
		this.usuarioFormularioModel.inicializarUsuarioFormulario();
		this.cargarDatosInicialesFormularioUsuario();
		this.usuarioFormularioModel.setTipoOperacion(TipoOperacion.AGREGAR);
	}

	private void cargarDatosInicialesFormularioUsuario() {
		this.usuarioFormularioModel.setListaRolesUsuarios(rolService.obtenerRoles());
	}

	public void agregarUsuario() {
		try {
			usuariosService.guardarUsuario(usuarioFormularioModel.getUsuarioFormulario());
			this.usuarioFormularioModel.inicializarUsuarioFormulario();
			obtenerUsuarios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El usuario se agrego con exito.");
			PaginaUtil.ejecutar("PF('wgvFormularioUsuario').hide()");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionUsuario:messages");
		}

	}

	public void iniciarFormularioModificarUsuario(UsuarioDTO usuarioSeleccionado) {
		this.usuarioFormularioModel.setUsuarioFormulario(usuarioSeleccionado);
		this.usuarioModel.setUsuarioFormularioTemporal(usuarioSeleccionado);
		this.cargarDatosInicialesFormularioUsuario();
		this.usuarioFormularioModel.setTipoOperacion(TipoOperacion.MODIFICAR);
	}

	public void modificarUsuario() {
		try {
			usuariosService.actualizarUsuario(usuarioFormularioModel.getUsuarioFormulario(),
					usuarioModel.getUsuarioFormularioTemporal());
			this.usuarioFormularioModel.inicializarUsuarioFormulario();
			obtenerUsuarios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El usuario se modifico con exito.");
			PaginaUtil.ejecutar("PF('wgvFormularioUsuario').hide()");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionUsuario:messages");
		}
	}

	public void iniciarFormularioEliminarUsuario(UsuarioDTO usuarioSeleccionado) {
		this.usuarioFormularioModel.setUsuarioFormulario(usuarioSeleccionado);
		this.usuarioFormularioModel.setTipoOperacion(TipoOperacion.ELIMINAR);
	}

	public void eliminarUsuario() {
		try {
			usuariosService.eliminarUsuario(usuarioFormularioModel.getUsuarioFormulario());
			obtenerUsuarios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El usuario se elimino con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.ejecutar("PF('dgEliminarUsuario').hide()");
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionUsuario:messages");
		}
	}

	public void deshabilitarUsuario(UsuarioDTO usuarioDTO) {
		try {
			usuariosService.deshabilitarUsuario(usuarioDTO);
			obtenerUsuarios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					usuarioFormularioModel.mensajeCambioEstadoServicio(usuarioDTO));
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionUsuario:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionUsuario:messages");
		}
	}

	public void ejecutarUsuarioTipoOperacion() {
		if (this.usuarioFormularioModel.esOPeracionAgregar()) {
			agregarUsuario();
		} else if (this.usuarioFormularioModel.esOperacionModificar()) {
			modificarUsuario();
		}
	}

	/**
	 * ---------------------------------- Metodos para la gestion de grupos de
	 * atencion -------------------------------------------
	 */
	public void iniciarFormularioAgregarGrupoAtencion() {
		this.grupoAtencionFormularioModel.inicializarGrupoAtencionFormulario();
		// this.cargarDatosInicialesFormularioGrupoAtencion();
		this.grupoAtencionFormularioModel
				.cargarDatosIniciales(grupoAtencionService.generarNumeracionCodGrupoAtencion());
		this.grupoAtencionFormularioModel.setTipoOperacion(TipoOperacion.AGREGAR);
	}

	// private void cargarDatosInicialesFormularioGrupoAtencion() {
	// this.usuarioFormularioModel.setListaRolesUsuarios(rolService.obtenerRoles());
	// }

	public void agregarGrupoAtencion() {
		try {
			grupoAtencionService.guardarGrupoAtencion(grupoAtencionFormularioModel.getGrupoFormulario());
			this.grupoAtencionFormularioModel.inicializarGrupoAtencionFormulario();
			obtenerGruposAtencion();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El grupo atención se agrego con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.ejecutar("PF('wgvFormularioGrupAtencion').hide()");
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionGruposAtencion:messages");
		}

	}

	public void iniciarFormularioModificarGrupoAtencion(GrupoAtencionDTO grupoAtencionSeleccionado) {
		this.grupoAtencionFormularioModel.setGrupoFormulario(grupoAtencionSeleccionado);
		// this.cargarDatosInicialesFormularioGrupoAtencion();
		this.grupoAtencionFormularioModel.setTipoOperacion(TipoOperacion.MODIFICAR);
	}

	public void modificarGrupoAtencion() {
		try {
			grupoAtencionService.actualizarGrupoAtencion(grupoAtencionFormularioModel.getGrupoFormulario());
			this.grupoAtencionFormularioModel.inicializarGrupoAtencionFormulario();
			obtenerGruposAtencion();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El grupo atención se modifico con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.ejecutar("PF('wgvFormularioGrupAtencion').hide()");
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionGruposAtencion:messages");
		}
	}

	public void iniciarFormularioEliminarGrupoAtencion(GrupoAtencionDTO grupoAtencionSeleccionado) {
		this.grupoAtencionFormularioModel.setGrupoFormulario(grupoAtencionSeleccionado);
		this.grupoAtencionFormularioModel.setTipoOperacion(TipoOperacion.ELIMINAR);
	}

	public void eliminarGrupoAtencion() {
		try {
			grupoAtencionService.eliminarGrupoAtencion(grupoAtencionFormularioModel.getGrupoFormulario());
			obtenerGruposAtencion();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El grupo atención se elimino con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.ejecutar("PF('dgEliminarGrupAtencion').hide()");
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionGruposAtencion:messages");
		}
	}

	public void iniciarFormularioAgregarUsuarioAGrupoAtencion(GrupoAtencionDTO grupoAtencionSeleccionado) {
		this.grupoAtencionFormularioModel.setGrupoAtencionSeleccionado(grupoAtencionSeleccionado);
		this.cargarDatosInicialesFormularioAgregarUsuarioGrupAtencion();
	}

	private void cargarDatosInicialesFormularioAgregarUsuarioGrupAtencion() {
		this.grupoAtencionFormularioModel.getDualModelListaTrabajadores()
				.setSource(trabajadorService.obtenerTrabajadores());
		this.grupoAtencionFormularioModel.getDualModelListaTrabajadores().setTarget(trabajadorGrupoAtencionService
				.obtenerTrabajadoresGrupoAtencion(grupoAtencionFormularioModel.getGrupoAtencionSeleccionado()));
		this.grupoAtencionFormularioModel.setListaTrabajadoresTemporal(trabajadorGrupoAtencionService
				.obtenerTrabajadoresGrupoAtencion(grupoAtencionFormularioModel.getGrupoAtencionSeleccionado()));
	}

	public void agregarUsuarioAGrupoAtencion() {
		try {
			trabajadorGrupoAtencionService.agregarUsuarioAGrupoAtencion(
					grupoAtencionFormularioModel.getDualModelListaTrabajadores().getTarget(),
					grupoAtencionFormularioModel.getListaTrabajadoresTemporal(),
					grupoAtencionFormularioModel.getGrupoAtencionSeleccionado());
			obtenerGruposAtencion();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"La asignación se realizó con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		} finally {
			PaginaUtil.ejecutar("PF('wgvFormularioAgrUsrGrupAtencion').hide()");
			PaginaUtil.actualizarComponente("tabGestionUsuGpAtencion:frmGestionGruposAtencion:messages");
		}
	}

	public void ejecutarGrupoAtencionTipoOperacion() {
		if (this.grupoAtencionFormularioModel.esOPeracionAgregar()) {
			agregarGrupoAtencion();
		} else if (this.grupoAtencionFormularioModel.esOperacionModificar()) {
			modificarGrupoAtencion();
		}
	}

	public UsuariosService getUsuariosService() {
		return usuariosService;
	}

	public void setUsuariosService(UsuariosService usuariosService) {
		this.usuariosService = usuariosService;
	}

	public UsuariosListarModel getUsuarioModel() {
		return usuarioModel;
	}

	public void setUsuarioModel(UsuariosListarModel usuarioModel) {
		this.usuarioModel = usuarioModel;
	}

	public void obtenerUsuarios() {
		this.usuarioModel.setListaUsuarios(usuariosService.obtenerUsuarios());
	}

	public void obtenerGruposAtencion() {
		this.grupoAtencionModel.setListaGrupoAtencion(grupoAtencionService.obtenerGruposAtencion());
	}

	public UsuariosFormularioModel getUsuarioFormularioModel() {
		return usuarioFormularioModel;
	}

	public void setUsuarioFormularioModel(UsuariosFormularioModel usuarioFormularioModel) {
		this.usuarioFormularioModel = usuarioFormularioModel;
	}

	public GrupoAtencionListarModel getGrupoAtencionModel() {
		return grupoAtencionModel;
	}

	public void setGrupoAtencionModel(GrupoAtencionListarModel grupoAtencionModel) {
		this.grupoAtencionModel = grupoAtencionModel;
	}

	public GrupoAtencionFormularioModel getGrupoAtencionFormularioModel() {
		return grupoAtencionFormularioModel;
	}

	public void setGrupoAtencionFormularioModel(GrupoAtencionFormularioModel grupoAtencionFormularioModel) {
		this.grupoAtencionFormularioModel = grupoAtencionFormularioModel;
	}

	private void inicializarUsuarioModels() {
		this.usuarioModel = new UsuariosListarModel();
		this.usuarioFormularioModel = new UsuariosFormularioModel();
	}

	private void inicializarGrupoAtencionModels() {
		this.grupoAtencionModel = new GrupoAtencionListarModel();
		this.grupoAtencionFormularioModel = new GrupoAtencionFormularioModel();
	}
}
