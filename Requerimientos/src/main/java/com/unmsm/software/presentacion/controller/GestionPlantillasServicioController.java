package com.unmsm.software.presentacion.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.ComponenteService;
import com.unmsm.software.negocio.service.ServicioService;
import com.unmsm.software.persistencia.dto.ComponenteDTO;
import com.unmsm.software.persistencia.dto.SlaDTO;
import com.unmsm.software.presentacion.model.PlantillaFormularioModel;
import com.unmsm.software.presentacion.model.PlantillaListarModel;
import com.unmsm.software.util.clases.PaginaUtil;
import com.unmsm.software.util.clases.TipoMensajeJSF;
import com.unmsm.software.util.clases.TipoOperacion;
import com.unmsm.software.util.excepciones.NegocioExcepcion;

@Controller("gestionPlantillasServicioController")
@Scope("view")
public class GestionPlantillasServicioController {

	@Autowired
	private ServicioService servicioService;

	@Autowired
	private ComponenteService componenteService;

	private PlantillaListarModel plantillaModel;
	private PlantillaFormularioModel plantillaFormularioModel;

	@PostConstruct
	public void init() {
		inicializarModels();
		obtenerServicios();
	}

	public void iniciarFormularioAgregar() {
		this.plantillaFormularioModel.inicializarComponenteFormulario();
		// this.plantillaFormularioModel.cargarDatosIniciales(slaService.generarNumeracionCodSLA());
		this.plantillaFormularioModel.setTipoOperacion(TipoOperacion.AGREGAR);
	}

	public void agregarComponente() {
		try {
			componenteService.guardarComponente(plantillaFormularioModel.getComponenteFormulario());
			PaginaUtil.ejecutar("PF('wgvFormularioSLA').hide()");
			this.plantillaFormularioModel.inicializarComponenteFormulario();
			obtenerComponentesPorServicio();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El Componente se agrego con exito.");
			PaginaUtil.actualizarComponente("frmMantenerSLA:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}

	}

	public void iniciarFormularioModificar(ComponenteDTO componenteSeleccionado) {
		this.plantillaFormularioModel.setComponenteFormulario(componenteSeleccionado);
		this.plantillaFormularioModel.setTipoOperacion(TipoOperacion.MODIFICAR);
	}

	public void modificarComponente() {
		try {
			componenteService.actualizarComponente(plantillaFormularioModel.getComponenteFormulario());
			PaginaUtil.ejecutar("PF('wgvFormularioSLA').hide()");
			this.plantillaFormularioModel.inicializarComponenteFormulario();
			obtenerComponentesPorServicio();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El Componente se modifico con exito.");
			PaginaUtil.actualizarComponente("frmMantenerSLA:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}
	}

	public void iniciarFormularioEliminar(ComponenteDTO componenteSeleccionado) {
		this.plantillaFormularioModel.setComponenteFormulario(componenteSeleccionado);
		this.plantillaFormularioModel.setTipoOperacion(TipoOperacion.ELIMINAR);
	}

	public void eliminarComponente() {
		try {
			componenteService.eliminarComponente(plantillaFormularioModel.getComponenteFormulario());
			PaginaUtil.ejecutar("PF('dgEliminarSLA').hide()");
			obtenerComponentesPorServicio();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El Componente se elimino con exito.");
			PaginaUtil.actualizarComponente("frmMantenerSLA:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}
	}

	public void ejecutarTipoOperacion() {
		if (this.plantillaFormularioModel.esOPeracionAgregar()) {
			agregarComponente();
		} else if (this.plantillaFormularioModel.esOperacionModificar()) {
			modificarComponente();
		}
	}
	public void visualizarDatosPorServicio(){
		obtenerComponentesPorServicio();
		plantillaModel.obtenerNombreServicioSeleccionado();
	}

	public void obtenerComponentesPorServicio() {
		this.plantillaModel.setListaComponentes(componenteService.obtenerComponentes(plantillaModel.getServicioSeleccionado()));
	}
	public void obtenerServicios(){
		this.plantillaModel.setListaServicios(servicioService.obtenerServicios());
	}

	public void inicializarModels() {
		this.plantillaModel = new PlantillaListarModel();
		this.plantillaFormularioModel = new PlantillaFormularioModel();
	}

	public PlantillaListarModel getPlantillaModel() {
		return plantillaModel;
	}

	public void setPlantillaModel(PlantillaListarModel plantillaModel) {
		this.plantillaModel = plantillaModel;
	}

	public PlantillaFormularioModel getPlantillaFormularioModel() {
		return plantillaFormularioModel;
	}

	public void setPlantillaFormularioModel(PlantillaFormularioModel plantillaFormularioModel) {
		this.plantillaFormularioModel = plantillaFormularioModel;
	}

}
