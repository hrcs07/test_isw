package com.unmsm.software.presentacion.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;

import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.util.clases.AplicacionUtil;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.EstadoTicket;

public class TicketFormularioModel {
	private TicketDTO ticketDTO;

	private Integer categoria1Seleccionada;
	private List<SelectItem> listItemCategoria1;

	private Integer categoria2Seleccionada;
	private List<SelectItem> listItemCategoria2;

	private List<SelectItem> listItemServicio;

	public TicketFormularioModel() {
		
	}

	public void inicializarTicket(Integer idCliente) {
		ticketDTO = new TicketDTO();
		ticketDTO.setIdCliente(idCliente);
		ticketDTO.setEstadoTicket(EstadoTicket.REGISTRADO.getMensaje());
		ticketDTO.setFechaTicket(new Date());
		ticketDTO.setArchivoAdjunto(null);
	}

	public void asignarCodigoTicket(int codigoTicket) {
		ticketDTO.setCodigoTicket(AplicacionUtil.formatoCodigoGenerado(Constantes.PREFIJO_COD_TICKET, codigoTicket));
	}

	public void cargarArchivo(FileUploadEvent event) {

	}

	public void inicializarCategoria2() {
		listItemServicio = new ArrayList<>();
		categoria2Seleccionada = null;
		ticketDTO.setIdServicio(null);
	}
	
	public void inicializarCategoria3() {
		ticketDTO.setIdServicio(null);
	}
	
	public void obtenerListaCategoria1(List<Categoria> lista) {
		if (lista != null && lista.size() > 0) {
			listItemCategoria1 = new ArrayList<SelectItem>();
			for (Categoria categoriaDTO : lista) {
				SelectItem item = new SelectItem();
				item.setLabel(categoriaDTO.getDescripcion());
				item.setValue(categoriaDTO.getId_categoria());
				listItemCategoria1.add(item);
			}
		}
	}

	public void obtenerListaCategoria2(List<Categoria> lista) {
		if (lista != null && lista.size() > 0) {
			listItemCategoria2 = new ArrayList<SelectItem>();
			for (Categoria categoriaDTO : lista) {
				SelectItem item = new SelectItem();
				item.setLabel(categoriaDTO.getDescripcion());
				item.setValue(categoriaDTO.getId_categoria());
				listItemCategoria2.add(item);
			}
		}
	}

	public void obtenerListaServicio(List<ServicioDTO> lista) {
		if (lista != null && lista.size() > 0) {
			listItemServicio = new ArrayList<SelectItem>();
			for (ServicioDTO servicioDTO : lista) {
				SelectItem item = new SelectItem();
				item.setLabel(servicioDTO.getNombreServicio());
				item.setValue(servicioDTO.getIdServicio());
				listItemServicio.add(item);
			}
		}
	}

	public TicketDTO getTicketDTO() {
		return ticketDTO;
	}

	public void setTicketDTO(TicketDTO ticketDTO) {
		this.ticketDTO = ticketDTO;
	}

	public Integer getCategoria1Seleccionada() {
		return categoria1Seleccionada;
	}

	public void setCategoria1Seleccionada(Integer categoria1Seleccionada) {
		this.categoria1Seleccionada = categoria1Seleccionada;
	}

	public List<SelectItem> getCategoria1() {
		return listItemCategoria1;
	}

	public void setCategoria1(List<SelectItem> categoria1) {
		this.listItemCategoria1 = categoria1;
	}

	public Integer getCategoria2Seleccionada() {
		return categoria2Seleccionada;
	}

	public void setCategoria2Seleccionada(Integer categoria2Seleccionada) {
		this.categoria2Seleccionada = categoria2Seleccionada;
	}

	public List<SelectItem> getCategoria2() {
		return listItemCategoria2;
	}

	public void setCategoria2(List<SelectItem> categoria2) {
		this.listItemCategoria2 = categoria2;
	}

	public List<SelectItem> getServicio() {
		return listItemServicio;
	}

	public void setServicio(List<SelectItem> servicio) {
		this.listItemServicio = servicio;
	}

}
