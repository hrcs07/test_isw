package com.unmsm.software.presentacion.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.ArchivoTicketService;
import com.unmsm.software.negocio.service.CategoriaService;
import com.unmsm.software.negocio.service.ClienteService;
import com.unmsm.software.negocio.service.MensajeService;
import com.unmsm.software.negocio.service.ServicioService;
import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;
import com.unmsm.software.persistencia.dto.MensajeDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.presentacion.model.DetalleTicketModel;
import com.unmsm.software.util.clases.PaginaUtil;

@Controller("detalleTicketController")
@ViewScoped
public class DetalleTicketController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private ServicioService servicioService;
	
	@Autowired
	private CategoriaService categoriaService;
	
	@Autowired
	private MensajeService mensajeService;
	
	@Autowired
	private ArchivoTicketService archivoTicketService;
	
	@Autowired
	private ClienteService clienteService;

	//Usuario el cual se logea como cliente
	//public UsuarioDTO usuarioDTO;
	private Integer idCliente = 1;
	
	private DetalleTicketModel detalleTicketModel;
	
	
	@PostConstruct
	public void init() {
		inicializarModels();
	}
	
	public void inicializarModels() {
		detalleTicketModel = new DetalleTicketModel();
	}
	
	public String cadenaCategorias(){
		String cadenaCategoria = "";
		if(detalleTicketModel.getTicketSeleccionado().getIdServicio() != null){
			ServicioDTO serv = servicioService.obtenerServicio(detalleTicketModel.getTicketSeleccionado().getIdServicio());
			cadenaCategoria = categoriaService.secuenciaCategorias(serv.getIdCategoria())+detalleTicketModel.getTicketSeleccionado().getDescNomServico();
		}
			
		return cadenaCategoria;
	}
	
	public void obtenerMensajesTicket(TabChangeEvent event){
		try{
			if (event.getTab().getId().equals("tabMensajes")) {
				System.out.println("Asunto: "+detalleTicketModel.getTicketSeleccionado().getAsuntoTicket());
				if(detalleTicketModel.getTicketSeleccionado().getIdTicket() != null){
					detalleTicketModel.setListaMensajesTicket( mensajeService.obtenerMensajesTicket(detalleTicketModel.getTicketSeleccionado().getIdTicket()) );
					
					for(MensajeDTO mensajeDTO : detalleTicketModel.getListaMensajesTicket()){
						mensajeDTO.setListaArchivosMensaje(archivoTicketService.buscarArchivosMensaje(mensajeDTO.getIdMensaje()));
					}
					
					PaginaUtil.actualizarComponente("frmTabViewDetalle:tvDetalleTicket");
				}
				detalleTicketModel.inicializarMensaje( clienteService.buscarCliente(idCliente).getIdUsuario() );
			}
		}catch(Exception e){
			System.out.println("Error al obtener mensajes: "+e);
		}
	}
	
	public void registrarMensaje(){
		try {
			if(detalleTicketModel.getMensajeDTO().getMensaje().trim().length()!=0){
				mensajeService.registrarMensaje(detalleTicketModel.getMensajeDTO());
				
				List<ArchivoTicketDTO> listaArchivos = detalleTicketModel.getListaArchivos();
				for(ArchivoTicketDTO archivo : listaArchivos){
					archivo.setIdMensaje( mensajeService.ultimoMensaje() );
					archivo.setIdTicket(null);
				}
				archivoTicketService.guardarArchivos(listaArchivos);
				
				PaginaUtil.mostrarMensajeJSF(1, "El mensaje se registro con exito.", "El mensaje se registro con exito.");
				System.out.println("********* Mensaje registrado *********");
				
				detalleTicketModel.setListaMensajesTicket( mensajeService.obtenerMensajesTicket(detalleTicketModel.getTicketSeleccionado().getIdTicket()) );
				
				for(MensajeDTO mensajeDTO : detalleTicketModel.getListaMensajesTicket()){
					mensajeDTO.setListaArchivosMensaje(archivoTicketService.buscarArchivosMensaje(mensajeDTO.getIdMensaje()));
				}
				
				detalleTicketModel.setListaArchivos(new ArrayList<ArchivoTicketDTO>());
				PaginaUtil.actualizarComponente("frmTabViewDetalle:tvDetalleTicket");
				
			}else{
				PaginaUtil.mostrarMensajeJSF(1, "Es necesario que escriba un mensaje en el campo de texto para hablar con el técnico", "El mensaje se registro con exito.");
			}
		} catch (Exception e) {
			System.out.println("Erro al registrar mensaje: "+e);
		}
	}
		
	//  ***************************************************************************************
	
	//  *********************************  Metodos set y get  *********************************
	
	//  ***************************************************************************************
	
	public ServicioService getServicioService() {
		return servicioService;
	}

	public void setServicioService(ServicioService servicioService) {
		this.servicioService = servicioService;
	}

	public CategoriaService getCategoriaService() {
		return categoriaService;
	}

	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}

	public DetalleTicketModel getDetalleTicketModel() {
		return detalleTicketModel;
	}

	public void setDetalleTicketModel(DetalleTicketModel detalleTicketModel) {
		this.detalleTicketModel = detalleTicketModel;
	}

	public MensajeService getMensajeService() {
		return mensajeService;
	}

	public void setMensajeService(MensajeService mensajeService) {
		this.mensajeService = mensajeService;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public ArchivoTicketService getArchivoTicketService() {
		return archivoTicketService;
	}

	public void setArchivoTicketService(ArchivoTicketService archivoTicketService) {
		this.archivoTicketService = archivoTicketService;
	}

	public ClienteService getClienteService() {
		return clienteService;
	}

	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
}
