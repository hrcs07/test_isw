package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.UsuarioDTO;
import com.unmsm.software.util.clases.Constantes;

public class UsuariosListarModel {

	private List<UsuarioDTO> listaUsuarios;

	private List<UsuarioDTO> listaUsuariosFiltrados;

	private UsuarioDTO usuarioFormularioTemporal;

	public UsuariosListarModel() {
		// TODO Auto-generated constructor stub
	}

	public UsuarioDTO getUsuarioFormularioTemporal() {
		return usuarioFormularioTemporal;
	}

	public void setUsuarioFormularioTemporal(UsuarioDTO usuarioFormularioTemporal) {
		this.usuarioFormularioTemporal = new UsuarioDTO();
		this.usuarioFormularioTemporal.setCtRol(usuarioFormularioTemporal.getCtRol());
//		this.usuarioFormularioTemporal = usuarioFormularioTemporal;
	}

	public List<UsuarioDTO> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioDTO> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<UsuarioDTO> getListaUsuariosFiltrados() {
		return listaUsuariosFiltrados;
	}

	public void setListaUsuariosFiltrados(List<UsuarioDTO> listaUsuariosFiltrados) {
		this.listaUsuariosFiltrados = listaUsuariosFiltrados;
	}

	public String mensajeEstadoUsuario(boolean estado) {
		return (estado ? Constantes.ESTADO_HABILITADO : Constantes.ESTADO_DESHABILITADO);
	}

}
