package com.unmsm.software.presentacion.model;

import com.unmsm.software.persistencia.dto.SlaDTO;
import com.unmsm.software.util.clases.AplicacionUtil;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.TipoOperacion;

public class SlaFormularioModel {

	private SlaDTO slaFormulario;
	private TipoOperacion tipoOperacion;
	

	public SlaFormularioModel() {
		// TODO Auto-generated constructor stub
	}

	public SlaDTO getSlaFormulario() {
		return slaFormulario;
	}

	public void setSlaFormulario(SlaDTO slaFormulario) {
		this.slaFormulario = slaFormulario;
	}

	public void inicializarSlaFormulario() {
		this.slaFormulario = new SlaDTO();
	}

	public TipoOperacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(TipoOperacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	public boolean esOPeracionAgregar(){
		if(this.tipoOperacion==TipoOperacion.AGREGAR)return true;
		else return false;
	}
	
	public boolean esOperacionModificar(){
		if(this.tipoOperacion==TipoOperacion.MODIFICAR)return true;
		else return false;
	}
	
	public String obtenerTituloOperacion(){
		if(this.tipoOperacion==TipoOperacion.AGREGAR){
			return TipoOperacion.AGREGAR.getMensaje();
		}else if(this.tipoOperacion == TipoOperacion.MODIFICAR){
			return TipoOperacion.MODIFICAR.getMensaje();
		}
		return "";
	}

	public void cargarDatosIniciales(int codigoSLA){
		this.slaFormulario.setCodigo(AplicacionUtil.formatoCodigoGenerado(Constantes.PREFIJO_COD_SLA, codigoSLA));
	}
}
