package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.util.clases.Constantes;

public class ServicioListarModel {

	private List<ServicioDTO> listaServicio;
	private List<ServicioDTO> listaServicioFiltrados;

	public ServicioListarModel() {
		// TODO Auto-generated constructor stub
	}

	public ServicioListarModel(List<ServicioDTO> listaServicio, List<ServicioDTO> listaServicioFiltrados) {
		super();
		this.listaServicio = listaServicio;
		this.listaServicioFiltrados = listaServicioFiltrados;
	}

	public List<ServicioDTO> getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List<ServicioDTO> listaServicio) {
		this.listaServicio = listaServicio;
	}

	public List<ServicioDTO> getListaServicioFiltrados() {
		return listaServicioFiltrados;
	}

	public void setListaServicioFiltrados(List<ServicioDTO> listaServicioFiltrados) {
		this.listaServicioFiltrados = listaServicioFiltrados;
	}
	
	public String mensajeEstadoServicio(boolean estado){
		return (estado?Constantes.ESTADO_HABILITADO:Constantes.ESTADO_DESHABILITADO);
	}

}
