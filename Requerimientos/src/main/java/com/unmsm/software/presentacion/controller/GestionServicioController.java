package com.unmsm.software.presentacion.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.unmsm.software.negocio.service.CategoriaService;
import com.unmsm.software.negocio.service.GrupoAtencionService;
import com.unmsm.software.negocio.service.ServicioService;
import com.unmsm.software.negocio.service.SlaService;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.presentacion.model.ServicioFormularioModel;
import com.unmsm.software.presentacion.model.ServicioListarModel;
import com.unmsm.software.util.clases.PaginaUtil;
import com.unmsm.software.util.clases.TipoMensajeJSF;
import com.unmsm.software.util.clases.TipoOperacion;
import com.unmsm.software.util.excepciones.NegocioExcepcion;

/**
 * @author renzo
 *
 */
@Controller("gestionServicioController")
@ViewScoped
public class GestionServicioController implements Serializable {
	private static final long serialVersionUID = 1L;
	@Autowired
	private ServicioService servicioService;

	@Autowired
	private CategoriaService categoriaService;

	@Autowired
	private GrupoAtencionService grupoAtencionService;

	@Autowired
	private SlaService slaService;

	private ServicioListarModel servicioModel;

	private ServicioFormularioModel servicioFormularioModel;

	@PostConstruct
	public void init() {
		inicializarModels();
		obtenerServicios();
	}

	public void iniciarFormularioAgregar() {
		this.servicioFormularioModel.inicializarServicioFormulario();
		this.servicioFormularioModel.inicializarCategoriasNiveles();
		this.servicioFormularioModel.setTipoOperacion(TipoOperacion.AGREGAR);
		this.servicioFormularioModel.generarCodigoServicio(servicioService.generarNumeracionCodServicio());
		this.cargarDatosInicialesFormulario();

	}

	private void cargarDatosInicialesFormulario() {
		this.servicioFormularioModel.setListaCategoriasPrimerNivel(categoriaService.listarCategoriaPrimerNivel());
		this.servicioFormularioModel.setListaGrupoAtencion(grupoAtencionService.obtenerGruposAtencion());
		this.servicioFormularioModel.setListaSLAs(slaService.obtenerSLAs());
	}

	public void listarCategoriasSegundoNivel() {
		this.servicioFormularioModel.setListaCategoriaSegundoNivel(categoriaService.listarCategoriaSegundoNivel(
				servicioFormularioModel.getCategoriaNivel1Seleccionado().getId_categoria()));
	}

	public void listarCategoriasTercerNivel() {
		this.servicioFormularioModel.setListaCategoriaTercerNivel(categoriaService.listarCategoriaTercerNivel(
				servicioFormularioModel.getCategoriaNivel2Seleccionado().getId_categoria()));
	}

	public void agregarServicio() {
		try {
			servicioService.guardarServicio(servicioFormularioModel.getServicioFormulario());
			PaginaUtil.ejecutar("PF('wgvFormularioServicio').hide()");
			this.servicioFormularioModel.inicializarServicioFormulario();
			obtenerServicios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El servicio se agrego con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}finally {
			PaginaUtil.actualizarComponente("frmGestionServicio:messages");
		}

	}

	public void iniciarFormularioModificar(ServicioDTO servicioSeleccionado) {
		this.servicioFormularioModel.setServicioFormulario(servicioSeleccionado);
		this.servicioFormularioModel.setTipoOperacion(TipoOperacion.MODIFICAR);
		cargarDatosInicialesFormulario();
	}

	public void modificarServicio() {
		try {
			servicioService.actualizarServicio(servicioFormularioModel.getServicioFormulario());
			PaginaUtil.ejecutar("PF('wgvFormularioServicio').hide()");
			this.servicioFormularioModel.inicializarServicioFormulario();
			obtenerServicios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El servicio se modifico con exito.");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}finally {
			PaginaUtil.actualizarComponente("frmGestionServicio:messages");
		}
	}

	public void iniciarFormularioEliminar(ServicioDTO servicioSeleccionado) {
		this.servicioFormularioModel.setServicioFormulario(servicioSeleccionado);
		this.servicioFormularioModel.setTipoOperacion(TipoOperacion.ELIMINAR);
	}

	public void eliminarServicio() {
		try {
			servicioService.eliminarServicio(servicioFormularioModel.getServicioFormulario());
			
			obtenerServicios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					"El servicio se elimino con exito.");
			PaginaUtil.actualizarComponente("frmGestionServicio:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
			
		}finally {
			PaginaUtil.ejecutar("PF('dgEliminarServicio').hide()");
			PaginaUtil.actualizarComponente("frmGestionServicio:messages");
		}
	}
	
	public void deshabilitarServicio(ServicioDTO servicioDTO){
		try {
			servicioService.deshabilitarServicio(servicioDTO);
			obtenerServicios();
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.INFORMATIVO.getId(), "Mensaje",
					servicioFormularioModel.mensajeCambioEstadoServicio(servicioDTO));
			PaginaUtil.actualizarComponente("frmGestionServicio:messages");
		} catch (NegocioExcepcion negocioExcepcion) {
			PaginaUtil.mostrarMensajeJSF(TipoMensajeJSF.ERROR.getId(), "Mensaje", negocioExcepcion.getMessage());
		}
	}

	public void ejecutarTipoOperacion() {
		if (this.servicioFormularioModel.esOPeracionAgregar()) {
			agregarServicio();
		} else if (this.servicioFormularioModel.esOperacionModificar()) {
			modificarServicio();
		}
	}

	public void obtenerServicios() {
		this.servicioModel.setListaServicio(servicioService.obtenerServicios());
	}

	public ServicioListarModel getServicioModel() {
		return servicioModel;
	}

	public void setServicioModel(ServicioListarModel servicioModel) {
		this.servicioModel = servicioModel;
	}

	public ServicioFormularioModel getServicioFormularioModel() {
		return servicioFormularioModel;
	}

	public void setServicioFormularioModel(ServicioFormularioModel servicioFormularioModel) {
		this.servicioFormularioModel = servicioFormularioModel;
	}

	public void inicializarModels() {
		this.servicioModel = new ServicioListarModel();
		this.servicioFormularioModel = new ServicioFormularioModel();
	}

	public ServicioService getServicioService() {
		return servicioService;
	}

	public void setServicioService(ServicioService servicioService) {
		this.servicioService = servicioService;
	}

}
