package com.unmsm.software.presentacion.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.persistencia.dto.UsuarioDTO;

public class PrincipalListarModel {
	
	private UsuarioDTO usuarioDTO;
	private List<TicketDTO> listaTickets;
	private List<TicketDTO> listaTicketsFiltrados;
	
	public PrincipalListarModel() {
		listaTicketsFiltrados = new ArrayList<TicketDTO>();
	}
	
	public String formatoFecha(Date fecha){
		String fechaFormat = "";
		
		if(fecha != null)
			fechaFormat = fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+(fecha.getYear()+1900);
			
		return fechaFormat;
	}

	public List<TicketDTO> getListaTickets() {
		return listaTickets;
	}

	public void setListaTickets(List<TicketDTO> listaTickets) {
		this.listaTickets = listaTickets;
	}

	public List<TicketDTO> getListaTicketsFiltrados() {
		return listaTicketsFiltrados;
	}

	public void setListaTicketsFiltrados(List<TicketDTO> listaTicketsFiltrados) {
		this.listaTicketsFiltrados = listaTicketsFiltrados;
	}

	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}

	public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
		this.usuarioDTO = usuarioDTO;
	}
}
