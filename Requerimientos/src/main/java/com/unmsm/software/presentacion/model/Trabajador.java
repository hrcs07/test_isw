package com.unmsm.software.presentacion.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;
import com.unmsm.software.negocio.service.TrabajadorService;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.persistencia.mapper.TrabajadorMapper;

public class Trabajador{

	private Integer id_trabajador;
	private Integer id_usuario;
	private String area;
	
	
	private List<Trabajador> listaTrabajador;

	TrabajadorService trabajadorService;

	public Trabajador() {

	}


	public Integer getId_trabajador() {
		return id_trabajador;
	}


	public void setId_trabajador(Integer id_trabajador) {
		this.id_trabajador = id_trabajador;
	}


	public Integer getId_usuario() {
		return id_usuario;
	}


	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}
	
	public void enviarCorreosReasig(String correo){

		try{

		    Properties props = new Properties();
		    props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.user", "ayrtonraquif@gmail.com");
	        props.put("mail.smtp.password", "rayrton94");
	        props.put("mail.smtp.port", 587);
	        props.put("mail.smtp.auth", "true");
	        
	        Session session = Session.getDefaultInstance(props, null);
	        
	        
	        MimeMessage message = new MimeMessage(session);
	        
	        message.setFrom(new InternetAddress("ayrtonraquif@gmail.com"));
	        
	        message.setRecipient(Message.RecipientType.TO, new InternetAddress(correo));
	        
	        message.setSubject("Administración: Ticket Reasignado");
	        
	        message.setText("Hola, le han reasginado un ticket, por favor acceda al dasboard para la atencion correspondiente");
	        
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com","ayrtonraquif@gmail.com","rayrton94");
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        System.out.println("Enviado");
	        
	        
	        
			}catch(Exception e){
				e.printStackTrace();
			}

	}
	
	public void enviarCorreos(String correo){

		try{

		    Properties props = new Properties();
		    props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.user", "ayrtonraquif@gmail.com");
	        props.put("mail.smtp.password", "rayrton94");
	        props.put("mail.smtp.port", 587);
	        props.put("mail.smtp.auth", "true");
	        
	        Session session = Session.getDefaultInstance(props, null);
	        
	        
	        MimeMessage message = new MimeMessage(session);
	        
	        message.setFrom(new InternetAddress("ayrtonraquif@gmail.com"));
	        
	        message.setRecipient(Message.RecipientType.TO, new InternetAddress(correo));
	        
	        message.setSubject("Administración: Nuevo Ticket");
	        
	        message.setText("Hola, se ha publicado un nuevo ticket, si desea tomarlo, por favor ingrese a su dashboard y acepte el ticket.");
	        
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com","ayrtonraquif@gmail.com","rayrton94");
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        System.out.println("Enviado");
	        
	        
	        
			}catch(Exception e){
				e.printStackTrace();
			}

	}
	
	public static void main(String[] args) {
		try{

		    Properties props = new Properties();
		    //props.put("mail.transport.protocol", "smtp");
		    props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.user", "ayrtonraquif@gmail.com");
	        props.put("mail.smtp.password", "rayrto94");
	        props.put("mail.smtp.port", 587);
	        props.put("mail.smtp.auth", "true");
	        
	        Session session = Session.getDefaultInstance(props, null);
	        
	        
	        MimeMessage message = new MimeMessage(session);
	        
	        message.setFrom(new InternetAddress("ayrtonraqui@gmail.com"));
	        
	        message.setRecipient(Message.RecipientType.TO, new InternetAddress("ayrtonraqui@gmail.com"));
	        
	        message.setSubject("Prueba");
	        
	        message.setText("Texto de Prueba");
	        
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com","ayrtonraquif@gmail.com","rayrton94");
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        
	        System.out.println("Mensaje enviado con exito");
	        
	        
			}catch(Exception e){
				System.out.println("ERROR: El correo ha sido ingresado incorrectamente.");
			}
	}


	public List<Trabajador> getListaTrabajador() {
		return listaTrabajador;
	}


	public void setListaTrabajador(List<Trabajador> listaTrabajador) {
		this.listaTrabajador = listaTrabajador;
	}

	
	
}
