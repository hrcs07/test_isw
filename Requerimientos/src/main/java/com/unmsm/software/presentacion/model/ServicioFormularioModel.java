package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.dto.SlaDTO;
import com.unmsm.software.util.clases.AplicacionUtil;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.TipoOperacion;

/**
 * @author renzo
 *
 */
public class ServicioFormularioModel {

	private ServicioDTO servicioFormulario;

	private TipoOperacion tipoOperacion;

	/**
	 * Falta correjir esto , porque no va de acuerdo a la arquitectura
	 */
	private List<Categoria> listaCategoriasPrimerNivel;

	private List<Categoria> listaCategoriaSegundoNivel;

	private List<Categoria> listaCategoriaTercerNivel;

	private List<GrupoAtencionDTO> listaGrupoAtencion;

	private List<SlaDTO> listaSLAs;

	private Categoria categoriaNivel1Seleccionado;

	private Categoria categoriaNivel2Seleccionado;

	public ServicioFormularioModel() {
		this.inicializarCategoriasNiveles();
	}

	public Categoria getCategoriaNivel1Seleccionado() {
		return categoriaNivel1Seleccionado;
	}

	public void setCategoriaNivel1Seleccionado(Categoria categoriaNivel1Seleccionado) {
		this.categoriaNivel1Seleccionado = categoriaNivel1Seleccionado;
	}

	public Categoria getCategoriaNivel2Seleccionado() {
		return categoriaNivel2Seleccionado;
	}

	public void setCategoriaNivel2Seleccionado(Categoria categoriaNivel2Seleccionado) {
		this.categoriaNivel2Seleccionado = categoriaNivel2Seleccionado;
	}

	public List<SlaDTO> getListaSLAs() {
		return listaSLAs;
	}

	public void setListaSLAs(List<SlaDTO> listaSLAs) {
		this.listaSLAs = listaSLAs;
	}

	public ServicioDTO getServicioFormulario() {
		return servicioFormulario;
	}

	public void setServicioFormulario(ServicioDTO servicioFormulario) {
		this.servicioFormulario = servicioFormulario;
	}

	public TipoOperacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(TipoOperacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public List<Categoria> getListaCategoriasPrimerNivel() {
		return listaCategoriasPrimerNivel;
	}

	public void setListaCategoriasPrimerNivel(List<Categoria> listaCategoriasPrimerNivel) {
		this.listaCategoriasPrimerNivel = listaCategoriasPrimerNivel;
	}

	public List<Categoria> getListaCategoriaSegundoNivel() {
		return listaCategoriaSegundoNivel;
	}

	public void setListaCategoriaSegundoNivel(List<Categoria> listaCategoriaSegundoNivel) {
		this.listaCategoriaSegundoNivel = listaCategoriaSegundoNivel;
	}

	public List<Categoria> getListaCategoriaTercerNivel() {
		return listaCategoriaTercerNivel;
	}

	public void setListaCategoriaTercerNivel(List<Categoria> listaCategoriaTercerNivel) {
		this.listaCategoriaTercerNivel = listaCategoriaTercerNivel;
	}

	public List<GrupoAtencionDTO> getListaGrupoAtencion() {
		return listaGrupoAtencion;
	}

	public void setListaGrupoAtencion(List<GrupoAtencionDTO> listaGrupoAtencion) {
		this.listaGrupoAtencion = listaGrupoAtencion;
	}

	public boolean esOPeracionAgregar() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR)
			return true;
		else
			return false;
	}

	public boolean esOperacionModificar() {
		if (this.tipoOperacion == TipoOperacion.MODIFICAR)
			return true;
		else
			return false;
	}

	public String obtenerTituloOperacion() {
		if (this.tipoOperacion == TipoOperacion.AGREGAR) {
			return TipoOperacion.AGREGAR.getMensaje();
		} else if (this.tipoOperacion == TipoOperacion.MODIFICAR) {
			return TipoOperacion.MODIFICAR.getMensaje();
		}
		return "";
	}

	public void generarCodigoServicio(int codigoSLA) {
		this.servicioFormulario
				.setCodigoServicio(AplicacionUtil.formatoCodigoGenerado(Constantes.PREFIJO_COD_SERVICIO, codigoSLA));
	}

	public void inicializarServicioFormulario() {
		this.servicioFormulario = new ServicioDTO();
	}
	
	public void inicializarCategoriasNiveles(){
		this.categoriaNivel1Seleccionado = new Categoria();
		this.categoriaNivel2Seleccionado = new Categoria();
	}
	
	public String mensajeCambioEstadoServicio(ServicioDTO servicioDTO){
		return (servicioDTO.isEstadoServicio()?"El servicio se habilito con exito.":"El servicio se deshabilito con exito.");
	}
	
	
}
