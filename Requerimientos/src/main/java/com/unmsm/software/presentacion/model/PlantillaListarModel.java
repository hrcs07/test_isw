package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.ComponenteDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;

public class PlantillaListarModel {
	private List<ComponenteDTO> listaComponentes;
	private List<ComponenteDTO> listaComponentesFiltrados;

	private List<ServicioDTO> listaServicios;
	private ServicioDTO servicioSeleccionado;

	public PlantillaListarModel() {
		inicializarServicioSeleccionado();
	}

	public ServicioDTO getServicioSeleccionado() {
		return servicioSeleccionado;
	}

	public void setServicioSeleccionado(ServicioDTO servicioSeleccionado) {
		this.servicioSeleccionado = servicioSeleccionado;
	}

	public List<ServicioDTO> getListaServicios() {
		return listaServicios;
	}

	public void setListaServicios(List<ServicioDTO> listaServicios) {
		this.listaServicios = listaServicios;
	}

	public List<ComponenteDTO> getListaComponentes() {
		return listaComponentes;
	}

	public void setListaComponentes(List<ComponenteDTO> listaComponentes) {
		this.listaComponentes = listaComponentes;
	}

	public List<ComponenteDTO> getListaComponentesFiltrados() {
		return listaComponentesFiltrados;
	}

	public void setListaComponentesFiltrados(List<ComponenteDTO> listaComponentesFiltrados) {
		this.listaComponentesFiltrados = listaComponentesFiltrados;
	}
	
	public void inicializarServicioSeleccionado(){
		this.servicioSeleccionado = new ServicioDTO();
	}
	
	public String obtenerNombreServicioSeleccionado(){
		for (ServicioDTO servicioDTO : listaServicios) {
			if(servicioDTO.getIdServicio().intValue()==servicioSeleccionado.getIdServicio().intValue()) return servicioDTO.getNombreServicio();
		}
		return "";
	}

}
