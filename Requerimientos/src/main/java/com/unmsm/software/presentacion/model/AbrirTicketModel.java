package com.unmsm.software.presentacion.model;

import java.util.List;

import com.unmsm.software.persistencia.dto.ServicioDTO;

public class AbrirTicketModel {

	private List<Categoria> categoria1;
	private Categoria categoria1Seleccionada;

	private List<Categoria> categoria2;
	private Categoria categoria2Seleccionada;

	private List<Categoria> categoria3;
	private Categoria categoria3Seleccionada;

	private List<ServicioDTO> servicio;
	private ServicioDTO servicioSeleccionado;
	
	//  ***************************************************************************************
	
	//  *********************************  Metodos set y get  *********************************
	
	//  ***************************************************************************************

	public List<Categoria> getCategoria1() {
		return categoria1;
	}

	public void setCategoria1(List<Categoria> categoria1) {
		this.categoria1 = categoria1;
	}

	public Categoria getCategoria1Seleccionada() {
		return categoria1Seleccionada;
	}

	public void setCategoria1Seleccionada(Categoria categoria1Seleccionada) {
		this.categoria1Seleccionada = categoria1Seleccionada;
	}

	public List<Categoria> getCategoria2() {
		return categoria2;
	}

	public void setCategoria2(List<Categoria> categoria2) {
		this.categoria2 = categoria2;
	}

	public Categoria getCategoria2Seleccionada() {
		return categoria2Seleccionada;
	}

	public void setCategoria2Seleccionada(Categoria categoria2Seleccionada) {
		this.categoria2Seleccionada = categoria2Seleccionada;
	}

	public List<Categoria> getCategoria3() {
		return categoria3;
	}

	public void setCategoria3(List<Categoria> categoria3) {
		this.categoria3 = categoria3;
	}

	public Categoria getCategoria3Seleccionada() {
		return categoria3Seleccionada;
	}

	public void setCategoria3Seleccionada(Categoria categoria3Seleccionada) {
		this.categoria3Seleccionada = categoria3Seleccionada;
	}

	public List<ServicioDTO> getServicio() {
		return servicio;
	}

	public void setServicio(List<ServicioDTO> servicio) {
		this.servicio = servicio;
	}

	public ServicioDTO getServicioSeleccionado() {
		return servicioSeleccionado;
	}

	public void setServicioSeleccionado(ServicioDTO servicioSeleccionado) {
		this.servicioSeleccionado = servicioSeleccionado;
	}
}
