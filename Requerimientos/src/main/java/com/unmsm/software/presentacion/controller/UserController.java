package com.unmsm.software.presentacion.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.stereotype.Controller;

import com.unmsm.software.presentacion.model.UserModel;

@Controller
@ManagedBean(name="userController")
@ViewScoped
public class UserController {
	private List<UserModel> users;
	
//	@Autowired 
//	UserServiceImpl userService;
	
    @PostConstruct
    public void init() {
//    	users = userService.listarUsers();
    }
    
	public List<UserModel> getUsers() {
		return users;
	}

	public void setUsers(List<UserModel> users) {
		this.users = users;
	}
	
	
	
	
}
