package com.unmsm.software.util.clases;

public enum Errores {
	ERROR_ACCESO_DATOS_NO_CATEGORIZADO("Error de acceso a datos no categorizado"),
	ERROR_ACCESO_DATOS("Error de acceso a datos"),
	ERROR_GENERAL_LOGICA_NEGOCIO("Error general de lógica de negocio"),
	
	ERROR_NEGOCIO_CONSULTA_REGISTRO("Error al consultar registro"),
	ERROR_NEGOCIO_MODIFICACION_REGISTRO("Error al modificar registro"),
	ERROR_NEGOCIO_INSERCION_REGISTRO("Error al registrar"),
	ERROR_NEGOCIO_ELIMINACION_REGISTRO("Error al eliminar registro"),
	
	ERROR_INSERTAR_USUARIO_USERNAME_REPETIDO("El username ingresado ya existe"),
	
	ERROR_ELIMINAR_SLA_EN_USO("No se pudo eliminar el SLA porque está siendo usada actualmente"),
	ERROR_ELIMINAR_USUARIO_ASIGNADO("No se pudo eliminar el usuario porque ha sido asignado a un grupo de atención"),
	ERROR_ELIMINAR_TECNICO_ASIGNADO("No se pudo eliminar el usuario(Tecnico) porque ha sido asignado a un grupo de atención"),
	ERROR_ELIMINAR_GRUPO_ATENCION_CON_TECNICOS("No se pudo eliminar el grupo de atencion porque hay tecnicos asignados"),
	ERROR_ELIMINAR_CLIENTE_CON_TICKECTS_FAVORITOS("No se pudo eliminar porque el cliente esta asocidado con tickets"),
	ERROR_ELIMINAR_SERVICIO("El servicio no se puede eliminar porque esta referenciado en un ticket creado");
	
	private String mensaje;

	
	private Errores(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
	
}
