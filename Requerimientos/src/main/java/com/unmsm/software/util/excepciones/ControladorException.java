package com.unmsm.software.util.excepciones;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ControladorException {

	@Pointcut("execution (* com.unmsm.software.negocio.service.impl.*.*(..))")
	public void errorNegocio(){}
	
	@Pointcut("execution(* com.unmsm.software.persistencia.mapper.*.*(..))")
	public void errorPersistencia(){}
	
//	@Pointcut("errorNegocio() || errorPersistencia()")
//	public void errorAplicacion(){}
	
	@Pointcut("errorNegocio()")
	public void errorAplicacion(){}
	
//	@AfterThrowing(pointcut = "errorAplicacion() ", throwing = "error")
//	public void manejarExcepcion(JoinPoint jointPoint, Throwable error) {
//		if(error instanceof NegocioExcepcion){
//			error.printStackTrace();
//		}else if(error instanceof PersistenciaExcepcion){
//			
//		}
//	}
}
