package com.unmsm.software.util.excepciones.persistencia;

import java.sql.SQLException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.UncategorizedDataAccessException;
import org.springframework.stereotype.Component;

import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Component
@Aspect
public class TrabajadorMapperException {

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.TrabajadorMapper.obtenerTrabajadores())", throwing = "error")
	public void obtenerTrabajadores(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.TrabajadorMapper.insertarTrabajador(..))", throwing = "error")
	public void insertarTrabajador(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}


	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.TrabajadorMapper.eliminarTrabajador(..))", throwing = "error")
	public void eliminarTrabajador(JoinPoint joinPoint, Throwable error) {
		if (error instanceof DataIntegrityViolationException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ELIMINAR_TECNICO_ASIGNADO.getMensaje(), error);
		} else if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

}
