package com.unmsm.software.util.clases;

public enum TipoOperacion {
	AGREGAR("AGREGAR"), MODIFICAR("MODIFICAR"), ELIMINAR("ELIMINAR");

	String mensaje;

	private TipoOperacion(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
