package com.unmsm.software.util.clases;

public class Constantes {
	public static final String PREFIJO_COD_SLA = "SLA";
	public static final Integer LONGITUD_NUMERACION_SLA = 5;

	public static final String PREFIJO_COD_SERVICIO = "SERV";
	public static final Integer LONGITUD_NUMERACION_SERVICIO = 5;
	
	public static final String PREFIJO_COD_GRUP_ATENCION = "GRP";
	public static final Integer LONGITUD_NUMERACION_GRUP_ATENCION = 5;
	
	public static final String PREFIJO_COD_TICKET = "TICKET";
	public static final Integer LONGITUD_NUMERACION_TICKET = 5;
	
	public static final int ROL_CLIENTE = 1;
	public static final int ROL_TECNICO = 2;
	public static final int ROL_ADMINISTRADOR = 3;
	
	public static final String ESTADO_HABILITADO = "Habilitado";
	public static final String ESTADO_DESHABILITADO = "Deshabilitado";
	
	
}
