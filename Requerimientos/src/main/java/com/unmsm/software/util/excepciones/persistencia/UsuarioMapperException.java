package com.unmsm.software.util.excepciones.persistencia;

import java.sql.SQLException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.UncategorizedDataAccessException;
import org.springframework.stereotype.Component;

import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Component
@Aspect
public class UsuarioMapperException {

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.UsuarioMapper.obtenerusuarios())", throwing = "error")
	public void obtenerSLAs(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.UsuarioMapper.insertarUsuario(..))", throwing = "error")
	public void insertarSLA(JoinPoint joinPoint, Throwable error) {
		if (error instanceof SQLException) {
			if (((SQLException)error).getSQLState().startsWith("23")) {
				throw new PersistenciaExcepcion(Errores.ERROR_INSERTAR_USUARIO_USERNAME_REPETIDO.getMensaje(), error);
          } 
		} else if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.UsuarioMapper.actualizarUsuario(..))", throwing = "error")
	public void actualizarSLA(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.UsuarioMapper.eliminarUsuario(..))", throwing = "error")
	public void eliminarSLA(JoinPoint joinPoint, Throwable error) {
		if (error instanceof DataIntegrityViolationException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ELIMINAR_USUARIO_ASIGNADO.getMensaje(), error);
		} else if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

}
