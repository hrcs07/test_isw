package com.unmsm.software.util.clases;

public class AplicacionUtil {
	public static String formatoCodigoGenerado(String prefijo,int numeracion){
		return prefijo+"-"+formatoNumero(numeracion,Constantes.LONGITUD_NUMERACION_SLA);
	}
	
	public static String formatoNumero(int numero,int cantDigitos){
		String numFormateado = "";
		int l = cantDigitos-String.valueOf(numero).length();
		for(int i=0;i<l;i++)numFormateado+="0";
		numFormateado+=numero;
		return numFormateado;
	}
	
}
