package com.unmsm.software.util.excepciones.persistencia;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.UncategorizedDataAccessException;
import org.springframework.stereotype.Component;

import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Component
@Aspect
public class ServicioMapperException {

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.ServicioMapper.obtenerServicios())", throwing = "error")
	public void obtenerServicios(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.ServicioMapper.insertarServicio(..))", throwing = "error")
	public void insertarServicio(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.ServicioMapper.actualizarServicio(..))", throwing = "error")
	public void actualizarServicio(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.ServicioMapper.eliminarServicio(..))", throwing = "error")
	public void eliminarServicio(JoinPoint joinPoint, Throwable error) {
		if (error instanceof DataIntegrityViolationException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ELIMINAR_SERVICIO.getMensaje(), error);
		} else if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.ServicioMapper.deshabilitarServicio(..))", throwing = "error")
	public void deshabilitarServicio(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

	@AfterThrowing(pointcut = "execution(* com.unmsm.software.persistencia.mapper.ServicioMapper.obtenerMaxIDServicio(..))", throwing = "error")
	public void obtenerMaxIDServicio(JoinPoint joinPoint, Throwable error) {
		if (error instanceof UncategorizedDataAccessException) {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), error);
		} else {
			throw new PersistenciaExcepcion(Errores.ERROR_ACCESO_DATOS.getMensaje(), error);
		}
	}

}
