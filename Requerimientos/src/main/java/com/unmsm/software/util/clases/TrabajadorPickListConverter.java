package com.unmsm.software.util.clases;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import com.unmsm.software.persistencia.dto.TrabajadorDTO;

@FacesConverter("trabajadorPickListConverter")
public class TrabajadorPickListConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return getObjectFromUIPickListComponent(component, value);
	}

	public String getAsString(FacesContext context, UIComponent component, Object object) {
		String string;

		if (object == null) {
			string = "";
		} else {
			try {
				string = String.valueOf(((TrabajadorDTO) object).getIdTrabajador());
			} catch (ClassCastException cce) {
				throw new ConverterException();
			}
		}
		return string;
	}

	@SuppressWarnings("unchecked")
	private TrabajadorDTO getObjectFromUIPickListComponent(UIComponent component, String value) {
		final DualListModel<TrabajadorDTO> dualList;
		try {
			dualList = (DualListModel<TrabajadorDTO>) ((PickList) component).getValue();
			TrabajadorDTO TrabajadorDTO = getObjectFromList(dualList.getSource(), Integer.valueOf(value));
			if (TrabajadorDTO == null) {
				TrabajadorDTO = getObjectFromList(dualList.getTarget(), Integer.valueOf(value));
			}

			return TrabajadorDTO;
		} catch (ClassCastException cce) {
			throw new ConverterException();
		} catch (NumberFormatException nfe) {
			throw new ConverterException();
		}
	}

	private TrabajadorDTO getObjectFromList(final List<?> list, final Integer identifier) {
		for (final Object object : list) {
			final TrabajadorDTO TrabajadorDTO = (TrabajadorDTO) object;
			if (TrabajadorDTO.getIdTrabajador().equals(identifier)) {
				return TrabajadorDTO;
			}
		}
		return null;
	}

}
