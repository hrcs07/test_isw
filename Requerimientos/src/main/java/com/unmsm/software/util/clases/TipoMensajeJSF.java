package com.unmsm.software.util.clases;

public enum TipoMensajeJSF {
	INFORMATIVO("Informativo", 1), ARDVERTENCIA("Advertencia", 2), ERROR("Error", 3), FATAL("Fatal", 4);

	private String descripcion;

	private int id;

	private TipoMensajeJSF(String descripcion, int id) {
		this.descripcion = descripcion;
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
