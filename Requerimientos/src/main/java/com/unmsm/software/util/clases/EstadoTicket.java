package com.unmsm.software.util.clases;

public enum EstadoTicket {
	TODO("TODO",0), REGISTRADO("REGISTRADO",1), CURSO("EN CURSO",2), ESPERA("EN ESPERA",3), FINALIZADO("FINALIZADO",4);

	String mensaje;
	int valor;
	
	private EstadoTicket(String mensaje, int valor) {
		this.mensaje = mensaje;
		this.valor = valor;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}
