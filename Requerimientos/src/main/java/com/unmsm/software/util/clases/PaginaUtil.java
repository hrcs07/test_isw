package com.unmsm.software.util.clases;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.context.RequestContext;

public class PaginaUtil {
	public static void redireccionar(String pagina) throws IOException {
		ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
		String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
		pagina = (pagina.startsWith("/") ? pagina : "/" + pagina);
		ctx.redirect(ctxPath + pagina);
	}

	/**
	 * Este metodo publica un mensaje en la vista JSF
	 * 
	 * @param severidad
	 *            : INFO=1, WARN=2, ERROR=3, FATAL=4.
	 * @param titulo
	 *            : nombre del mensaje.
	 * @param descripcion
	 *            : cuerpo del mensaje.
	 */
	public static void mostrarMensajeJSF(int severidad, String titulo, String descripcion) {
		mostrarMensajeJSFporId(null, severidad, titulo, descripcion);
	}

	public static void mostrarMensajeJSFporId(String idMsg,int severidad, String titulo, String descripcion) {
		
		FacesMessage facesMessage = null;
		switch (severidad) {
		case 1:
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, descripcion);
			break;
		case 2:
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, descripcion);
			break;
		case 3:
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, descripcion);
			break;
		case 4:
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL, titulo, descripcion);
			break;
		default:
			break;
		}
		
		FacesContext contex = FacesContext.getCurrentInstance();
        contex.getExternalContext (). getFlash (). setKeepMessages ( true );
        contex.addMessage(idMsg, facesMessage);
		
	}

	/**
	 * Este metodo permite ejecutar una instrucción Primefaces
	 * 
	 * @param sentencia
	 *            : es la instrucción a ejecutar. Ejemplo=
	 *            "PF('dialogoWidget').show()"
	 */
	public static void ejecutar(String sentencia) {
		RequestContext.getCurrentInstance().execute(sentencia);
	}

	/**
	 * Este metodo permite generar un dialogo
	 * 
	 * @param dialogo
	 *            : nombre del dialogo. Ejemplo= "PF('dialogoWidget').show()"
	 */
	public static void generarDialogo(String dialogo) {
		RequestContext.getCurrentInstance().execute("PF('" + dialogo + "')");
	}

	/**
	 * Este metodo permite actualizar un componente o control en la Vista de
	 * JSF.
	 * 
	 * @param id
	 *            : es el valor del componente a actualizar en la vista.
	 *            Ejemplo= "actualizarComponente("form:control");"
	 */
	public static void actualizarComponente(String id) {
		RequestContext.getCurrentInstance().update(id);
	}
	
	
}
