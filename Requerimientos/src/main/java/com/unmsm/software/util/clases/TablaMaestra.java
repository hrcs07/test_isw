package com.unmsm.software.util.clases;

public enum TablaMaestra {

	TABLA_ROL("Roles de usuario", 1),
	TABLA_PRIORIDAD("Prioridad del ticket", 4);	

	private String titulo;
	private int id;

	private TablaMaestra(String titulo, int id) {
		this.titulo = titulo;
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
