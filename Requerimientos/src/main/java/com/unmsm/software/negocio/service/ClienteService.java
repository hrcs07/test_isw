package com.unmsm.software.negocio.service;

import com.unmsm.software.persistencia.dto.ClienteDTO;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;

public interface ClienteService {

	public void guardarCliente(ClienteDTO clienteDTO);

	public ClienteDTO buscarCliente(Integer idCliente);
}
