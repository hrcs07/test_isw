package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.TablaMaestraDTO;

/**
 * @author renzo
 *
 */
public interface RolService {
	
	public List<TablaMaestraDTO> obtenerRoles();

	public void guardarRol(TablaMaestraDTO tablaMaestraDTO);

	public void actualizarRol(TablaMaestraDTO tablaMaestraDTO);

	public void eliminarRol(TablaMaestraDTO tablaMaestraDTO);

	public Integer obtenerMaxIDRol();
}
