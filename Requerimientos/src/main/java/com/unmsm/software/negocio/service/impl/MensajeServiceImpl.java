package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unmsm.software.negocio.service.MensajeService;
import com.unmsm.software.persistencia.dto.MensajeDTO;
import com.unmsm.software.persistencia.mapper.MensajeMapper;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Service
public class MensajeServiceImpl implements MensajeService{
	
	@Autowired
	private MensajeMapper mensajeMapper;

	@Override
	public List<MensajeDTO> obtenerMensajesTicket(Integer idTicket) {
		List<MensajeDTO> listaMensajes = new ArrayList<MensajeDTO>();
		try{
			listaMensajes = mensajeMapper.obtenerListaMensaje(idTicket);
		}catch(Exception e){
			System.out.println("Error al listar mensajes del ticket: "+e);
		}
		return listaMensajes;
	}

	@Override
	public void registrarMensaje(MensajeDTO mensajeDTO) {
		try{
			mensajeMapper.guardarMensaje(mensajeDTO);
		}catch(Exception e){
			System.out.println("Error al guardar mensaje del ticket: "+e);
		}
	}

	@Override
	public Integer ultimoMensaje() {
		Integer idMensaje = null;
		try {
			idMensaje = mensajeMapper.obtenerMaxIDMensaje();
		} catch (Exception e) {
			System.out.println("Erro al obtener el ultimo mensaje: "+e);
		}
		return idMensaje;
	}

}
