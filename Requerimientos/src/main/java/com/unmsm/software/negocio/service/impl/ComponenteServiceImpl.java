package com.unmsm.software.negocio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unmsm.software.negocio.service.ComponenteService;
import com.unmsm.software.persistencia.dto.ComponenteDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.mapper.ComponenteMapper;

@Service
public class ComponenteServiceImpl implements ComponenteService{
	
	@Autowired
	private ComponenteMapper componenteMapper;

	@Override
	public void guardarComponente(ComponenteDTO componenteDTO) {
		componenteMapper.insertarComponente(componenteDTO);
	}

	@Override
	public List<ComponenteDTO> obtenerComponentes(ServicioDTO servicioDTO) {
		return componenteMapper.obtenerComponentes(servicioDTO.getIdServicio());
	}

	@Override
	public void actualizarComponente(ComponenteDTO componenteDTO) {
		componenteMapper.actualizarComponente(componenteDTO);
	}

	@Override
	public void eliminarComponente(ComponenteDTO componenteDTO) {
		componenteMapper.eliminarComponente(componenteDTO);
	}

}
