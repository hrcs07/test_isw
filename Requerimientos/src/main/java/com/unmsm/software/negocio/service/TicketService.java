package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.TablaMaestraDTO;
import com.unmsm.software.persistencia.dto.TicketDTO;

public interface TicketService {
	
	public void guardarTicket(TicketDTO ticketDTO);

	public Integer generarNumeracionCodTicket();
	
	public List<TicketDTO> obtenerTicketsCliente(Integer idCliente);
	
	public List<TicketDTO> filtrarListaTickets(List<TicketDTO> listTickets, String estadoTicket);
	
	public int cantidadTicketsEstado(List<TicketDTO> listTickets, String estadoTicket);
	
	public List<TablaMaestraDTO> obtenerPrioridades();
	
	public List<TicketDTO> obtenerTicketsInicial();
	
	public List<TicketDTO> obtenerTickets(Integer idTecnico);
	
	public Integer numeroTicketsNuevos();
	
	public Integer numeroTicketsEnAtencion();
	
	public Integer numeroTicketsEnEspera();
	
	public Integer numeroTicketsFinalizado();
	
	public Integer numeroTicketsTotal();
	
	public Integer numeroSemaforoVerde();
	
	public Integer numeroSemaforoAmarillo();
	
	public Integer numeroSemaforoRojo();
	
	public void aceptarTicket(Integer idTicket, Integer idTrabajador);
	
	public void reasignacionTicket(Integer idTicket, Integer idTrabajador);
	
	public String obtenerNombreTrabajador(Integer idUsuario);

}
