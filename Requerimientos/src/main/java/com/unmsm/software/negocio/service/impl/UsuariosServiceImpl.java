package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.UsuariosService;
import com.unmsm.software.persistencia.dto.ClienteDTO;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.persistencia.dto.UsuarioDTO;
import com.unmsm.software.persistencia.mapper.ClienteMapper;
import com.unmsm.software.persistencia.mapper.TrabajadorMapper;
import com.unmsm.software.persistencia.mapper.UsuarioMapper;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UsuariosServiceImpl implements UsuariosService {

	@Autowired
	private UsuarioMapper usuarioMapper;

	@Autowired
	private TrabajadorMapper trabajadorMapper;

	@Autowired
	private ClienteMapper clienteMapper;

	@Override
	public void guardarUsuario(UsuarioDTO usuarioDTO) {
		Integer idUsuario = null;
		try {
			usuarioDTO.setEstadoUsuario(true);
			usuarioDTO.setPassword(usuarioDTO.getUsername());
			usuarioMapper.insertarUsuario(usuarioDTO);
			idUsuario = usuarioDTO.getIdUsuario();
			if (usuarioDTO.getCtRol().intValue() == Constantes.ROL_CLIENTE) {
				ClienteDTO cliente = new ClienteDTO();
				cliente.setIdUsuario(idUsuario);
				clienteMapper.insertarCliente(cliente);
			} else if (usuarioDTO.getCtRol().intValue() == Constantes.ROL_TECNICO) {
				TrabajadorDTO trabajador = new TrabajadorDTO();
				trabajador.setIdUsuario(idUsuario.intValue());
				trabajadorMapper.insertarTrabajador(trabajador);
			}
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
	}

	@Override
	public List<UsuarioDTO> obtenerUsuarios() {
		List<UsuarioDTO> listaUsuariosTemporal = null;
		List<UsuarioDTO> listaUsuarios = new ArrayList<>();
		try {
			listaUsuariosTemporal = usuarioMapper.obtenerUsuarios();
			for (UsuarioDTO usuarioDTO : listaUsuariosTemporal) {
				usuarioDTO.setDesEstadoServicio(
						usuarioDTO.isEstadoUsuario() ? Constantes.ESTADO_HABILITADO : Constantes.ESTADO_DESHABILITADO);
				listaUsuarios.add(usuarioDTO);
			}
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
		return listaUsuarios;
	}

	@Override
	public void actualizarUsuario(UsuarioDTO usuarioDTO, UsuarioDTO usuarioDTOTemporal) {
		try {
			if (usuarioDTO.getCtRol().intValue() == Constantes.ROL_CLIENTE
					&& (usuarioDTO.getCtRol().intValue() != usuarioDTOTemporal.getCtRol().intValue())) {
				TrabajadorDTO trabajador = new TrabajadorDTO();
				trabajador.setIdUsuario(usuarioDTO.getIdUsuario());
				trabajadorMapper.eliminarTrabajador(trabajador);
				ClienteDTO cliente = new ClienteDTO();
				cliente.setIdUsuario(usuarioDTO.getIdUsuario());
				clienteMapper.insertarCliente(cliente);

			} else if (usuarioDTO.getCtRol().intValue() == Constantes.ROL_TECNICO
					&& (usuarioDTO.getCtRol().intValue() != usuarioDTOTemporal.getCtRol().intValue())) {
				ClienteDTO cliente = new ClienteDTO();
				cliente.setIdUsuario(usuarioDTO.getIdUsuario());
				clienteMapper.eliminarCliente(cliente);
				TrabajadorDTO trabajador = new TrabajadorDTO();
				trabajador.setIdUsuario(usuarioDTO.getIdUsuario());
				trabajadorMapper.insertarTrabajador(trabajador);
			}
			usuarioMapper.actualizarUsuario(usuarioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
	}

	@Override
	public void eliminarUsuario(UsuarioDTO usuarioDTO) {
		try {

			if (usuarioDTO.getCtRol().intValue() == Constantes.ROL_CLIENTE) {
				ClienteDTO clienteDTO = new ClienteDTO();
				clienteDTO.setIdUsuario(usuarioDTO.getIdUsuario());
				clienteMapper.eliminarCliente(clienteDTO);
			} else if (usuarioDTO.getCtRol().intValue() == Constantes.ROL_TECNICO) {
				TrabajadorDTO trabajadorDTO = new TrabajadorDTO();
				trabajadorDTO.setIdUsuario(usuarioDTO.getIdUsuario());
				trabajadorMapper.eliminarTrabajador(trabajadorDTO);
			}
			usuarioMapper.eliminarUsuario(usuarioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_ELIMINACION_REGISTRO.getMensaje(), e);
		}
	}

	@Override
	public void deshabilitarUsuario(UsuarioDTO usuarioDTO) {
		try {
			boolean estado = !usuarioDTO.isEstadoUsuario();
			usuarioDTO.setEstadoUsuario(estado);
			usuarioMapper.deshabilitarUsuario(usuarioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
	}

	@Override
	public UsuarioDTO buscarUsuario(String username, String password) {
		UsuarioDTO usuario = new UsuarioDTO();
		try {
			usuario = usuarioMapper.buscarUsuario(username, password);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), persistenciaExcepcion);
		}
		return usuario;
	}

}
