package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unmsm.software.negocio.service.TrabajadorGrupoAtencionService;
import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.persistencia.dto.TrabajadorGrupoAtencionDTO;
import com.unmsm.software.persistencia.mapper.TrabajadorGrupoAtencionMapper;

/**
 * @author renzo
 *
 */
@Service
public class TrabajadorGrupoAtencionServiceImpl implements TrabajadorGrupoAtencionService {

	@Autowired
	private TrabajadorGrupoAtencionMapper trabajadorGrupoAtencionMapper;

	@Override
	public void agregarUsuarioAGrupoAtencion(List<TrabajadorDTO> listaTrabajadoresAGuardar,
			List<TrabajadorDTO> listaTrabajadoresTemporales, GrupoAtencionDTO grupoAtencion) {
		List<TrabajadorDTO> listTrabajadoresTemporales = listaTrabajadoresTemporales;
		List<TrabajadorDTO> listaTrabajadores = listaTrabajadoresAGuardar;

		List<TrabajadorDTO> listaTrabajadoresNuevos = null;
		List<TrabajadorDTO> listaTrabajadoresEliminar = null;
		int tamanioListaTrab = listaTrabajadores.size(), tamanioListaTrabTemp = listTrabajadoresTemporales.size();
		boolean listaVacia = false;
		for (int i = 0; i < tamanioListaTrab & !listaVacia; i++) {
			for (int j = 0; j < tamanioListaTrabTemp & !listaVacia; j++) {
				if (listaTrabajadores.get(i).getIdTrabajador().intValue() == listTrabajadoresTemporales.get(j)
						.getIdTrabajador().intValue()) {
					listaTrabajadores.remove(i);
					if (i > 0)
						i--;
					tamanioListaTrab--;
					if (tamanioListaTrab == 0)
						listaVacia = true;
					listTrabajadoresTemporales.remove(j);
					if (j > 0)
						j--;
					tamanioListaTrabTemp--;
					if (tamanioListaTrabTemp == 0)
						listaVacia = true;
				}
			}
		}
		listaTrabajadoresNuevos = listaTrabajadores;
		listaTrabajadoresEliminar = listTrabajadoresTemporales;
		for (TrabajadorDTO trabajadorDTO : listaTrabajadoresNuevos) {
			TrabajadorGrupoAtencionDTO trabajadorGrupoAtencionDTO = new TrabajadorGrupoAtencionDTO();
			trabajadorGrupoAtencionDTO.setIdTrabajador(trabajadorDTO.getIdTrabajador());
			trabajadorGrupoAtencionDTO.setIdGrupoAtencion(grupoAtencion.getIdGrupoAtencion());
			trabajadorGrupoAtencionMapper.insertarUsuarioAGrupoAtencion(trabajadorGrupoAtencionDTO);
		}
		for (TrabajadorDTO trabajadorDTO : listaTrabajadoresEliminar) {
			TrabajadorGrupoAtencionDTO trabajadorGrupoAtencionDTO = new TrabajadorGrupoAtencionDTO();
			trabajadorGrupoAtencionDTO.setIdGrupoAtencion(grupoAtencion.getIdGrupoAtencion());
			trabajadorGrupoAtencionDTO.setIdTrabajador(trabajadorDTO.getIdTrabajador());
			trabajadorGrupoAtencionMapper.eliminarGrupoAtencion(trabajadorGrupoAtencionDTO);
		}
	}

	@Override
	public void eliminarUsuarioDeGrupoAtencion(TrabajadorGrupoAtencionDTO trabajadorGrupoAtencion) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<TrabajadorDTO> obtenerTrabajadoresGrupoAtencion(GrupoAtencionDTO grupoAtencion) {
		List<TrabajadorDTO> listaTrabajadores = new ArrayList<>();
		List<TrabajadorGrupoAtencionDTO> listaTrabajadoresGrupoAtencion = trabajadorGrupoAtencionMapper
				.obtenerTrabajadoresGrupoAtencion();
		for (TrabajadorGrupoAtencionDTO trabajadorGrupoAtencionDTO : listaTrabajadoresGrupoAtencion) {
			if (trabajadorGrupoAtencionDTO.getIdGrupoAtencion().intValue() == grupoAtencion.getIdGrupoAtencion()
					.intValue()) {
				TrabajadorDTO trabajador = new TrabajadorDTO();
				trabajador.setIdTrabajador(trabajadorGrupoAtencionDTO.getIdTrabajador());
				trabajador.setDescDatosTrabajador(trabajadorGrupoAtencionDTO.getDesDatosTrabajador());
				listaTrabajadores.add(trabajador);
			}
		}
		return listaTrabajadores;
	}

}
