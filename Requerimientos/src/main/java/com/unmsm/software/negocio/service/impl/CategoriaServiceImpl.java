package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.CategoriaService;
import com.unmsm.software.persistencia.mapper.CategoriaPersistence;
import com.unmsm.software.presentacion.model.Categoria;

@Service
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	CategoriaPersistence categoriaPersistence;

	@Override
	public List<Categoria> getCategorias() {
		List<Categoria> listaCategorias = new ArrayList<Categoria>();
		try {
			listaCategorias = categoriaPersistence.categorias();
		} catch (Exception e) {
			System.out.println("Error al listar categorias: " + e);
		}
		return listaCategorias;
	}

	@Override
	public List<Categoria> listarCategoriaPrimerNivel() {
		List<Categoria> listaCategoriasPrimerNivel = new ArrayList<Categoria>();
		try {
			listaCategoriasPrimerNivel = categoriaPersistence.listarCategoriaPrimerNivel();
		} catch (Exception e) {
			System.out.println("Error al listar categorias del primer nivel: " + e);
		}
		return listaCategoriasPrimerNivel;
	}

	@Override
	public List<Categoria> listarCategoriaSegundoNivel(Integer idCategoriaPadre) {
		List<Categoria> lsCategoriaSegundoNivel = new ArrayList<Categoria>();
		try {
			lsCategoriaSegundoNivel = categoriaPersistence.listarCategoriaSegundoNivel(idCategoriaPadre);
		} catch (Exception e) {
			System.out.println("Error al listar categorias del segundo nivel: " + e);
		}
		return lsCategoriaSegundoNivel;
	}

	@Override
	public List<Categoria> listarCategoriaTercerNivel(Integer idCategoriaPadre) {
		List<Categoria> lsCategoriaTercerNivel = new ArrayList<Categoria>();
		try {
			lsCategoriaTercerNivel = categoriaPersistence.listarCategoriaTercerNivel(idCategoriaPadre);
		} catch (Exception e) {
			System.out.println("Error al listar categorias del tercer nivel: " + e);
		}
		return lsCategoriaTercerNivel;
	}

	@Override
	@Transactional
	public void insertarCategoria(Categoria categoria) {
		categoriaPersistence.insertarCategoria(categoria);
		Integer id_categoria = categoriaPersistence.obtenerMaxIdCategoria();
		categoria.setId_categoria(id_categoria);
		categoriaPersistence.actualizarNiv(categoria);
	}

	@Override
	public void editarCategoria(Categoria categoria) {
		categoriaPersistence.editarCategoria(categoria);
	}

	@Override
	public void deshabilitarCategoria(Categoria categoria) {
		categoriaPersistence.deshabilitarCategoria(categoria);

	}

	@Override
	public void eliminarCategoria(Categoria categoria) {
		categoriaPersistence.eliminarCategoria(categoria);

	}

	@Override
	public void habilitarCategoria(Categoria categoria) {
		categoriaPersistence.habilitarCategoria(categoria);

	}

	@Override
	public Integer obtenerMaxIdCategoria() {
		return categoriaPersistence.obtenerMaxIdCategoria();
	}

	public String secuenciaCategorias(Integer idCategoria) {
		String cadenaCategoria = "";
		try {

			Categoria cat3 = categoriaPersistence.buscarCategoria(idCategoria);
			Categoria cat2 = categoriaPersistence.buscarCategoria(cat3.getCat_padre());
			Categoria cat1 = categoriaPersistence.buscarCategoria(cat2.getCat_padre());

			cadenaCategoria = cat1.getDescripcion() + " > " + cat2.getDescripcion() + "\n> " + cat3.getDescripcion()
					+ " > ";

		} catch (Exception e) {
			System.out.println("Error cadena categoria: " + e);
		}

		return cadenaCategoria;
	}

	public CategoriaPersistence getCategoriaPersistence() {
		return categoriaPersistence;
	}

	public void setCategoriaPersistence(CategoriaPersistence categoriaPersistence) {
		this.categoriaPersistence = categoriaPersistence;
	}
}
