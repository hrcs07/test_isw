package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.ServicioService;
import com.unmsm.software.persistencia.dto.ServicioDTO;
import com.unmsm.software.persistencia.mapper.ServicioMapper;
import com.unmsm.software.util.clases.Constantes;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.clases.EstadoTicket;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

/**
 * @author renzo
 *
 */
@Service
@Transactional
public class ServicioServiceImpl implements ServicioService {

	@Autowired
	private ServicioMapper servicioMapper;

	@Override
	public void guardarServicio(ServicioDTO servicioDTO) {
		try {
			servicioDTO.setEstadoServicio(true);
			servicioMapper.insertarServicio(servicioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_INSERCION_REGISTRO.getMensaje(), e);
		}
	}

	@Override
	public List<ServicioDTO> obtenerServicios() {
		List<ServicioDTO> liServicioDTOTemporales = null;
		List<ServicioDTO> liServicioDTOs = new ArrayList<>();
		try {
			liServicioDTOTemporales = servicioMapper.obtenerServicios();
			for (ServicioDTO servicioDTO : liServicioDTOTemporales) {
				servicioDTO.setDesEstadoServicio(servicioDTO.isEstadoServicio() ? Constantes.ESTADO_HABILITADO
						: Constantes.ESTADO_DESHABILITADO);
				liServicioDTOs.add(servicioDTO);
			}
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			throw new NegocioExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), e);
		}
		return liServicioDTOs;
	}

	@Override
	public List<ServicioDTO> obtenerServiciosPorCategoria(Integer idCategoria) {
		List<ServicioDTO> liServicioDTOCategoria = null;
		try {
			liServicioDTOCategoria = servicioMapper.obtenerServiciosPorCategoria(idCategoria);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), persistenciaExcepcion);
		}
		return liServicioDTOCategoria;
	}

	@Override
	public void actualizarServicio(ServicioDTO servicioDTO) {
		try {
			servicioMapper.actualizarServicio(servicioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_MODIFICACION_REGISTRO.getMensaje(), e);
		}
	}

	@Override
	public void eliminarServicio(ServicioDTO servicioDTO) {
		try {
			servicioMapper.eliminarServicio(servicioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_ELIMINACION_REGISTRO.getMensaje(), e);
		}
	}

	@Override
	public Integer generarNumeracionCodServicio() {
		Integer idServicioGenerado = null;
		try {
			idServicioGenerado = servicioMapper.obtenerMaxIDServicio();
			if (idServicioGenerado == null)
				idServicioGenerado = 1;
			else
				idServicioGenerado++;
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			System.out.println("Error consultar servicio: " + e);
		}
		return idServicioGenerado;
	}

	@Override
	public void deshabilitarServicio(ServicioDTO servicioDTO) {
		try {
			boolean estado = !servicioDTO.isEstadoServicio();
			servicioDTO.setEstadoServicio(estado);
			servicioMapper.deshabilitarServicio(servicioDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_MODIFICACION_REGISTRO.getMensaje(), e);
		}
	}

	public ServicioMapper getServicioMapper() {
		return servicioMapper;
	}

	public void setServicioMapper(ServicioMapper servicioMapper) {
		this.servicioMapper = servicioMapper;
	}

	@Override
	public ServicioDTO obtenerServicio(Integer idServicio) {
		ServicioDTO servicioDTO = new ServicioDTO();
		try {
			servicioDTO = servicioMapper.obtenerServicio(idServicio);
		} catch (Exception e) {
			System.out.println("Error al buscar servicio: " + e);
		}

		return servicioDTO;
	}
}
