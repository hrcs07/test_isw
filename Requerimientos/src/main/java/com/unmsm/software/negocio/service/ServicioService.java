package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.ServicioDTO;

/**
 * @author renzo
 *
 */
public interface ServicioService {

	public void guardarServicio(ServicioDTO servicioDTO);

	public List<ServicioDTO> obtenerServicios();
	
	public ServicioDTO obtenerServicio(Integer idServicio);
	
	public List<ServicioDTO> obtenerServiciosPorCategoria(Integer idCategoria);

	public void actualizarServicio(ServicioDTO servicioDTO);

	public void eliminarServicio(ServicioDTO servicioDTO);

	public Integer generarNumeracionCodServicio();
	
	public void deshabilitarServicio(ServicioDTO servicioDTO);

}
