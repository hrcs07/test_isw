package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;

/**
 * @author renzo
 *
 */
public interface GrupoAtencionService {
	
	public void guardarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);

	public List<GrupoAtencionDTO> obtenerGruposAtencion();

	public void actualizarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);

	public void eliminarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);

	public Integer generarNumeracionCodGrupoAtencion();
}
