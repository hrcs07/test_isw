package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.ComponenteDTO;
import com.unmsm.software.persistencia.dto.ServicioDTO;

public interface ComponenteService {
	
	public void guardarComponente(ComponenteDTO componenteDTO);

	public List<ComponenteDTO> obtenerComponentes(ServicioDTO servicioDTO);

	public void actualizarComponente(ComponenteDTO componenteDTO);

	public void eliminarComponente(ComponenteDTO componenteDTO);

}
