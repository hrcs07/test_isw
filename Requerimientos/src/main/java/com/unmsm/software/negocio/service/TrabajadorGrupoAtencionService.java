package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.persistencia.dto.TrabajadorGrupoAtencionDTO;

public interface TrabajadorGrupoAtencionService {
	
	public List<TrabajadorDTO> obtenerTrabajadoresGrupoAtencion(GrupoAtencionDTO grupoAtencion);

	public void agregarUsuarioAGrupoAtencion(List<TrabajadorDTO> listaTrabajadoresAGuardar,
			List<TrabajadorDTO> listaTrabajadoresTemporales, GrupoAtencionDTO grupoAtencion);

	public void eliminarUsuarioDeGrupoAtencion(TrabajadorGrupoAtencionDTO trabajadorGrupoAtencion);

}
