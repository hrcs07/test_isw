package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.UsuarioDTO;

/**
 * @author renzo
 *
 */
public interface UsuariosService {

	public void guardarUsuario(UsuarioDTO usuarioDTO);

	public List<UsuarioDTO> obtenerUsuarios();

	public void actualizarUsuario(UsuarioDTO usuarioDTO, UsuarioDTO usuarioDTOTemporal);

	public void eliminarUsuario(UsuarioDTO usuarioDTO);

	public void deshabilitarUsuario(UsuarioDTO usuarioDTO);

	public UsuarioDTO buscarUsuario(String username, String password);
}
