package com.unmsm.software.negocio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.TrabajadorService;
import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.persistencia.mapper.TrabajadorMapper;
import com.unmsm.software.presentacion.model.Trabajador;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

/**
 * @author renzo
 *
 */
@Service
@Transactional
public class TrabajadorServiceImpl implements TrabajadorService{

	@Autowired
	private TrabajadorMapper trabajadorMapper;
	
	@Override
	public List<TrabajadorDTO> obtenerTrabajadores() {
		return trabajadorMapper.obtenerTrabajadores();
	}

	@Override
	public void guardarTrabajador(TrabajadorDTO trabajadorDTO) {
		trabajadorMapper.insertarTrabajador(trabajadorDTO);
		
	}
	
	public void setTrabajadorMapper(TrabajadorMapper trabajadorMapper) {
		this.trabajadorMapper = trabajadorMapper;
	}

	@Override
	public void enviarEmail(Integer idServicio) {
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("ID Servicio: " + idServicio);
		Integer idGrupoAtencion=trabajadorMapper.obtenerGrupoAtencion(idServicio);
		System.out.println("ID Grupo Atencion: " + idGrupoAtencion);
		System.out.println("Trabajadores del grupo de atencion: ");
		for(Trabajador trabajador : trabajadorMapper.obtenerListaTrabajadores1(idGrupoAtencion)){
			Integer idTrabajador = trabajador.getId_trabajador();
			Integer idUsuario = trabajadorMapper.obtenerIdUsuario(idTrabajador);
			String correoUsuario = trabajadorMapper.obtenerCorreoUsuario(idUsuario);
			System.out.println("ID de Trabajador: " + idTrabajador + "- ID de Usuario: " + idUsuario + " - Correo Usuario: " + correoUsuario);
			trabajador.enviarCorreos(correoUsuario);
		}
		System.out.println("--------------------------------------------------------------------------------");

		
	}
	
	@Override
	public List<Trabajador> obtenerListaTrabajadores(){
		List<Trabajador> listaTrabajador = null;
		try {
			listaTrabajador = trabajadorMapper.obtenerListaTrabajadores();
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			System.out.println("Error: " + persistenciaExcepcion);
		}
		return listaTrabajador;
	}
	
	@Override
	public String obtenerNombreTrabajador(Integer idUsuario){
		String num="";
		try {
			num = trabajadorMapper.obtenerCorreoUsuario(idUsuario);
		} catch (Exception e) {
			System.out.println("Error maper: " + e);
		}
		return num;
	}
	
	
	@Override
	public Integer getIdUsuario(String user){
		return trabajadorMapper.getIdUsuario(user);
	}
}
