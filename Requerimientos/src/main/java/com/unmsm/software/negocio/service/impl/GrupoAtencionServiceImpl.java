package com.unmsm.software.negocio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.GrupoAtencionService;
import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;
import com.unmsm.software.persistencia.mapper.GrupoAtencionMapper;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Service
@Transactional
public class GrupoAtencionServiceImpl implements GrupoAtencionService {

	@Autowired
	private GrupoAtencionMapper grupoAtencionMapper;

	@Override
	public void guardarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO) {
		try {
			grupoAtencionMapper.insertarGrupoAtencion(grupoAtencionDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
	}

	@Override
	public List<GrupoAtencionDTO> obtenerGruposAtencion() {
		List<GrupoAtencionDTO> listaGruposAtencion = null;
		try {
			listaGruposAtencion = grupoAtencionMapper.obtenerGruposAtencion();
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
		return listaGruposAtencion;
	}

	@Override
	public void actualizarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO) {
		try {
			grupoAtencionMapper.actualizarGrupoAtencion(grupoAtencionDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}
	}

	@Override
	public void eliminarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO) {
		try {
			grupoAtencionMapper.eliminarGrupoAtencion(grupoAtencionDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}

	}

	@Override
	public Integer generarNumeracionCodGrupoAtencion() {
		Integer idGrupoAtencionGenerado = null;
		try {
			idGrupoAtencionGenerado = grupoAtencionMapper.obtenerMaxIdGrupoAtencion();
			if (idGrupoAtencionGenerado == null)
				idGrupoAtencionGenerado = 1;
			else
				idGrupoAtencionGenerado++;
			return idGrupoAtencionGenerado;
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		}

	}

}
