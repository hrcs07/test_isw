package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;

public interface ArchivoTicketService {
	
	public void guardarArchivos(List<ArchivoTicketDTO> listaArchivos);
	
	public List<ArchivoTicketDTO> buscarArchivosTicket(Integer idTicket);
	
	public List<ArchivoTicketDTO> buscarArchivosMensaje(Integer idMensaje);
}
