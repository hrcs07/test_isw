package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.MensajeDTO;

public interface MensajeService {
	
	public List<MensajeDTO> obtenerMensajesTicket(Integer idTicket);
	
	public void registrarMensaje(MensajeDTO mensajeDTO); 
	
	public Integer ultimoMensaje();
}
