package com.unmsm.software.negocio.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.presentacion.model.Trabajador;

/**
 * @author renzo
 *
 */
public interface TrabajadorService {
	public List<TrabajadorDTO> obtenerTrabajadores();
	public void guardarTrabajador(TrabajadorDTO trabajadorDTO);
	
	public void enviarEmail(Integer idServicio);
	
	public List<Trabajador> obtenerListaTrabajadores();
	
	public String obtenerNombreTrabajador(Integer idUsuario);
	
	public Integer getIdUsuario(String user);
	
	
}
