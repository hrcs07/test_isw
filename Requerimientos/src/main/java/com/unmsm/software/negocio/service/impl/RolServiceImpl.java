package com.unmsm.software.negocio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.RolService;
import com.unmsm.software.persistencia.dto.TablaMaestraDTO;
import com.unmsm.software.persistencia.mapper.TablaMaestraMapper;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.clases.TablaMaestra;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

/**
 * @author renzo
 *
 */
@Service
@Transactional
public class RolServiceImpl implements RolService {

	@Autowired
	private TablaMaestraMapper tablaMaestraMapper;

	@Override
	public List<TablaMaestraDTO> obtenerRoles() {
		List<TablaMaestraDTO> listaRolesDtos = null;
		try {
			listaRolesDtos = tablaMaestraMapper.obtenerRegistroTablaMaestraPorIdPrim(TablaMaestra.TABLA_ROL.getId());
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), persistenciaExcepcion);
		}
		return listaRolesDtos;
	}

	@Override
	public void guardarRol(TablaMaestraDTO tablaMaestraDTO) {
		try {
			tablaMaestraMapper.insertarSLA(tablaMaestraDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_INSERCION_REGISTRO.getMensaje(), persistenciaExcepcion);
		}
	}

	@Override
	public void actualizarRol(TablaMaestraDTO tablaMaestraDTO) {
		try {
			tablaMaestraMapper.actualizarRegistroTablaMaestra(tablaMaestraDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_MODIFICACION_REGISTRO.getMensaje(), persistenciaExcepcion);
		}

	}

	@Override
	public void eliminarRol(TablaMaestraDTO tablaMaestraDTO) {
		try {
			tablaMaestraMapper.eliminarRegistroTablaMaestra(tablaMaestraDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_ELIMINACION_REGISTRO.getMensaje(), persistenciaExcepcion);
		}

	}

	@Override
	public Integer obtenerMaxIDRol() {
		Integer idRolGenerado = null;
		try {
			idRolGenerado = tablaMaestraMapper.obtenerMaxIdSecTablaMaestraPorIdPrim(TablaMaestra.TABLA_ROL.getId());
			if (idRolGenerado == null)
				idRolGenerado = 1;
			else
				idRolGenerado++;
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_GENERAL_LOGICA_NEGOCIO.getMensaje(), persistenciaExcepcion);
		}
		return idRolGenerado;
	}

}
