package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unmsm.software.negocio.service.FavoritoService;
import com.unmsm.software.persistencia.dto.FavoritoDTO;
import com.unmsm.software.persistencia.mapper.FavoritoMapper;

@Service
public class FavoritoServiceImpl implements FavoritoService{

	@Autowired
	private FavoritoMapper favoritoMapper;
	
	@Override
	public List<FavoritoDTO> obtenerServiciosFavoritos(Integer idCliente) {
		List<FavoritoDTO> listaFavoritos = new ArrayList<FavoritoDTO>();
		try{
			listaFavoritos = favoritoMapper.obtenerServicioFavorito(idCliente);
		}catch(Exception e){
			System.out.println("Error al listar favoritos del cliente");
		}
		return listaFavoritos;
	}

}
