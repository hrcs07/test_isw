package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.FavoritoDTO;

public interface FavoritoService {

	public List<FavoritoDTO> obtenerServiciosFavoritos(Integer idCliente);
	
}
