package com.unmsm.software.negocio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.SlaService;
import com.unmsm.software.persistencia.dto.SlaDTO;
import com.unmsm.software.persistencia.mapper.SlaMapper;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

/**
 * @author renzo broncano
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class SlaServiceImpl implements SlaService {

	@Autowired
	private SlaMapper slaMapper;

	@Override
	public void guardarSLA(SlaDTO slaDTO) {
		try {
			this.slaMapper.insertarSLA(slaDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_INSERCION_REGISTRO.getMensaje(), persistenciaExcepcion);
		}
	}

	@Override
	public List<SlaDTO> obtenerSLAs() {
		List<SlaDTO> listaSLaDtos = null;
		try {
			listaSLaDtos = slaMapper.obtenerSLAs();
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_CONSULTA_REGISTRO.getMensaje(), persistenciaExcepcion);
		}
		return listaSLaDtos;
	}

	@Override
	public void actualizarSLA(SlaDTO slaDTO) {
		try {
			slaMapper.actualizarSLA(slaDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_MODIFICACION_REGISTRO.getMensaje(), persistenciaExcepcion);
		}
	}

	@Override
	public void eliminarSLA(SlaDTO slaDTO) {
		try {
			slaMapper.eliminarSLA(slaDTO);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(persistenciaExcepcion.getMessage(), persistenciaExcepcion);
		} catch (Exception e) {
			throw new NegocioExcepcion(Errores.ERROR_NEGOCIO_ELIMINACION_REGISTRO.getMensaje(), e);
		}
	}

	@Override
	public Integer generarNumeracionCodSLA() {
		Integer idSLAGenerado = null;
		try {
			idSLAGenerado = slaMapper.obtenerMaxIDSLA();
			if (idSLAGenerado == null)
				idSLAGenerado = 1;
			else
				idSLAGenerado++;
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_GENERAL_LOGICA_NEGOCIO.getMensaje(), persistenciaExcepcion);
		}
		return idSLAGenerado;
	}

	public SlaMapper getSlaMapper() {
		return slaMapper;
	}

	public void setSlaMapper(SlaMapper slaMapper) {
		this.slaMapper = slaMapper;
	}

}
