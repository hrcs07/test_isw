package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.negocio.service.TicketService;
import com.unmsm.software.persistencia.dto.TablaMaestraDTO;
import com.unmsm.software.persistencia.dto.TicketDTO;
import com.unmsm.software.persistencia.mapper.TablaMaestraMapper;
import com.unmsm.software.persistencia.mapper.TicketMapper;
import com.unmsm.software.util.clases.Errores;
import com.unmsm.software.util.clases.TablaMaestra;
import com.unmsm.software.util.excepciones.NegocioExcepcion;
import com.unmsm.software.util.excepciones.PersistenciaExcepcion;

@Service
@Transactional
public class TicketServiceImpl implements TicketService{
	
	@Autowired
	private TicketMapper ticketMapper;
	
	@Autowired
	private TablaMaestraMapper tablaMaestraMapper;

	@Override
	public List<TicketDTO> obtenerTicketsCliente(Integer idCliente) {
		List<TicketDTO> listaTickets = new ArrayList<TicketDTO>();
		try{
			listaTickets = ticketMapper.obtenerTicketsCliente(idCliente);
		}catch(Exception e){
			System.out.println("Error al obtener los ticket del cliente");
		}
		return listaTickets;
	}
	
	@Override
	public void guardarTicket(TicketDTO ticketDTO){
		ticketMapper.insertarTicket(ticketDTO);
	}

	@Override
	public List<TicketDTO> filtrarListaTickets(List<TicketDTO> listaTickets, String estadoTicket) {
		List<TicketDTO> listTicketsFilter = new ArrayList<>();
		
		for(TicketDTO ticketDTO : listaTickets){
			if(ticketDTO.getEstadoTicket().equals(estadoTicket))
				listTicketsFilter.add(ticketDTO);
		}
		
		return listTicketsFilter;
	}

	@Override
	public int cantidadTicketsEstado(List<TicketDTO> listTickets, String estadoTicket) {
		int cantidadTickets = 0;

		for(TicketDTO ticketDTO : listTickets){
			if(ticketDTO.getEstadoTicket().equals(estadoTicket))
				cantidadTickets++;
		}

		return cantidadTickets;
	}
	
	@Override
	public Integer generarNumeracionCodTicket() {
		Integer idTicket = null;
		try {
			idTicket = ticketMapper.obtenerMaxIDTicket();
			if (idTicket == null)
				idTicket = 1;
			else
				idTicket++;
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_GENERAL_LOGICA_NEGOCIO.getMensaje(), persistenciaExcepcion);
		}
		return idTicket;
	}

	@Override
	public List<TablaMaestraDTO> obtenerPrioridades() {
		List<TablaMaestraDTO> listaPrioridades = null;
		try {
			listaPrioridades = tablaMaestraMapper.obtenerRegistroTablaMaestraPorIdPrim(TablaMaestra.TABLA_PRIORIDAD.getId());
		} catch (Exception e) {
			System.out.println("Error al listar prioridades: "+e);
		}
		return listaPrioridades;
	}

	public TicketMapper getTicketMapper() {
		return ticketMapper;
	}

	public void setTicketMapper(TicketMapper ticketMapper) {
		this.ticketMapper = ticketMapper;
	}
	
	@Override
	public List<TicketDTO> obtenerTicketsInicial(){
		List<TicketDTO> listaTickets = null;
		try {
			listaTickets = ticketMapper.obtenerTicketsInicial();
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), persistenciaExcepcion);
		}
		return listaTickets;
	}
	
	@Override
	public List<TicketDTO> obtenerTickets(Integer idTecnico){
		List<TicketDTO> listaTickets = null;
		try {
			listaTickets = ticketMapper.obtenerTickets(idTecnico);
		} catch (PersistenciaExcepcion persistenciaExcepcion) {
			throw new NegocioExcepcion(Errores.ERROR_ACCESO_DATOS_NO_CATEGORIZADO.getMensaje(), persistenciaExcepcion);
		}
		return listaTickets;
	}
	
	@Override
	public Integer numeroTicketsNuevos(){
		return ticketMapper.numeroTicketsNuevos();
	}
	
	@Override
	public Integer numeroTicketsEnEspera(){
		return ticketMapper.numeroTicketsEnEspera();
	}
	
	@Override
	public Integer numeroTicketsEnAtencion(){
		return ticketMapper.numeroTicketsEnAtencion();
	}
	
	@Override
	public Integer numeroTicketsFinalizado(){
		return ticketMapper.numeroTicketsFinalizado();
	}
	
	@Override
	public Integer numeroTicketsTotal(){
		return ticketMapper.numeroTicketsTotal();
	}
	
	@Override
	public Integer numeroSemaforoVerde(){
		return ticketMapper.numeroSemaforoVerde();
	}
	
	@Override
	public Integer numeroSemaforoAmarillo(){
		return ticketMapper.numeroSemaforoAmarillo();
	}
	
	@Override
	public Integer numeroSemaforoRojo(){
		return ticketMapper.numeroSemaforoRojo();
	}
	
	@Override
	public void aceptarTicket(Integer idTicket, Integer idTrabajador){
		ticketMapper.modificarEstadoTicket(idTicket, idTrabajador);
		System.out.println("Ticket con id: " + idTicket + " - Nuevo Estado : En Atencion");
	}
	
	@Override
	public void reasignacionTicket( Integer idTicket, Integer idTrabajador){
		Date date = new Date();
		String id_s = String.valueOf(date.getHours()) + String.valueOf(date.getMinutes()) + String.valueOf(date.getMinutes());
		int id = Integer.parseInt(id_s);
		System.out.println("ID: "+id);
		System.out.println("Ticket Reasignado con exito: " + idTrabajador + "::" + idTicket);
		ticketMapper.reasignacionTicket(idTicket, idTrabajador);
		System.out.println("Ticket Reasignado con exito");
	}
	
	@Override
	public String obtenerNombreTrabajador(Integer idUsuario){
		String num="";
		try {
			num = ticketMapper.obtenerNombreTrabajador(idUsuario);
		} catch (Exception e) {
			System.out.println("Error maper: " + e);
		}
		return num;
	}
}
