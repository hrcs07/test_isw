package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.presentacion.model.Categoria;

public interface CategoriaService {

	public List<Categoria> getCategorias();

	public List<Categoria> listarCategoriaPrimerNivel();

	public List<Categoria> listarCategoriaSegundoNivel(Integer idCategoriaPadre);

	public List<Categoria> listarCategoriaTercerNivel(Integer idCategoriaPadre);

	public void insertarCategoria(Categoria categoria);

	public void editarCategoria(Categoria categoria);

	public void deshabilitarCategoria(Categoria categoria);

	public void eliminarCategoria(Categoria categoria);

	public void habilitarCategoria(Categoria categoria);

	public Integer obtenerMaxIdCategoria();

	public String secuenciaCategorias(Integer idServicio);
}
