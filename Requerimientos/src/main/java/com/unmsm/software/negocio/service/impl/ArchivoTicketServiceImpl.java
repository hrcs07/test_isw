package com.unmsm.software.negocio.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unmsm.software.negocio.service.ArchivoTicketService;
import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;
import com.unmsm.software.persistencia.mapper.ArchivoTicketMapper;

@Service
public class ArchivoTicketServiceImpl implements ArchivoTicketService{

	@Autowired
	private ArchivoTicketMapper archivoTicketMapper;
	
	
	@Override
	public void guardarArchivos(List<ArchivoTicketDTO> listaArchivos) {
		try{
			for(ArchivoTicketDTO archivo : listaArchivos){
				archivoTicketMapper.insertarArchivo(archivo);
			}
		}catch(Exception e){
			System.out.println("Error al guardar archivo: "+e);
		}
	}


	@Override
	public List<ArchivoTicketDTO> buscarArchivosTicket(Integer idTicket) {
		List<ArchivoTicketDTO> listaArchivosTicket = new ArrayList<ArchivoTicketDTO>();
		try{
			listaArchivosTicket = archivoTicketMapper.buscarArchivosTicket(idTicket);
		}catch(Exception e){
			System.out.println("Error al buscar archivos del ticket: "+e);
		}
		return listaArchivosTicket;
	}


	@Override
	public List<ArchivoTicketDTO> buscarArchivosMensaje(Integer idMensaje) {
		List<ArchivoTicketDTO> listaArchivosMensaje = new ArrayList<ArchivoTicketDTO>();
		try{
			listaArchivosMensaje = archivoTicketMapper.buscarArchivosMensaje(idMensaje);
		}catch(Exception e){
			System.out.println("Error al buscar archivos del mensaje: "+e);
		}
		return listaArchivosMensaje;
	}

}
