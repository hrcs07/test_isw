package com.unmsm.software.negocio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unmsm.software.negocio.service.ClienteService;
import com.unmsm.software.persistencia.dto.ClienteDTO;
import com.unmsm.software.persistencia.mapper.ClienteMapper;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteMapper clienteMapper;

	@Override
	public void guardarCliente(ClienteDTO clienteDTO) {
		clienteMapper.insertarCliente(clienteDTO);
	}

	@Override
	public ClienteDTO buscarCliente(Integer idCliente) {
		ClienteDTO clienteDTO = new ClienteDTO();
		try {
			clienteDTO = clienteMapper.buscarCliente(idCliente);
		} catch (Exception e) {
			System.out.println("Error al buscar Cliente: " + e);
		}
		return clienteDTO;
	}

}
