package com.unmsm.software.negocio.service;

import java.util.List;

import com.unmsm.software.persistencia.dto.SlaDTO;

/**
 * @author renzo
 *
 */
public interface SlaService {
	
	public void guardarSLA(SlaDTO slaDTO);

	public List<SlaDTO> obtenerSLAs();
	
	public void actualizarSLA(SlaDTO slaDTO);
	
	public void eliminarSLA(SlaDTO slaDTO);
	
	public Integer generarNumeracionCodSLA();
}
