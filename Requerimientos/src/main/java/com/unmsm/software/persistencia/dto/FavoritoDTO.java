package com.unmsm.software.persistencia.dto;

public class FavoritoDTO {
	
	private Integer idFavorito;
	private Integer idCliente;
	private Integer idServicio;
	
	/**
	 * descNomServico : almacena el nombre del servicio del ticket favorito	
	 */
	private String descNomServico;
	
	
	public FavoritoDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdFavorito() {
		return idFavorito;
	}

	public void setIdFavorito(Integer idFavorito) {
		this.idFavorito = idFavorito;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public String getDescNomServico() {
		return descNomServico;
	}

	public void setDescNomServico(String descNomServico) {
		this.descNomServico = descNomServico;
	}
}
