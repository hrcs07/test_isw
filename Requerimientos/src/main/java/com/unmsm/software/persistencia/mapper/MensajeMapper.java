package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.unmsm.software.persistencia.dto.MensajeDTO;

public interface MensajeMapper {

	@Select(value = "SELECT m.id_mensaje as idMensaje,m.id_usuario as idUsuario,m.id_ticket as idTicket,m.mensaje as mensaje,m.fecha_mensaje as fechaMensaje,CONCAT(tm.descripcion,': ',u.nombre) as nomUsuario "
			+ "FROM MENSAJE m, TABLA_MAESTRA tm, USUARIO u "
			+ "WHERE id_ticket = #{idTicket} and m.id_usuario=u.id_usuario "
			+ "and tm.id_prim=1 and tm.id_sec = u.tb_rol ORDER BY m.id_mensaje DESC")
	@Results( value = { @Result(javaType = MensajeDTO.class), 
			@Result(property = "idMensaje", column = "idMensaje"),
			@Result(property = "idUsuario", column = "idUsuario"), 
			@Result(property = "idTicket", column = "idTicket"),
			@Result(property = "mensaje", column = "mensaje"),
			@Result(property = "fechaMensaje", column = "fechaMensaje"),
			@Result(property = "decNombreUsuario", column = "nomUsuario")})
	public List<MensajeDTO> obtenerListaMensaje(@Param("idTicket") Integer idTicket);
	
	@Insert("INSERT INTO mensaje(id_mensaje, id_usuario, id_ticket, mensaje, fecha_mensaje) VALUES (#{idMensaje},#{idUsuario},#{idTicket},#{mensaje},#{fechaMensaje})")
	public void guardarMensaje(MensajeDTO mensajeDTO);
	
	@Select("SELECT max(id_mensaje) FROM mensaje")
	public Integer obtenerMaxIDMensaje();
}
