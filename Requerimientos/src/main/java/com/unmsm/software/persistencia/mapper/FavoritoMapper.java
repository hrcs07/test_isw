package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.unmsm.software.persistencia.dto.FavoritoDTO;

public interface FavoritoMapper {
	
	@Results({ @Result(property = "idFavorito", column = "id_favoritos"), 
		@Result(property = "idCliente", column = "id_cliente"),
		@Result(property = "idServicio", column = "id_servicio"),
		@Result(property = "descNomServico", column = "nombre_servicio") })

	@Select("SELECT fav.id_favoritos,fav.id_cliente, fav.id_servicio, serv.nombre_servicio "
			+ "FROM favoritos fav, servicio serv "
			+ "WHERE fav.id_cliente = #{idCliente} and serv.id_servicio = fav.id_servicio")
	public List<FavoritoDTO> obtenerServicioFavorito(@Param("idCliente") Integer idCliente);

	@Insert("INSERT INTO favoritos (id_favoritos,id_cliente,id_servicio) VALUES (#{idFavorito},#{idCliente},#{idServicio})")
	public void insertarFavorito(FavoritoDTO favoritoDTO);
	
}
