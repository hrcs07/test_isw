package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.unmsm.software.persistencia.dto.ComponenteDTO;

public interface ComponenteMapper {
	@Results({ @Result(property = "idComponente", column = "id_componente"),
		@Result(property = "idServicio", column = "id_servicio"),
			@Result(property = "nombreComponente", column = "nombre_componente"),
			@Result(property = "ctTipoComponente", column = "tipo_componente"),
			@Result(property = "descTipoComponente", column = "descripcion"),
			@Result(property = "estadoComponente", column = "estado")})
	@Select("SELECT cp.id_componente as id_componente,cp.id_servicio as id_servicio,"
			+ "cp.nombre_componente as nombre_componente,cp.tipo_componente as tipo_componente,"
			+ "tm.descripcion as descripcion,cp.estado as estado"
			+ " FROM componente cp, tabla_maestra tm "
			+ " WHERE cp.tipo_componente = tm.id_sec and tm.id_prim = 3 and cp.id_servicio = #{idServicio}")
	public List<ComponenteDTO> obtenerComponentes(@Param("idServicio") Integer idServicio);

	@Insert("INSERT INTO componente (id_componente,id_servicio,nombre_componente,tipo_componente,estado) VALUES (#{idComponente},#{idServicio},#{nombreComponente},#{ctTipoComponente},#{estadoComponente})")
	public void insertarComponente(ComponenteDTO componenteDTO);

	@Update("UPDATE componente SET id_servicio = #{idServicio},nombre_componente = #{nombreComponente},tipo_componente = #{ctTipoComponente},estado = #{estadoComponente} WHERE id_componente = #{idComponente}")
	public void actualizarComponente(ComponenteDTO componenteDTO);

	@Delete("DELETE FROM componente WHERE id_componente = #{idComponente}")
	public void eliminarComponente(ComponenteDTO componenteDTO);

//	@Select("SELECT max(id_SLA) FROM componente")
//	public Integer obtenerMaxIDComponente();
}
