package com.unmsm.software.persistencia.dto;

public class GrupoAtencionDTO {

	private Integer idGrupoAtencion;
	private String codigo;
	private String titulo;

	public Integer getIdGrupoAtencion() {
		return idGrupoAtencion;
	}

	public void setIdGrupoAtencion(Integer idGrupoAtencion) {
		this.idGrupoAtencion = idGrupoAtencion;
	}


	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
