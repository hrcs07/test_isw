package com.unmsm.software.persistencia.dto;

public class TrabajadorGrupoAtencionDTO {

	private Integer idTrabajadorGrupAtencion;
	private Integer idGrupoAtencion;

	private Integer idTrabajador;
	private String desDatosTrabajador;

	public Integer getIdTrabajadorGrupAtencion() {
		return idTrabajadorGrupAtencion;
	}

	public void setIdTrabajadorGrupAtencion(Integer idTrabajadorGrupAtencion) {
		this.idTrabajadorGrupAtencion = idTrabajadorGrupAtencion;
	}

	public Integer getIdGrupoAtencion() {
		return idGrupoAtencion;
	}

	public void setIdGrupoAtencion(Integer idGrupoAtencion) {
		this.idGrupoAtencion = idGrupoAtencion;
	}

	public Integer getIdTrabajador() {
		return idTrabajador;
	}

	public void setIdTrabajador(Integer idTrabajador) {
		this.idTrabajador = idTrabajador;
	}

	public String getDesDatosTrabajador() {
		return desDatosTrabajador;
	}

	public void setDesDatosTrabajador(String desDatosTrabajador) {
		this.desDatosTrabajador = desDatosTrabajador;
	}

}
