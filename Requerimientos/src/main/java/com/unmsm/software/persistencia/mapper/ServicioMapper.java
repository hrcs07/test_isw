package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.persistencia.dto.ServicioDTO;

@Transactional(propagation = Propagation.MANDATORY)
public interface ServicioMapper {
	@Results(value = { @Result(javaType = ServicioDTO.class), @Result(property = "idServicio", column = "idServicio"),
			@Result(property = "nombreServicio", column = "nombreServicio"),
			@Result(property = "codigoServicio", column = "codigoServicio"),
			@Result(property = "idCategoria", column = "idCategoria"),
			@Result(property = "desCategoria", column = "desCategoria"),
			@Result(property = "idGrupoAtencion", column = "idGrupoAtencion"),
			@Result(property = "desCodTitGrupoAtencion", column = "desCodTitGrupoAtencion"),
			@Result(property = "idSLA", column = "idSLA"), @Result(property = "desCategoria", column = "desCategoria"),
			@Result(property = "desCodTitSLA", column = "desCodTitSLA"),
			@Result(property = "estadoServicio", column = "estado") })
	@Select("SELECT serv.id_servicio as idServicio,serv.nombre_servicio as nombreServicio,serv.codigo as codigoServicio, "
			+ "cat.id_categoria as idCategoria,cat.descripcion as desCategoria,"
			+ "grat.id_grupo_atencion  as idGrupoAtencion,CONCAT(grat.codigo,'-',grat.titulo) as desCodTitGrupoAtencion,"
			+ "sla.id_SLA as idSLA,CONCAT(sla.codigo,'-',sla.descripcion) as desCodTitSLA,serv.estado as estado "
			+ " FROM servicio serv,categoria cat, grupo_atencion grat,sla sla " + " WHERE serv.id_SLA = sla.id_SLA "
			+ " and cat.id_categoria = serv.id_categoria " + " and grat.id_grupo_atencion = serv.id_grupo_atencion ")
	public List<ServicioDTO> obtenerServicios();

	@Insert("INSERT INTO servicio (id_servicio,id_categoria,id_grupo_atencion,id_SLA,codigo,nombre_servicio,estado) VALUES (#{idServicio},#{idCategoria},#{idGrupoAtencion},#{idSLA},#{codigoServicio},#{nombreServicio},#{estadoServicio})")
	public void insertarServicio(ServicioDTO servicioDTO);

	@Select("SELECT serv.id_servicio as idServicio, serv.nombre_servicio as nombreServicio "
			+ "FROM servicio serv WHERE serv.id_categoria = #{idCat}")
	public List<ServicioDTO> obtenerServiciosPorCategoria(@Param("idCat") Integer idCategoria);

	@Select(value = "SELECT serv.id_servicio as idServicio, serv.id_categoria as idCategoria, "
			+ "serv.id_grupo_atencion as idGrupoAtencion, serv.id_SLA as idSLA, serv.codigo as codigoServicio, "
			+ "serv.nombre_servicio as nombreServicio FROM servicio serv WHERE serv.id_servicio = #{idServ}")
	@Results(value = { @Result(javaType = ServicioDTO.class), @Result(property = "idServicio", column = "id_servicio"),
			@Result(property = "idCategoria", column = "id_categoria"),
			@Result(property = "idGrupoAtencion", column = "id_grupo_atencion"),
			@Result(property = "idSLA", column = "id_SLA"), @Result(property = "codigoServicio", column = "codigo"),
			@Result(property = "nombreServicio", column = "nombre_servicio") })
	public ServicioDTO obtenerServicio(@Param("idServ") Integer idServicio);

	@Update("UPDATE servicio SET id_categoria = #{idCategoria},id_grupo_atencion = #{idGrupoAtencion},id_SLA = #{idSLA},codigo = #{codigoServicio},nombre_servicio = #{nombreServicio} WHERE id_servicio = #{idServicio}")
	public void actualizarServicio(ServicioDTO servicioDTO);

	@Update("UPDATE servicio SET estado = #{estadoServicio} WHERE id_servicio = #{idServicio}")
	public void deshabilitarServicio(ServicioDTO servicioDTO);

	@Delete("DELETE FROM servicio WHERE id_servicio = #{idServicio}")
	public void eliminarServicio(ServicioDTO servicioDTO);

	@Select("SELECT max(id_servicio) FROM servicio")
	public Integer obtenerMaxIDServicio();
}