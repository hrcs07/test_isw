package com.unmsm.software.persistencia.dto;

/**
 * @author renzo
 *
 */
public class ServicioDTO {

	private Integer idServicio;

	private String codigoServicio;

	/**
	 * desCategoria : almacena datos de una categoria
	 */
	private Integer idCategoria;
	private String desCategoria;

	/**
	 * desCodTitGrupoAtencion : almacena Codigo - Titulo de un grupo de atencion
	 */
	private Integer idGrupoAtencion;
	private String desCodTitGrupoAtencion;

	/**
	 * desCodTitSLA : almacena Codigo-Titulo de SLA
	 */
	private Integer idSLA;
	private String desCodTitSLA;

	private String nombreServicio;

	private boolean estadoServicio;

	private String desEstadoServicio;

	public Integer getIdServicio() {
		return idServicio;
	}

	public String getDesEstadoServicio() {
		return desEstadoServicio;
	}

	public void setDesEstadoServicio(String desEstadoServicio) {
		this.desEstadoServicio = desEstadoServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getDesCategoria() {
		return desCategoria;
	}

	public void setDesCategoria(String desCategoria) {
		this.desCategoria = desCategoria;
	}

	public Integer getIdGrupoAtencion() {
		return idGrupoAtencion;
	}

	public void setIdGrupoAtencion(Integer idGrupoAtencion) {
		this.idGrupoAtencion = idGrupoAtencion;
	}

	public String getDesCodTitGrupoAtencion() {
		return desCodTitGrupoAtencion;
	}

	public void setDesCodTitGrupoAtencion(String desCodTitGrupoAtencion) {
		this.desCodTitGrupoAtencion = desCodTitGrupoAtencion;
	}

	public Integer getIdSLA() {
		return idSLA;
	}

	public void setIdSLA(Integer idSLA) {
		this.idSLA = idSLA;
	}

	public String getDesCodTitSLA() {
		return desCodTitSLA;
	}

	public void setDesCodTitSLA(String desCodTitSLA) {
		this.desCodTitSLA = desCodTitSLA;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public boolean isEstadoServicio() {
		return estadoServicio;
	}

	public void setEstadoServicio(boolean estadoServicio) {
		this.estadoServicio = estadoServicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idServicio == null) ? 0 : idServicio.hashCode());
		result = prime * result + ((idGrupoAtencion == null) ? 0 : idGrupoAtencion.hashCode());
		result = prime * result + ((idCategoria == null) ? 0 : idCategoria.hashCode());
		result = prime * result + ((desCategoria == null) ? 0 : desCategoria.hashCode());

		result = prime * result + ((idGrupoAtencion == null) ? 0 : idGrupoAtencion.hashCode());
		result = prime * result + ((desCodTitGrupoAtencion == null) ? 0 : desCodTitGrupoAtencion.hashCode());

		result = prime * result + ((idSLA == null) ? 0 : idSLA.hashCode());
		result = prime * result + ((desCodTitSLA == null) ? 0 : desCodTitSLA.hashCode());
		result = prime * result + ((nombreServicio == null) ? 0 : nombreServicio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicioDTO other = (ServicioDTO) obj;
		if (idServicio == null) {
			if (other.idServicio != null)
				return false;
		} else if (!idServicio.equals(other.idServicio))
			return false;
		if (idGrupoAtencion == null) {
			if (other.idGrupoAtencion != null)
				return false;
		} else if (!idGrupoAtencion.equals(other.idGrupoAtencion))
			return false;
		if (idCategoria == null) {
			if (other.idCategoria != null)
				return false;
		} else if (!idCategoria.equals(other.idCategoria))
			return false;
		if (idSLA == null) {
			if (other.idSLA != null)
				return false;
		} else if (!idSLA.equals(other.idSLA))
			return false;
		if (nombreServicio == null) {
			if (other.nombreServicio != null)
				return false;
		} else if (!nombreServicio.equals(other.nombreServicio))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return idServicio.toString();
	}

	public int compareTo(ServicioDTO o) {
		// TODO Auto-generated method stub
		return this.getIdServicio().compareTo(o.getIdServicio());
	}

}
