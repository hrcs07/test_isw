package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.unmsm.software.persistencia.dto.GrupoAtencionDTO;

public interface GrupoAtencionMapper {

	@Results({ @Result(property = "idGrupoAtencion", column = "id_grupo_atencion"),
			@Result(property = "codigo", column = "codigo"),
			@Result(property = "titulo", column = "titulo") })
	@Select("SELECT id_grupo_atencion,codigo,titulo FROM grupo_atencion")
	public List<GrupoAtencionDTO> obtenerGruposAtencion();

	@Insert("INSERT INTO grupo_atencion (id_grupo_atencion,codigo,titulo) VALUES (#{idGrupoAtencion},#{codigo},#{titulo})")
	public void insertarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);

	@Update("UPDATE grupo_atencion SET codigo = #{codigo},titulo = #{titulo} WHERE id_grupo_atencion = #{idGrupoAtencion}")
	public void actualizarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);

	@Delete("DELETE FROM grupo_atencion WHERE id_grupo_atencion = #{idGrupoAtencion}")
	public void eliminarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);

	@Select("SELECT max(id_grupo_atencion) FROM grupo_atencion")
	public Integer obtenerMaxIdGrupoAtencion();
}
