package com.unmsm.software.persistencia.dto;

public class TrabajadorDTO {
	private Integer idTrabajador;
	private Integer idUsuario;
	private String area;
	private String descDatosTrabajador;

	public Integer getIdTrabajador() {
		return idTrabajador;
	}

	public void setIdTrabajador(Integer idTrabajador) {
		this.idTrabajador = idTrabajador;
	}

	public String getDescDatosTrabajador() {
		return descDatosTrabajador;
	}

	public void setDescDatosTrabajador(String descDatosTrabajador) {
		this.descDatosTrabajador = descDatosTrabajador;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTrabajador == null) ? 0 : idTrabajador.hashCode());
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((descDatosTrabajador == null) ? 0 : descDatosTrabajador.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrabajadorDTO other = (TrabajadorDTO) obj;
		if (idTrabajador == null) {
			if (other.idTrabajador != null)
				return false;
		} else if (!idTrabajador.equals(other.idTrabajador))
			return false;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (descDatosTrabajador == null) {
			if (other.descDatosTrabajador != null)
				return false;
		} else if (!descDatosTrabajador.equals(other.descDatosTrabajador))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return idTrabajador.toString();
	}

	public int compareTo(TrabajadorDTO o) {
		// TODO Auto-generated method stub
		return this.getIdTrabajador().compareTo(o.getIdTrabajador());
	}
}
