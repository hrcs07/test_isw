package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.unmsm.software.persistencia.dto.UsuarioDTO;

/**
 * @author renzo
 *
 */
public interface UsuarioMapper {

	@Results(value = { @Result(javaType = UsuarioDTO.class), @Result(property = "idUsuario", column = "id_usuario"),
			@Result(property = "username", column = "username"), @Result(property = "password", column = "password"),
			@Result(property = "nombre", column = "nombre"), @Result(property = "apellidos", column = "apellidos"),
			@Result(property = "ctRol", column = "tb_rol"), @Result(property = "correo", column = "correo"),
			@Result(property = "telefono", column = "telefono"),
			@Result(property = "desRolUsuario", column = "descripcion"),
			@Result(property = "estadoUsuario", column = "estado") })
	@Select("SELECT id_usuario,username,password,nombre,apellidos,tb_rol,correo,telefono,descripcion,estado"
			+ " FROM usuario ,tabla_maestra " + " WHERE tb_rol = id_sec and id_prim = 1 ")
	public List<UsuarioDTO> obtenerUsuarios();

	@Select(value = "SELECT us.id_usuario,us.username,us.password,us.nombre,us.apellidos,us.tb_rol,us.correo,us.telefono,tm.descripcion "
			+ " FROM usuario us,tabla_maestra tm "
			+ " WHERE us.username = #{user} and us.password = #{pass} and tm.id_prim = 1 and tm.id_sec = us.tb_rol")
	@Results(value = { @Result(javaType = UsuarioDTO.class), @Result(property = "idUsuario", column = "id_usuario"),
			@Result(property = "username", column = "username"), @Result(property = "password", column = "password"),
			@Result(property = "nombre", column = "nombre"), @Result(property = "apellidos", column = "apellidos"),
			@Result(property = "ctRol", column = "tb_rol"), @Result(property = "desRolUsuario", column = "descripcion"),
			@Result(property = "correo", column = "correo"), @Result(property = "telefono", column = "telefono") })
	public UsuarioDTO buscarUsuario(@Param("user") String username, @Param("pass") String password);

	@Insert("INSERT INTO usuario (username,password,nombre,apellidos,tb_rol,correo,telefono,estado) VALUES (#{username},#{password},#{nombre},#{apellidos},#{ctRol},#{correo},#{telefono},#{estadoUsuario})")
	@Options(useGeneratedKeys = true, keyProperty = "idUsuario", keyColumn = "id_usuario")
	public int insertarUsuario(UsuarioDTO usuarioDTO);

	@Update("UPDATE usuario SET username = #{username},password = #{password},nombre = #{nombre},apellidos = #{apellidos},tb_rol =#{ctRol},correo =#{correo},telefono=#{telefono} WHERE id_usuario = #{idUsuario}")
	public void actualizarUsuario(UsuarioDTO usuarioDTO);

	@Update("UPDATE usuario SET estado =#{estadoUsuario} WHERE id_usuario =#{idUsuario}")
	public void deshabilitarUsuario(UsuarioDTO usuarioDTO);

	@Delete("DELETE FROM usuario WHERE id_usuario = #{idUsuario}")
	public void eliminarUsuario(UsuarioDTO usuarioDTO);

	@Select("SELECT max(id_usuario) FROM usuario")
	public Integer obtenerMaxIdUsuario();
}
