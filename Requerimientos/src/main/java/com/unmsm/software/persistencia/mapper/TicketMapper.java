package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unmsm.software.persistencia.dto.TicketDTO;

@Transactional(propagation = Propagation.MANDATORY)
public interface TicketMapper {

	@Insert("INSERT INTO ticket(id_ticket, id_cliente, id_servicio, codigo_ticket, asunto_ticket, detalle_ticket, estado_ticket, fecha_ticket,prioridad) "
			+ "VALUES (#{idTicket},#{idCliente},#{idServicio},#{codigoTicket},#{asuntoTicket},#{detalleTicket},#{estadoTicket},#{fechaTicket},#{prioridadTicket})")
	public void insertarTicket(TicketDTO ticketDTO);

	@Select("SELECT max(id_ticket) FROM ticket")
	public Integer obtenerMaxIDTicket();

	@Select(value = "SELECT t.id_ticket, t.id_cliente, t.id_servicio, s.nombre_servicio, t.codigo_ticket, t.asunto_ticket, t.detalle_ticket, t.estado_ticket, t.fecha_ticket, t.prioridad FROM ticket t, servicio s WHERE id_cliente = #{idCliente} and t.id_servicio = s.id_servicio ORDER BY t.codigo_ticket DESC")

	@Results(value = { @Result(javaType = TicketDTO.class), @Result(property = "idTicket", column = "id_ticket"),
			@Result(property = "idCliente", column = "id_cliente"),
			@Result(property = "idServicio", column = "id_servicio"),
			@Result(property = "descNomServico", column = "nombre_servicio"),
			@Result(property = "codigoTicket", column = "codigo_ticket"),
			@Result(property = "asuntoTicket", column = "asunto_ticket"),
			@Result(property = "detalleTicket", column = "detalle_ticket"),
			@Result(property = "estadoTicket", column = "estado_ticket"),
			@Result(property = "fechaTicket", column = "fecha_ticket"),
			@Result(property = "prioridadTicket", column = "prioridad") })
	public List<TicketDTO> obtenerTicketsCliente(@Param("idCliente") Integer idCliente);
	
	@Select(value = "SELECT id_ticket, id_servicio, id_cliente, codigo_ticket, asunto_ticket, detalle_ticket, estado_ticket, fecha_ticket, prioridad FROM ticket order by id_ticket desc")
	@Results(value = { @Result(javaType = TicketDTO.class), @Result(property = "idTicket", column = "id_ticket"), @Result(property = "idCliente", column = "id_cliente"),
					   @Result(property = "idServicio", column = "id_servicio"), @Result(property = "codigoTicket", column = "codigo_ticket"),
					   @Result(property = "asuntoTicket", column = "asunto_ticket"), @Result(property = "detalleTicket", column = "detalle_ticket"),
					   @Result(property = "estadoTicket", column = "estado_ticket"), @Result(property = "fechaTicket", column = "fecha_ticket"),
					   @Result(property = "prioridad", column = "prioridad")})	
	public List<TicketDTO> obtenerTicketsInicial();
	
	@Select(value = "SELECT id_ticket, id_servicio, id_cliente, codigo_ticket, asunto_ticket, detalle_ticket, estado_ticket, fecha_ticket, prioridad FROM ticket WHERE  id_tecnico= #{idTecnico} OR estado_ticket = 'REGISTRADO' order by id_ticket desc")
	@Results(value = { @Result(javaType = TicketDTO.class), @Result(property = "idTicket", column = "id_ticket"), @Result(property = "idCliente", column = "id_cliente"),
					   @Result(property = "idServicio", column = "id_servicio"), @Result(property = "codigoTicket", column = "codigo_ticket"),
					   @Result(property = "asuntoTicket", column = "asunto_ticket"), @Result(property = "detalleTicket", column = "detalle_ticket"),
					   @Result(property = "estadoTicket", column = "estado_ticket"), @Result(property = "fechaTicket", column = "fecha_ticket"),
					   @Result(property = "prioridad", column = "prioridad")})	
	public List<TicketDTO> obtenerTickets(@Param("idTecnico") Integer idTecnico);
	
	
	@Select("SELECT count(id_ticket) FROM ticket where estado_ticket = 'REGISTRADO'")
	public Integer numeroTicketsNuevos();
	
	@Select("SELECT count(id_ticket) FROM ticket where estado_ticket = 'EN CURSO'")
	public Integer numeroTicketsEnAtencion();
	
	@Select("SELECT count(id_ticket) FROM ticket where estado_ticket = 'EN ESPERA'")
	public Integer numeroTicketsEnEspera();
	
	@Select("SELECT count(id_ticket) FROM ticket where estado_ticket = 'FINALIZADO'")
	public Integer numeroTicketsFinalizado();
	
	@Select("SELECT count(id_ticket) FROM ticket")
	public Integer numeroTicketsTotal();
	
	@Select("SELECT count(id_ticket) from ticket where prioridad = 'Baja'")
	public Integer numeroSemaforoVerde();
	
	@Select("SELECT count(id_ticket) from ticket where prioridad = 'Media'")
	public Integer numeroSemaforoAmarillo();
	
	@Select("SELECT count(id_ticket) from ticket where prioridad = 'Alta'")
	public Integer numeroSemaforoRojo();
	
	@Update("UPDATE ticket SET estado_ticket = 'EN CURSO', id_tecnico = #{idTrabajador} WHERE id_ticket = #{idTicket}")
	public void modificarEstadoTicket(@Param("idTicket") Integer idTicket, @Param("idTrabajador") Integer idTrabajador);
	
	
	@Update("UPDATE ticket SET id_tecnico = #{idTrabajador} WHERE id_ticket = #{idTicket}")
	public void reasignacionTicket(@Param("idTicket") Integer idTicket, @Param("idTrabajador") Integer idTrabajador);
	
	@Select("SELECT nombre from usuario where id_usuario = #{idUsuario}") 
	public String obtenerNombreTrabajador(@Param("idUsuario") Integer idUsuario);

}
