package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.unmsm.software.persistencia.dto.TrabajadorGrupoAtencionDTO;

public interface TrabajadorGrupoAtencionMapper {
	@Results(value={@Result(javaType = TrabajadorGrupoAtencionDTO.class), 
			@Result(property = "idTrabajadorGrupAtencion", column = "	id_gpatencion_trabajdor"),
			@Result(property = "idGrupoAtencion", column = "id_grupo_atencion"), 
			@Result(property = "idTrabajador", column = "id_trabajador"),
			@Result(property = "desDatosTrabajador", column = "desDatosTrabajador")})
	@Select("SELECT gp.id_gpatencion_trabajdor as id_gpatencion_trabajdor,gp.id_grupo_atencion as id_grupo_atencion,gp.id_trabajador as id_trabajador,"
			+ "CONCAT(usu.nombre,' ',usu.apellidos) as desDatosTrabajador"
			+ " FROM gpatencion_trabajador gp ,trabajador tb, usuario usu "
			+ " WHERE gp.id_trabajador = tb.id_trabajador and tb.id_usuario = usu.id_usuario and usu.estado = 1 ")
	public List<TrabajadorGrupoAtencionDTO> obtenerTrabajadoresGrupoAtencion();

	@Insert("INSERT INTO gpatencion_trabajador(id_gpatencion_trabajdor,id_grupo_atencion,id_trabajador) VALUES (#{idTrabajadorGrupAtencion},#{idGrupoAtencion},#{idTrabajador})")
	public void insertarUsuarioAGrupoAtencion(TrabajadorGrupoAtencionDTO trabajadorGrupoAtencionDTO);

//	@Update("UPDATE grupo_atencion SET codigo = #{codigo},titulo = #{titulo} WHERE id_grupo_atencion = #{idGrupoAtencion}")
//	public void actualizarGrupoAtencion(GrupoAtencionDTO grupoAtencionDTO);
//
	@Delete("DELETE FROM gpatencion_trabajador WHERE id_grupo_atencion = #{idGrupoAtencion} and id_trabajador = #{idTrabajador} ")
	public void eliminarGrupoAtencion(TrabajadorGrupoAtencionDTO trabajadorGrupoAtencionDTO);
//
//	@Select("SELECT max(id_grupo_atencion) FROM grupo_atencion")
//	public Integer obtenerMaxIdGrupoAtencion();
}
