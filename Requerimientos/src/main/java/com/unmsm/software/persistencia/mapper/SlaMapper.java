package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.unmsm.software.persistencia.dto.SlaDTO;

public interface SlaMapper {

	@Results({ @Result(property = "id_sla", column = "id_SLA"), @Result(property = "codigo", column = "codigo"),
			@Result(property = "descripcion", column = "descripcion"), @Result(property = "dias", column = "dias"),
			@Result(property = "horas", column = "horas"), @Result(property = "minutos", column = "minutos") })
	@Select("SELECT id_SLA,codigo,descripcion,dias,horas,minutos FROM sla")
	public List<SlaDTO> obtenerSLAs();

	@Insert("INSERT INTO sla (id_SLA,codigo,descripcion,dias,horas,minutos) VALUES (#{id_sla},#{codigo},#{descripcion},#{dias},#{horas},#{minutos})")
	public void insertarSLA(SlaDTO slaDTO);
	

	@Update("UPDATE SLA SET codigo = #{codigo},descripcion = #{descripcion},dias = #{dias},horas = #{horas},minutos =#{minutos} WHERE id_SLA = #{id_sla}")
	public void actualizarSLA(SlaDTO slaDTO);

	@Delete("DELETE FROM sla WHERE id_SLA = #{id_sla}")
	public void eliminarSLA(SlaDTO slaDTO);
	
	@Select("SELECT max(id_SLA) FROM sla")
	public Integer obtenerMaxIDSLA();
}
