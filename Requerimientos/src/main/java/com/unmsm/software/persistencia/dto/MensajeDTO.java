package com.unmsm.software.persistencia.dto;

import java.util.Date;
import java.util.List;

public class MensajeDTO {
	
	private Integer idMensaje;
	private Integer idUsuario;
	
	/**
	 * decNombreUsuario   : almacena el nombre del usuario que envio el mensaje
	 */
	private String decNombreUsuario;
	
	private Integer idTicket;
	private String mensaje;
	private Date fechaMensaje;
	
	private List<ArchivoTicketDTO> listaArchivosMensaje;
	
	public MensajeDTO() {
		
	}

	public List<ArchivoTicketDTO> getListaArchivosMensaje() {
		return listaArchivosMensaje;
	}

	public void setListaArchivosMensaje(List<ArchivoTicketDTO> listaArchivosMensaje) {
		this.listaArchivosMensaje = listaArchivosMensaje;
	}

	public Integer getIdMensaje() {
		return idMensaje;
	}

	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}

	public Integer getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(Integer idTicket) {
		this.idTicket = idTicket;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Date getFechaMensaje() {
		return fechaMensaje;
	}

	public void setFechaMensaje(Date fechaMensaje) {
		this.fechaMensaje = fechaMensaje;
	}

	public String getDecNombreUsuario() {
		return decNombreUsuario;
	}

	public void setDecNombreUsuario(String decNombreUsuario) {
		this.decNombreUsuario = decNombreUsuario;
	}
}
