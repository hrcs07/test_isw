package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.unmsm.software.presentacion.model.Categoria;

public interface CategoriaPersistence {

	@Select(value = "  SELECT categoria.id_categoria, categoria.descripcion, categoria.estado, categoria.nivel, categoria.cat_padre , categoria.imagen "
			+ "FROM Categoria categoria where categoria.estado=2 or categoria.estado=1 order by niv1, niv2, id_categoria ")
	@Results(value = { @Result(javaType = Categoria.class), @Result(property = "id_categoria", column = "id_categoria"),
			@Result(property = "descripcion", column = "descripcion"), @Result(property = "estado", column = "estado"),
			@Result(property = "nivel", column = "nivel"), @Result(property = "cat_padre", column = "cat_padre"),
			@Result(property = "imagen", column = "imagen")})
	public List<Categoria> categorias();
	
	@Select(value = "SELECT id_categoria,descripcion,estado FROM categoria WHERE nivel=1")
	@Results(value = { @Result(javaType = Categoria.class), 
			@Result(property = "id_categoria", column = "id_categoria"),
			@Result(property = "descripcion", column = "descripcion"),
			@Result(property = "estado", column = "estado") })
	public List<Categoria> listarCategoriaPrimerNivel();

	@Select(value = "SELECT id_categoria,descripcion,estado FROM categoria WHERE nivel = 2 and cat_padre = #{idCategoria}")
	@Results(value = { @Result(javaType = Categoria.class), 
			@Result(property = "id_categoria", column = "id_categoria"),
			@Result(property = "descripcion", column = "descripcion"),
			@Result(property = "estado", column = "estado") })
	public List<Categoria> listarCategoriaSegundoNivel(@Param("idCategoria") Integer idCategoria);

	@Select(value = "SELECT id_categoria,descripcion,estado FROM categoria WHERE nivel = 3 and cat_padre = #{idCategoria}")
	@Results(value = { @Result(javaType = Categoria.class), 
			@Result(property = "id_categoria", column = "id_categoria"),
			@Result(property = "descripcion", column = "descripcion"),
			@Result(property = "estado", column = "estado") })
	public List<Categoria> listarCategoriaTercerNivel(@Param("idCategoria") Integer idCategoria);
	
	@Select(value = "SELECT id_categoria,descripcion,cat_padre FROM categoria WHERE id_categoria = #{idCat}")
	@Results(value = { @Result(javaType = Categoria.class),
		@Result(property = "id_categoria", column = "id_categoria"),
		@Result(property = "descripcion", column = "descripcion"),
		@Result(property = "cat_padre", column = "cat_padre")})
	public Categoria buscarCategoria(@Param("idCat") Integer idCategoria);
	
	@Insert("INSERT INTO categoria (descripcion,estado,nivel,cat_padre,imagen,NIV1,NIV2,NIV3) VALUES (#{descripcion},#{estado},#{nivel},#{cat_padre},#{imagen},#{niv1},#{niv2},#{niv3})")
	public void insertarCategoria(Categoria categoria);
	
	@Update("UPDATE categoria SET descripcion=#{descripcion}, imagen=#{imagen} where id_categoria=#{id_categoria}")
	public void editarCategoria(Categoria categoria);
	
	@Update("UPDATE categoria SET estado=1 where id_categoria=#{id_categoria}")
	public void deshabilitarCategoria(Categoria categoria);	
	
	@Update("UPDATE categoria SET estado=0 where id_categoria=#{id_categoria}")
	public void eliminarCategoria(Categoria categoria);	
	
	@Update("UPDATE categoria SET estado=2 where id_categoria=#{id_categoria}")
	public void habilitarCategoria(Categoria categoria);	
	
	@Select("SELECT max(id_categoria) FROM categoria")
	public Integer obtenerMaxIdCategoria();
	
	@Update("UPDATE categoria SET niv#{nivel}=id_categoria where id_categoria=#{id_categoria}")
	public void actualizarNiv(Categoria categoria);	
}
