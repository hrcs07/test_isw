package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.unmsm.software.persistencia.dto.TrabajadorDTO;
import com.unmsm.software.presentacion.model.Trabajador;

/**
 * @author renzo
 *
 */
public interface TrabajadorMapper {

	@Results(value = { @Result(javaType = TrabajadorDTO.class),
			@Result(property = "idTrabajador", column = "idTrabajador"),
			@Result(property = "idUsuario", column = "idUsuario"),
			@Result(property = "descDatosTrabajador", column = "nombTrabajador"),
			@Result(property = "area", column = "area") })
	@Select("SELECT trab.id_trabajador as idTrabajador, CONCAT(usur.nombre,' ',usur.apellidos) as nombTrabajador"
			+ " FROM trabajador trab,usuario usur" + " WHERE trab.id_usuario = usur.id_usuario "
			+ " and usur.tb_rol = 2 and (select count(gp.id_gpatencion_trabajdor) FROM gpatencion_trabajador gp WHERE gp.id_trabajador = trab.id_trabajador) < 1 and usur.estado = 1 ")
	public List<TrabajadorDTO> obtenerTrabajadores();

	@Insert("INSERT INTO trabajador (id_trabajador,id_usuario,area) VALUES (#{idTrabajador},#{idUsuario},#{area})")
	public void insertarTrabajador(TrabajadorDTO trabajadorDTO);
	
	@Delete("DELETE FROM trabajador WHERE id_usuario = #{idUsuario}")
	public void eliminarTrabajador(TrabajadorDTO trabajadorDTO);

//	@Results(value = { @Result(javaType = TrabajadorDTO.class),
//			@Result(property = "idTrabajador", column = "id_trabajador") })
//	@Select("SELECT id_trabajador FROM trabajador  WHERE id_usuario = #{idUsuario}")
//	public List<TrabajadorDTO> obtenerTrabajadorPorIdUsuario(@Param("idUsuario") Integer idUsuario);
	
	@Select("SELECT id_grupo_atencion from servicio where id_servicio = #{idServicio}") 
	public Integer obtenerGrupoAtencion(@Param("idServicio") Integer idServicio);
	
	@Select("SELECT id_trabajador from gpatencion_trabajador where id_grupo_atencion = #{idGrupoAtencion}")
	public List<Trabajador> obtenerListaTrabajadores1(@Param("idGrupoAtencion") Integer idGrupoAtencion);
	
	@Select("SELECT id_usuario from trabajador where id_trabajador = #{idTrabajador}") 
	public Integer obtenerIdUsuario(@Param("idTrabajador") Integer idTrabajador);
	
	@Select("SELECT correo from usuario where id_usuario = #{idUsuario}") 
	public String obtenerCorreoUsuario(@Param("idUsuario") Integer idUsuario);
	
	@Select(value = "SELECT id_trabajador, id_usuario, area FROM trabajador")
	@Results(value = { @Result(javaType = Trabajador.class), @Result(property = "id_trabajador", column = "id_trabajador"), @Result(property = "id_usuario", column = "id_usuario"),
					   @Result(property = "area", column = "area")})	
	public List<Trabajador> obtenerListaTrabajadores();
	
	@Select("SELECT nombre from usuario where id_usuario = #{idUsuario}") 
	public String obtenerNombreTrabajador(@Param("idUsuario") Integer idUsuario);
	
	@Select("SELECT id_usuario from usuario where username = #{user}") 
	public Integer getIdUsuario(@Param("user") String user);
}
