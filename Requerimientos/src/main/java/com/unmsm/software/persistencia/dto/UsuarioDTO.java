package com.unmsm.software.persistencia.dto;

/**
 * @author renzo
 *
 */
public class UsuarioDTO {

	private Integer idUsuario;
	private String username;
	private String password;
	private String nombre;
	private String apellidos;

	/**
	 * Almacenara los datos respecto al rol que se le asigno a un usuario en
	 * especifico
	 */
	private Integer ctRol;
	private String desRolUsuario;

	private String correo;
	private String telefono;

	private boolean estadoUsuario;

	private String desEstadoServicio;

	public String getDesEstadoServicio() {
		return desEstadoServicio;
	}

	public void setDesEstadoServicio(String desEstadoServicio) {
		this.desEstadoServicio = desEstadoServicio;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getCtRol() {
		return ctRol;
	}

	public void setCtRol(Integer ctRol) {
		this.ctRol = ctRol;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDesRolUsuario() {
		return desRolUsuario;
	}

	public void setDesRolUsuario(String desRolUsuario) {
		this.desRolUsuario = desRolUsuario;
	}

	public boolean isEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(boolean estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

}
