package com.unmsm.software.persistencia.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TicketDTO {

	private Integer idTicket;
	private Integer idCliente;
	private Integer idServicio;

	/**
	 * descNomServico : almacena el nombre del servicio a que pertenece el
	 * ticket
	 */
	private String descNomServico;

	private String codigoTicket;
	private String asuntoTicket;
	private String detalleTicket;
	private String estadoTicket;
	private Date fechaTicket;
	private String prioridadTicket;
	private byte[] archivoAdjunto;
	private String prioridad;
	
	private Integer idSemaforo;
	private String colorSemaforo;
	
	private String fechaModificada;
	
	private List<TicketDTO> listaTickets;
	
	private List<ArchivoTicketDTO> listaArchivosTicket;

	public TicketDTO() {

	}

	public byte[] getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(byte[] archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}	

	public List<ArchivoTicketDTO> getListaArchivosTicket() {
		return listaArchivosTicket;
	}

	public void setListaArchivosTicket(List<ArchivoTicketDTO> listaArchivosTicket) {
		this.listaArchivosTicket = listaArchivosTicket;
	}

	public Integer getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(Integer idTicket) {
		this.idTicket = idTicket;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public String getCodigoTicket() {
		return codigoTicket;
	}

	public void setCodigoTicket(String codigoTicket) {
		this.codigoTicket = codigoTicket;
	}

	public String getAsuntoTicket() {
		return asuntoTicket;
	}

	public void setAsuntoTicket(String asuntoTicket) {
		this.asuntoTicket = asuntoTicket;
	}

	public String getDetalleTicket() {
		return detalleTicket;
	}

	public void setDetalleTicket(String detalleTicket) {
		this.detalleTicket = detalleTicket;
	}

	public String getEstadoTicket() {
		return estadoTicket;
	}

	public void setEstadoTicket(String estadoTicket) {
		this.estadoTicket = estadoTicket;
	}

	public Date getFechaTicket() {
		return fechaTicket;
	}

	public void setFechaTicket(Date fechaTicket) {
		this.fechaTicket = fechaTicket;
	}

	public String getDescNomServico() {
		return descNomServico;
	}

	public void setDescNomServico(String descNomServico) {
		this.descNomServico = descNomServico;
	}

	public String getPrioridadTicket() {
		return prioridadTicket;
	}

	public void setPrioridadTicket(String prioridadTicket) {
		this.prioridadTicket = prioridadTicket;
	}
	
	public List<TicketDTO> getListaTickets() {
		return listaTickets;
	}

	public void setListaTickets(List<TicketDTO> listaTickets) {
		this.listaTickets = listaTickets;
	}

	public String getColorSemaforo() {
		if(this.idSemaforo==1){
			return "green";
		}else if(this.idSemaforo==2){
			return "yellow";
		}else{
			return "red";
		}
	}

	public void setColorSemaforo(String colorSemaforo) {
		this.colorSemaforo = colorSemaforo;
	}

	public String getFechaModificada() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String format = formatter.format(this.fechaTicket);
		return format;
	}

	public void setFechaModificada(String fechaModificada) {
		this.fechaModificada = fechaModificada;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
}
