package com.unmsm.software.persistencia.dto;

public class TablaMaestraDTO {

	private Integer idTablaMaestra;
	private Integer idPrim;
	private Integer idSec;
	private String descripcion;

	public Integer getIdTablaMaestra() {
		return idTablaMaestra;
	}

	public void setIdTablaMaestra(Integer idTablaMaestra) {
		this.idTablaMaestra = idTablaMaestra;
	}

	public Integer getIdPrim() {
		return idPrim;
	}

	public void setIdPrim(Integer idPrim) {
		this.idPrim = idPrim;
	}

	public Integer getIdSec() {
		return idSec;
	}

	public void setIdSec(Integer idSec) {
		this.idSec = idSec;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
