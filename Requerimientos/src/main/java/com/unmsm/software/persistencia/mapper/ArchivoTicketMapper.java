package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.unmsm.software.persistencia.dto.ArchivoTicketDTO;
public interface ArchivoTicketMapper {

	@Insert("INSERT INTO archivo_ticket(id_archivo, id_ticket, id_mensaje, nombre_archivo, archivo, fecha_subida) VALUES (#{idArchivo},#{idTicket},#{idMensaje},#{nombreArchivo},#{archivoBytes},#{fSubida})")
	public void insertarArchivo(ArchivoTicketDTO archivoTicketDTO);
	
	@Select(value = "SELECT id_archivo, id_ticket, id_mensaje, nombre_archivo, archivo, fecha_subida FROM archivo_ticket WHERE id_ticket = #{idTicket}")
	@Results(value = { @Result(javaType = ArchivoTicketDTO.class), 
			@Result(property = "idArchivo", column = "id_archivo"),
			@Result(property = "idTicket", column = "id_ticket"),
			@Result(property = "idMensaje", column = "id_mensaje"), 
			@Result(property = "nombreArchivo", column = "nombre_archivo"),
			@Result(property = "archivoBytes", column = "archivo"),
			@Result(property = "fSubida", column = "fecha_subida") })
	public List<ArchivoTicketDTO> buscarArchivosTicket(@Param("idTicket") Integer idTicket);
	
	@Select(value = "SELECT id_archivo, id_ticket, id_mensaje, nombre_archivo, archivo, fecha_subida FROM archivo_ticket WHERE id_mensaje = #{idMensaje}")
	@Results(value = { @Result(javaType = ArchivoTicketDTO.class), 
			@Result(property = "idArchivo", column = "id_archivo"),
			@Result(property = "idTicket", column = "id_ticket"),
			@Result(property = "idMensaje", column = "id_mensaje"), 
			@Result(property = "nombreArchivo", column = "nombre_archivo"),
			@Result(property = "archivoBytes", column = "archivo"),
			@Result(property = "fSubida", column = "fecha_subida") })
	public List<ArchivoTicketDTO> buscarArchivosMensaje(@Param("idMensaje") Integer idMensaje);
}
