package com.unmsm.software.persistencia.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.unmsm.software.persistencia.dto.ClienteDTO;

public interface ClienteMapper {

	@Insert("INSERT INTO cliente (id_cliente,id_usuario) VALUES (#{idCliente},#{idUsuario})")
	public void insertarCliente(ClienteDTO cliente);

	@Delete("DELETE FROM cliente WHERE id_usuario = #{idUsuario}")
	public void eliminarCliente(ClienteDTO cliente);

	@Select(value = "SELECT id_cliente, id_usuario FROM cliente WHERE id_cliente = #{idCliente}")
	@Results(value = { @Result(javaType = ClienteDTO.class), @Result(property = "idUsuario", column = "id_usuario"),
			@Result(property = "idCliente", column = "id_cliente") })
	public ClienteDTO buscarCliente(@Param("idCliente") Integer idCliente);
}
