package com.unmsm.software.persistencia.dto;

import java.util.List;

public class CategoriaDTO implements Comparable<CategoriaDTO>{
    /**
	 * 
	 */
	
	/**
	 * 
	 */
	private Integer id_categoria;
	private String descripcion;
	private Integer estado;
	private Integer nivel;
	private Integer cat_padre;
	private List<CategoriaDTO> categorias;
	
	private String name;
    
    private String size;
     
    private String type;

    	
	public CategoriaDTO(String name, String size, String type) {
		super();
		this.name = name;
		this.size = size;
		this.type = type;
	}
	

	public CategoriaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
		
    public Integer getId_categoria() {
		return id_categoria;
	}


	public void setId_categoria(Integer id_categoria) {
		this.id_categoria = id_categoria;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getEstado() {
		return estado;
	}


	public void setEstado(Integer estado) {
		this.estado = estado;
	}


	public Integer getNivel() {
		return nivel;
	}


	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}


	public Integer getCat_padre() {
		return cat_padre;
	}


	public void setCat_padre(Integer cat_padre) {
		this.cat_padre = cat_padre;
	}


	public List<CategoriaDTO> getCategorias() {
		return categorias;
	}


	public void setCategorias(List<CategoriaDTO> categorias) {
		this.categorias = categorias;
	}


	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CategoriaDTO other = (CategoriaDTO) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (size == null) {
            if (other.size != null)
                return false;
        } else if (!size.equals(other.size))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return name;
    }
	public int compareTo(CategoriaDTO o) {
		// TODO Auto-generated method stub
		 return this.getName().compareTo(o.getName());
	}
    
    
}
