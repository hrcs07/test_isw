package com.unmsm.software.persistencia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.unmsm.software.persistencia.dto.TablaMaestraDTO;

public interface TablaMaestraMapper {

	@Results({ @Result(property = "idTablaMaestra", column = "id_tabla_maestra"),
			@Result(property = "idPrim", column = "id_prim"), @Result(property = "idSec", column = "id_sec"),
			@Result(property = "descripcion", column = "descripcion") })
	@Select("SELECT id_tabla_maestra,id_prim,id_sec,descripcion " + " FROM tabla_maestra "
			+ " WHERE id_prim =#{idPrim}")
	public List<TablaMaestraDTO> obtenerRegistroTablaMaestraPorIdPrim(@Param("idPrim") Integer idPrim);

	@Results({ @Result(property = "idTablaMaestra", column = "id_tabla_maestra"),
			@Result(property = "idPrim", column = "id_prim"), @Result(property = "idSec", column = "id_sec"),
			@Result(property = "descripcion", column = "descripcion") })
	@Select("SELECT id_tabla_maestra,id_prim,id_sec,descripcion " + " FROM tabla_maestra "
			+ " WHERE id_prim=#{idPrim} and id_sec =#{idSec}")
	public TablaMaestraDTO obtenerRegistroTablaMaestraPorIdSec(@Param("idPrim") Integer idPrim,
			@Param("idSec") Integer idSec);

	@Insert("INSERT INTO tabla_maestra (id_tabla_maestra,id_prim,id_sec,descripcion) VALUES (#{idTablaMaestra},#{idPrim},#{idSec},#{descripcion})")
	public void insertarSLA(TablaMaestraDTO tablaMaestraDTO);

	@Update("UPDATE tabla_maestra SET id_prim = #{idPrim},id_sec = #{idSec},descripcion = #{descripcion} WHERE id_tabla_maestra = #{idTablaMaestra}")
	public void actualizarRegistroTablaMaestra(TablaMaestraDTO tablaMaestraDTO);

	@Delete("DELETE FROM tabla_maestra WHERE id_tabla_maestra = #{idTablaMaestra}")
	public void eliminarRegistroTablaMaestra(TablaMaestraDTO tablaMaestraDTO);

	@Select("SELECT max(id_sec) " + " FROM tabla_maestra " + " WHERE id_prim = #{idPrim}")
	public Integer obtenerMaxIdSecTablaMaestraPorIdPrim(@Param("idPrim") Integer idPrim);

}
