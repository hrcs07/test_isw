package com.unmsm.software.persistencia.dto;

import java.util.Date;

public class ArchivoTicketDTO{	
	private Integer idArchivo;
	private Integer idTicket;
	private Integer idMensaje;
	private String nombreArchivo;
	private byte[] archivoBytes;
	private Date fSubida;
	
	public ArchivoTicketDTO() {
		
	}
	
	public Integer getIdArchivo() {
		return idArchivo;
	}
	
	public void setIdArchivo(Integer idArchivo) {
		this.idArchivo = idArchivo;
	}
	
	public Integer getIdTicket() {
		return idTicket;
	}
	
	public void setIdTicket(Integer idTicket) {
		this.idTicket = idTicket;
	}
	
	public Integer getIdMensaje() {
		return idMensaje;
	}
	
	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	public byte[] getArchivoBytes() {
		return archivoBytes;
	}
	
	public void setArchivoBytes(byte[] archivoBytes) {
		this.archivoBytes = archivoBytes;
	}
	
	public Date getfSubida() {
		return fSubida;
	}
	
	public void setfSubida(Date fSubida) {
		this.fSubida = fSubida;
	}
}
