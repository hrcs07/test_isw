package com.unmsm.software.persistencia.dto;

public class SlaDTO {

	private Integer id_sla;
	private String codigo;
	private String descripcion;
	private Integer dias;
	private Integer horas;
	private Integer minutos;

	public SlaDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId_sla() {
		return id_sla;
	}

	public void setId_sla(Integer id_sla) {
		this.id_sla = id_sla;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public Integer getMinutos() {
		return minutos;
	}

	public void setMinutos(Integer minutos) {
		this.minutos = minutos;
	}

	public Integer getHoras() {
		return horas;
	}

	public void setHoras(Integer horas) {
		this.horas = horas;
	}

}
