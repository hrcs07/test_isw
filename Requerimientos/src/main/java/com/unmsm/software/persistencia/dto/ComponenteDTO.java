package com.unmsm.software.persistencia.dto;

public class ComponenteDTO {
	private Integer idComponente;
	private Integer idServicio;
	private String nombreComponente;
	private Integer ctTipoComponente;
	private String descTipoComponente;
	private boolean estadoComponente;

	public Integer getIdComponente() {
		return idComponente;
	}

	public void setIdComponente(Integer idComponente) {
		this.idComponente = idComponente;
	}

	public String getNombreComponente() {
		return nombreComponente;
	}

	public void setNombreComponente(String nombreComponente) {
		this.nombreComponente = nombreComponente;
	}

	public Integer getCtTipoComponente() {
		return ctTipoComponente;
	}

	public void setCtTipoComponente(Integer ctTipoComponente) {
		this.ctTipoComponente = ctTipoComponente;
	}

	public String getDescTipoComponente() {
		return descTipoComponente;
	}

	public void setDescTipoComponente(String descTipoComponente) {
		this.descTipoComponente = descTipoComponente;
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public boolean isEstadoComponente() {
		return estadoComponente;
	}

	public void setEstadoComponente(boolean estadoComponente) {
		this.estadoComponente = estadoComponente;
	}

}
